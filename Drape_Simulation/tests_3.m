%% Getting updated parameters

close all;
clear all;
clc;

Updated_Parameters = containers.Map();
load('parametric_surface.mat');
load('Keys cell array.mat');
load('Keys num array.mat');
% Loading the part
part = 'GE90.STL';
[vertices, f, n, c, stltitle] = stlread(part);
delete(gca);
close all;  

%% Filtering the faces which lie on surface only
idx_face = [];
for i=1:size(f)
    c = dot([0.0,0.0,1],n(i,:)); %Dot product between Z and normal
    if c>0 & c<=1
        idx_face = [idx_face; i];
    end
end

updated_keys = [];
points = horzcat( cell2mat(values(Parameters,keys)),key );
for j=1:size(idx_face)
    disp(size(idx_face,1)-j)
    p1 = vertices(f(idx_face(j),1),:);
    p2 = vertices(f(idx_face(j),2),:);
    p3 = vertices(f(idx_face(j),3),:);
    triangle = [p1;p2;p3;p1];
    % Check if the all grid points lie inside any triangle for face j.
    in = inpolygon(points(:,1),points(:,2),triangle(:,1),triangle(:,2));
    % Find the row numbers of elements that actually lie inside.
    % K returns the row numbers from point array
    k = find(in);
    if isempty(k)
    else
        temp = [points(k,1),points(k,2),points(k,3)];
        % Transfer the point to the plane
        a = ((p2(2)-p1(2))*(p3(3)-p1(3)))-((p3(2)-p1(2))*(p2(3)-p1(3)));
        b = ((p2(3)-p1(3))*(p3(1)-p1(1)))-((p3(3)-p1(3))*(p2(1)-p1(1)));
        c = ((p2(1)-p1(1))*(p3(2)-p1(2)))-((p3(1)-p1(1))*(p2(2)-p1(2)));
        d = -(a*p1(1))-(b*p1(2))-(c*p1(3));
        zval = ((-d-(a*temp(:,1))-(b*temp(:,2)))/c)-0.08;
        updated_keys = [updated_keys; temp(:,3)];
        for count=1:size(temp,1)
            Updated_Parameters(num2str(temp(count,3))) = [temp(count,1),temp(count,2),zval(count,1)];
        end
    end
end
updated_key_cell = {};
updated_key_cell = num2cell(updated_keys);
updated_key_cell = cellfun(@num2str, updated_key_cell, 'UniformOutput', false);

save('updated_parameters.mat','Updated_Parameters');
save('updated keys array.mat','updated_keys');
save('updated keys str cell.mat','updated_key_cell');


