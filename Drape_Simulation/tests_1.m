%% Parameterizing the surface

close all;
clear all;
clc;

parametric_precision = 1001;    % Multiple of 10 plus 1. i.e 101, 1001, 10001
Parameters = containers.Map();
part = 'GE90.STL';
[v, f, n, c, stltitle] = stlread(part);
delete(gca);
close all;  

% To parameterize the surface, we consider lowest and highest value of
% individual X and Y coordinate. We create linspaces for X and Y.
% We obtain u and v for all the coordinates on the surface and update stl.
% We keep 10000 elements for enough precision. X goes with u and Y with v

lowest_X = min(v(:,1));
highest_X = max(v(:,1));
lowest_Y = min(v(:,2));
highest_Y = max(v(:,2));

X = linspace(lowest_X,highest_X,parametric_precision);
Y = linspace(lowest_Y,highest_Y,parametric_precision);
u = 0:1/(parametric_precision-1):1;
v = 0:1/(parametric_precision-1):1;

% u in taken as row here and v is taken as columns
for i=1:parametric_precision
    for j=1:parametric_precision
        key = parametric_precision*u(1,i) + v(1,j);
        key = num2str(key);
        Parameters(key) = [X(1,i),Y(1,j)];
    end
end
save('parametric_surface.mat','Parameters');