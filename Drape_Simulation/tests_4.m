%% Testing the Updated Parameters

close all;
clear all;
clc;

load('updated_parameters.mat');
load('updated keys str cell.mat');
part = 'GE90.STL';
[v, f, n, c, stltitle] = stlread(part);
delete(gca);
close all;  

% Choose what color the CAD part needs to be displayed.
col_matrix = [0.941176, 0.972549, 1];

% Plotting the CAD part in Figure-1
figure(1)
patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
xlabel('X-Axis')
ylabel('Y-Axis')
zlabel('Z-Axis')
hold on

points = cell2mat(values(Updated_Parameters,updated_key_cell));
scatter3(points(:,1),points(:,2),points(:,3),100,'r','.');
