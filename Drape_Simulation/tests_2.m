%% Testing the Auxillary Point Generation Algorithm

close all;
clear all;
clc;

% Note range of u and v is from 0 to 1 with 1/1000 precision
% Key used in container map is 1001*value of u + value of v
load('parametric_surface.mat');

%% Assigning values of X,Y,Z to each parametric combination

u = 0:1/(1001-1):1;
v = 0:1/(1001-1):1;
combinations = [];                               % empty matrix for storage
for i = 1:1001            
    j = [0:1/(1001-1):1]';      
    combinations = [combinations; [ [u(1,i)*ones(size(j,1),1)],j] ];
end

keys = {};
key = 1001*combinations(:,1) + combinations(:,2);
keys = num2cell(key);
keys = cellfun(@num2str, keys, 'UniformOutput', false);
save('Keys num array.mat','key')
save('Keys cell array.mat','keys');