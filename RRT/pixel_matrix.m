%% This Script computes the pixel matrix for the given map with resolution 1
clear
clc

P = rrt_map();


%Space Dimensions
x_len = 200;
y_len = 200;

hold on

for (i=1:1:x_len)
    for(j=1:1:y_len)
        M(i,j)=0;
        for (k=1:1:size(P,3))
            xv = [P(1,1,k),P(2,1,k),P(3,1,k),P(4,1,k),P(1,1,k)];
            yv = [P(1,2,k),P(2,2,k),P(3,2,k),P(4,2,k),P(1,2,k)];
            if (inpolygon(i,j,xv,yv)) M(i,j)=1; end
        end
        if(M(i,j)==1) 
            scatter(i,j,50,'b','.')
        end
        if(M(i,j)==0)
            scatter(i,j,50,'w','.')
        end
    end
end

csvwrite('Map.csv',M);