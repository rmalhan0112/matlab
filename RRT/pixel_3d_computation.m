%% 3D computation of the whole space.

%% This Script computes the pixel matrix for the given map with resolution 1
clear
clc

P = rrt_map();


%Space Dimensions
x_len = 200;
y_len = 200;
%Object Dimensions
length = 4;     % Along Y
width = 8;      % Along X

theta_len = 180;
figure(1)
xlim([0 x_len]);
ylim([0 y_len]);
zlim([0 theta_len]);
xlabel('X Value');
ylabel('Y Value');
zlabel('Theta Value');
hold on

tic
for (i=1:10:x_len)
    for(j=1:10:y_len)
        for(t=1:9:theta_len)
            M(i,j,t)=0;
    % Obtain the rectangle at the given Point
            v = draw_rect( [ i,j,deg2rad(t) ], length, width );
    % Collision Checking for whole rectangle with 4 lines.
            M(i,j,t) = coll_check( 1,2,P,v );
            M(i,j,t) = coll_check( 2,3,P,v );
            M(i,j,t) = coll_check( 3,4,P,v );
            M(i,j,t) = coll_check( 4,1,P,v );
            
            if(M(i,j,t)==1) 
                scatter3(i,j,t,50,'b','.')
                pause(0.00001)
            end
            if(M(i,j,t)==0)
                scatter3(i,j,t,50,'w','.')
                pause(0.00001)
            end
        end
    end
end
% save('3D Map_rrt.mat',M);
% csvwrite('Map.csv',M);
toc



%% Collision Check

function collision = coll_check( v1,v2,P,v )
    collision = 0;
    % Define the four lines of rectangle
    [a,b] = bresenham(v(v1,1),v(v1,2),v(v2,1),v(v2,2));
    l = [a(:,1),b(:,1)];

    %Collision Check for Rectangle
    for ( idx=1:size(l,1) )
        for (k=1:1:size(P,3))
            xv = [P(1,1,k),P(2,1,k),P(3,1,k),P(4,1,k),P(1,1,k)];
            yv = [P(1,2,k),P(2,2,k),P(3,2,k),P(4,2,k),P(1,2,k)];
            if (inpolygon(l(idx,1),l(idx,2),xv,yv)) collision=1; end
        end
    end
end


%% Function to draw Rectangle
function points = draw_rect(primitive, a, b)
    x = primitive(1,1);
    y = primitive(1,2);
    t = primitive(1,3);
    
    R = [ cos(t),-sin(t);
           sin(t),cos(t) ];
    T(1:2,1:2) = R;
    T(1,3) = x;
    T(2,3) = y;
    T(3,3) = 1;
    
% Create Rectangle at origin and transform points.
    p1 = T*[a/2;b/2;1];
    p2 = T*[-a/2;b/2;1];
    p3 = T*[-a/2;-b/2;1];
    p4 = T*[a/2;-b/2;1];
    
    points = [ p1(1,1),p1(2,1);
               p2(1,1),p2(2,1);
               p3(1,1),p3(2,1);
               p4(1,1),p4(2,1);];
end