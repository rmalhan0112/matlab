%% Rapidly Exploring Random Tree (RRT)

% This code is the implementation of RRT algorithm in a 2D obstacle space
% for a rectangular robot having three motion primitives. X,Y,theta.
clear
clc
close all

M = csvread('Map.csv');
P = rrt_map();


%Declarations
node = [];
n_tree = [];
path = [];

%Space Dimensions
x_len = 200;
y_len = 200;
theta_min = -90;
theta_max = 90;
%Object Dimensions
length = 7;     % Along x
width = 14;      % Along y

%Initialize
n_strt = [ 30,165,0 ];
n_goal = [ 170,30,90 ];
n_tree = [ n_tree; [n_strt,0] ];

% vid = VideoWriter('RRT.mp4');
% open(vid);
figure(1)
fig = gcf;
xlim([0 x_len]);
ylim([0 y_len]);
hold on
vert_obj = draw_rect( n_strt(1,:), length, width );
plot_rect( vert_obj, 'r' )
vert_obj = draw_rect( n_goal(1,:), length, width );
plot_rect( vert_obj, 'k' )
xlabel('X value');
ylabel('Y value');
% F = getframe(fig);
% writeVideo(vid,F);

%% Algorithm
% Creation of Random Samples
tic
idx = 0;
flag=0;
while(flag==0 && idx<1e4)
    x = randsample( 1:1:x_len,1 );
    y = randsample( 1:1:y_len,1 );
    theta = randsample( theta_min:1:theta_max,1 );
    p = randsample(0:0.1:1,1);
%     scatter(x,y,'k','.')
%     F = getframe(fig);
%     writeVideo(vid,F);

    idx = idx + 1;
    node = [ node; [x,y,theta] ];
    
% If it is a Valid node
    %Finding the valid step and parent in that direction
    % If Probaility sampling < 0.9
    if (p<0.9)
        [inter_node,parent] = valid_step( node(idx,:), n_tree, length, width,M );
    end
    
    % If Probaility sampling < 0.9
    if (p>=0.9)
        [inter_node,parent] = valid_step( n_goal, n_tree, length, width,M );
    end
    %STOPPING CONDITION    
    if ( norm(n_goal(1,1:2)-inter_node(1,1:2)) < 10 )
            disp('Goal State Reached')
            inter_node = n_goal;
            flag = 1;
    end
    n_tree = [n_tree; [inter_node,parent]];
    
    % Plot the Connection for node.
%     scatter(inter_node(1,1),inter_node(1,2),70,'r','.');
    scatter(n_tree(parent,1),n_tree(parent,2),70,'k','.');
    
%     plot( [n_tree(parent,1),inter_node(1,1)] ,...
%     [n_tree(parent,2),inter_node(1,2)],'r','LineWidth',0.5 );

%         vert_obj = draw_rect( inter_node(1:3), length, width );
%         plot_rect( vert_obj, 'b' )
%         F = getframe(fig);
%         writeVideo(vid,F);
        pause(0.0000000001)
end
toc

%% Define Path for RRT
path = [path; n_goal];
parent = n_tree(size(n_tree,1),4);
while(parent~=1)
    path = [path; n_tree(parent,1:3)];
    parent = n_tree(parent,4);
end
path = [path; n_strt];

% Plot the path
for (i=size(path):-1:2)
    vert_obj = draw_rect( path(i,:), length, width );
    plot_rect( vert_obj, 'b' )
    plot( [path(i,1),path(i-1,1)], [path(i,2),path(i-1,2)], 'r', 'LineWidth',2 )
%     F = getframe(fig);
%     writeVideo(vid,F);
    pause(0.01)
    if(i==2)
        for(i=1:1:40)
%             F = getframe(fig);
%             writeVideo(vid,F);
            pause(0.1)
        end
    end
end
% close(vid)
% close all








%% Functions

%% Valid Step Function

function [inter_node,i] = valid_step(node, n_tree, length, width,M)
    dist=[]; %Distance Storage
    for (iter=1:1:size(n_tree,1))
        dx = node(1,1) - n_tree(iter,1);
        dy = node(1,2) - n_tree(iter,2);
        dt = node(1,3) - n_tree(iter,3);
        d = (dx^2 + dy^2 + dt^2);
%         d = (dx^2 + dy^2);
        dist = [dist; d];
    end
    [val,i] = min(dist); %i is the iteration or row number
    
    %Linear Interpolation for resolution:
    dx =  node(1,1) - n_tree(i,1);
    dy =  node(1,2) - n_tree(i,2);
    dt =  node(1,3) - n_tree(i,3);
    dir_vec = [dx,dy,dt];
    dir_vec = dir_vec / norm(dir_vec);
    
%     if(abs(dx) < 1 || abs(dy) < 1 || abs(dt) < 1)
%         inter_node(1,1:3) = n_tree(i,1:3);
%         return;
%     end
    
    collision = 0;
    step = 1;
    inter_node(1,:) = n_tree(i,1:3);
%     while (collision~=1 && step<100)
    while (step<50)
        step = step+1;
        vert_obj = draw_rect( inter_node(1,:), length, width );

    % Check for Collision of this rectangle
        collision = coll_check( 1,2,vert_obj,M ); if(collision==1) continue; end
        collision = coll_check( 2,3,vert_obj,M ); if(collision==1) continue; end
        collision = coll_check( 3,4,vert_obj,M ); if(collision==1) continue; end
        collision = coll_check( 4,1,vert_obj,M ); if(collision==1) continue; end
        
        direction = dir_vec*step;
        inter_node(1,:) = n_tree(i,1:3) + round(direction(1,:));
    end
end



%% Collision Check

function collision = coll_check( v1,v2,v,M )
    collision = 0;
    % Define the four lines of rectangle
    [a,b] = bresenham(v(v1,1),v(v1,2),v(v2,1),v(v2,2));
    l = [a(:,1),b(:,1)];

    %Collision Check for Rectangle
    for ( idx=1:size(l,1) )
        if( M(l(idx,1),l(idx,2))==1 )
            collision=1;
            return
        end
    end
end



%% Forming a rectangle for robot
function points = draw_rect(primitive, a, b)
    x = primitive(1,1);
    y = primitive(1,2);
    t = deg2rad(primitive(1,3));
    
    R = [ cos(t),-sin(t);
           sin(t),cos(t) ];
    T(1:2,1:2) = R;
    T(1,3) = x;
    T(2,3) = y;
    T(3,3) = 1;
    
% Create Rectangle at origin and transform points.
    p1 = T*[a/2;b/2;1];
    p2 = T*[-a/2;b/2;1];
    p3 = T*[-a/2;-b/2;1];
    p4 = T*[a/2;-b/2;1];
    
    points = [ p1(1,1),p1(2,1);
               p2(1,1),p2(2,1);
               p3(1,1),p3(2,1);
               p4(1,1),p4(2,1);];
end


%% Plotting the rectangle
function plot_rect( vert_obj, color )
    chain_x = [ vert_obj(1,1),vert_obj(2,1),vert_obj(3,1),vert_obj(4,1),vert_obj(1,1) ];
    chain_y = [ vert_obj(1,2),vert_obj(2,2),vert_obj(3,2),vert_obj(4,2),vert_obj(1,2) ];
    plot(chain_x,chain_y,color,'Linewidth',0.7);
end