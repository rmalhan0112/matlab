%% This is the map of the planar robot obstacle space.
clear 
clc

corner = [900,500];
r = 200;
rectangle('Position',[corner(1,1),corner(1,2),2*r,2*r],...
    'Curvature',[1,1],'FaceColor','g');

% close all

xlim([0 2200]);
ylim([0 1100]);
hold on
xlabel('X value');
ylabel('Y value');

% Define Circle as a Polygon for Collision Checking
xv = [];
xu = [];
for angle=1:1:360
    xv = [xv; [r*cos(deg2rad(angle)),0,1]];
    xu = [xu; [0,r*sin(deg2rad(angle)),1]];
end
transf = [1,0,(corner(1,1)+r);
            0,1,(corner(1,2)+r);
            0,0,1];
xv = (transf*xv')';
xu = (transf*xu')';
xv = xv(:,1);
xu = xu(:,2);

% Collision Checking
%Space Dimensions

for (i=1:10:2200)
    for(j=1:10:1100)
        M(i,j)=0;
        if (inpolygon(i,j,xv,xu)) M(i,j)=1; end
        if(M(i,j)==1) 
            scatter(i,j,50,'w','.')
            pause(0.00000001)
        end
        if(M(i,j)==0)
            scatter(i,j,50,'b','.')
            pause(0.00000001)
        end
    end
end

csvwrite('Robot_Map.csv',M);