%% This code simulates the planar robot with given Joint angles

clear
clc
close all

J = csvread('599_Waypoints.csv');
obstacle();
%Space Dimensions
x_len = 2200;
y_len = 1100;

% Robot Link Dimensions:
l1 = 500;
l2 = 300;
l3 = 100;
l4 = 50;

% DH parameters for the global frames of all four links 
F(1,:) = [0,0,0];
F(2,:) = [0,l1,0];
F(3,:) = [0,l2,0];

% Links as defined by two points in their global frame
l(:,:,1) = [0,0;0,l1/2;0,l1/2;0,l1];
l(:,:,2) = [0,0;0,l2/2;0,l2/2;0,l2];
l(:,:,3) = [0,0;0,l3;-l4/2,l3;l4/2,l3];

figure(1)
xlim([0 x_len]);
ylim([0 y_len]);
hold on
xlabel('X value');
ylabel('Y value');

J_strt = [-70,0,0];
line = robot_config(J_strt,F,l);
X = round(line(2,1,3));
Y = round(line(2,2,3));

J_goal = [70,0,0];
line = robot_config(J_goal,F,l);
X = round(line(2,1,3));
Y = round(line(2,2,3));

% Plot the Map
line = robot_config(J_strt,F,l);
plot_robot(line,'r');
line = robot_config(J_goal,F,l);
plot_robot(line,'k');
pause(2)

x = input('Press 1 to continue or 2 to cancel\n');
if (x==1)
    disp('Plotting Motion');
else
    return;
end

pause(2)

J=[0,0,0];
%% Plotter
for (i=size(J):-1:1)
    line = robot_config(J(i,:),F,l);
    plot_robot(line,'b');
    pause(0.2)
%     cla
    obstacle()
end





%% Function Definitions


%% Returns the new Coordinates of the robot
function line = robot_config(J,F,l)
    T = [1,0,1100;
        0,1,0;
        0,0,1];
    
    for i=1:size(F,1) 
        T = T * transf_M( F(i,:), J(1,i) );
        line(:,:,i) = transf_link(T, l(:,:,i)); 
    end
end


%% Returns the transformation matrices for each link
function T = transf_M(F , t)
    t = deg2rad(t);
    Rz = [ cos(t),-sin(t),0;
           sin(t),cos(t),0;
           0,0,1];
    
    theta = deg2rad(F(1,3));
    R = [ cos(theta),-sin(theta);
           sin(theta),cos(theta) ];
       
    T(1:2,1:2) = R;
    T(1,3) = F(1,1);
    T(2,3) = F(1,2);
    T(3,3) = 1;
    T = T*Rz;
end

%% Returns transformed links
function line = transf_link(T, l)
    points = [];
    for i=1:1:size(l,1)
        p = T*[l(i,1);l(i,2);1];
        p = p';
        points = [points; p(1,1:2)];
    end
    line = points;
end

%% Plots the robot
function plot_robot(line,color)
    for k=1:size(line,3)
        for i=1:2:size(line,1)
            X = [line(i,1,k),line(i+1,1,k)];
            Y = [line(i,2,k),line(i+1,2,k)];
            plot(X,Y,color,'LineWidth',1);
        end
    end
end

%% Define Circle as a Polygon for Collision Checking
function [xv,xu] = obstacle()
%     corner = [625,600];
%     r = 100;
%     corner1 = [900,600];
%     r1 = 100;
%     corner2 = [1175,600];
%     r2 = 100;
    corner = [1100,700];
    r = 50;
    corner1 = [1675,575];
    r1 = 40;
    corner2 = [500,600];
    r2 = 65;
    corner3 = [1900,350];
    r3 = 10;
    
    rectangle('Position',[corner(1,1),corner(1,2),2*r,2*r],...
    'Curvature',[1,1],'FaceColor','g');
    rectangle('Position',[corner1(1,1),corner1(1,2),2*r1,2*r1],...
    'Curvature',[1,1],'FaceColor','g');
    rectangle('Position',[corner2(1,1),corner2(1,2),2*r2,2*r2],...
    'Curvature',[1,1],'FaceColor','g');
    rectangle('Position',[corner3(1,1),corner3(1,2),2*r3,2*r3],...
    'Curvature',[1,1],'FaceColor','g');
end