%% RRT for Planar Robot

clear
clc
close all

% vid = VideoWriter('RRT_Manipulator_1.avi');
% open(vid);
% fig = gcf;

% M = csvread('Robot_Map.csv');
[xv,xu] = obstacle();
%Space Dimensions
x_len = 2200;
y_len = 1100;

% Robot Link Dimensions:
l1 = 500;
l2 = 300;
l3 = 100;
l4 = 50;

% DH parameters for the global frames of all four links 
F(1,:) = [0,0,0];
F(2,:) = [0,l1,0];
F(3,:) = [0,l2,0];

% Links as defined by two points in their global frame
l(:,:,1) = [0,0;0,l1/2;0,l1/2;0,l1];
l(:,:,2) = [0,0;0,l2/2;0,l2/2;0,l2];
l(:,:,3) = [0,0;0,l3;-l4/2,l3;l4/2,l3];

figure(1)
xlim([0 x_len]);
ylim([0 y_len]);
hold on
xlabel('X value');
ylabel('Y value');

% Declarations
n_tree = [];
% J_strt = [-8,20,20];
% J_strt = [-70,0,0];
J_strt = [-10,0,0];
line = robot_config(J_strt,F,l);
X = round(line(2,1,3));
Y = round(line(2,2,3));
J_strt(4:5) = [X,Y];

% J_goal = [30,-35,-30];
% J_goal = [70,0,0];
J_goal = [10,0,0];
line = robot_config(J_goal,F,l);
X = round(line(2,1,3));
Y = round(line(2,2,3));
J_goal(4:5) = [X,Y];

n_tree = [ n_tree; [J_strt(1:3),1,J_strt(4:5)] ];
count = 0;
idx = 0;
flag=0;

% Plot the Map with start and goal states
% corner = [900,500];
% r = 200;

line = robot_config(J_strt,F,l);
plot_robot(line,'r');
line = robot_config(J_goal,F,l);
plot_robot(line,'k');
pause(0.0001)

x = input('Press 1 to continue or 2 to cancel\n');
if (x==1)
    disp('Running RRT');
else
    return;
end

%% Algorithm
tic
while(flag==0 && idx<1e5)
    idx = idx + 1;
    if (mod(idx,1000)==0) fprintf('%d Iterations reached:\nDistance to Goal: %f\n\n',idx,dist_to_goal); end
    t1 = randsample( -90:1:90,1 );
    t2 = randsample( -90:1:90,1 );
    t3 = randsample( -90:1:90,1 );
    p = randsample(0:0.1:1,1);
    
    J_node = [t1,t2,t3];
   
        
    % If Probaility sampling > 0.9
    if (p<0.9)
        [step,direction,parent,X,Y] = valid_step( J_node, n_tree, xv,xu,F,l );
    end
    
    % If Probaility sampling < 0.9
    if (p>=0.9)
        [step,direction,parent,X,Y] = valid_step( J_goal, n_tree, xv,xu,F,l );
    end
    if (step~=0)
        count = count + 1;
        inter_node(1,1:3) = n_tree(parent,1:3) + direction(1,:);
        inter_node(1,4:5) = [X,Y];
        %STOPPING CONDITION
        dist_to_goal = norm(J_goal(1,1:2)-inter_node(1,1:2));
        if ( dist_to_goal < 4 )
            disp('Goal State Reached')
            inter_node = [J_goal];
            flag = 1;
        end
        n_tree = [n_tree; [inter_node(1:3),parent,inter_node(4:5)]];
        % Plot the Connection for node.
%         scatter(n_tree(parent,5),n_tree(parent,6),20,'r','.');
%         plot( [n_tree(parent,5),inter_node(1,4)] ,...
%         [n_tree(parent,6),inter_node(1,5)],'r','LineWidth',0.05 );
%         pause(0.000000001)
    end
end
toc
fprintf ('Total Samples: %d & Accepted Sample: %d\n',idx,count);


%% Define Path for RRT
path = [];
if (flag==1)
    x = input('Path Found. Press 1 to Plot\n');
    pause(3);
    disp('Plotting');
    
    path = [path; J_goal];
    parent = n_tree(size(n_tree,1),4);
    while(parent~=1)
        path = [path; [n_tree(parent,1:3),n_tree(parent,4:5)]];
        parent = n_tree(parent,4);
    end
    path = [path; J_strt];
%     % Plot the motion and path
%     for (i=size(path,1)-1:-1:3)
%         plot( [path(i,4),path(i-1,4)], [path(i,5),path(i-1,5)], 'k', 'LineWidth',0.5 )
%         pause(0.1)
%     end
    for (i=size(path,1)-1:-1:2)
        line = robot_config(path(i,:),F,l);
        plot_robot(line,'b');
%         Frame = getframe(fig);
%         writeVideo(vid,Frame);
        pause(0.2)
    end
else 
    disp('Path Not Found')
end

% close(vid)











%% Function Definitions

function [step,direction,i,X,Y] = valid_step( node, n_tree, xv,xu,F,l );
    
    dist=[]; %Distance Storage
    for iter=1:1:size(n_tree,1)
        dt1 = node(1,1) - n_tree(iter,1);
        dt2 = node(1,2) - n_tree(iter,2);
        dt3 = node(1,3) - n_tree(iter,3);
        d = (dt1^2 + dt2^2 + dt3^2);
        dist = [dist; d];
    end
    [val,i] = min(dist); %i is the iteration or row number
    
    %Linear Interpolation for resolution:
    dt1 =  node(1,1) - n_tree(i,1);
    dt2 =  node(1,2) - n_tree(i,2);
    dt3 =  node(1,3) - n_tree(i,3);
    dir_vec = [dt1,dt2,dt3];
    
    step = 0.3;
    direction = round(dir_vec*step);
    sing_step = 8;
    if (direction(1,1)^2 + direction(1,2)^2 + direction(1,3)^2 > 3*sing_step^2)
       if (dir_vec(1,1)~=0) dir_vec(1,1) = dir_vec(1,1) / abs(dir_vec(1,1)); end
       if (dir_vec(1,2)~=0) dir_vec(1,2) = dir_vec(1,2) / abs(dir_vec(1,2)); end
       if (dir_vec(1,3)~=0) dir_vec(1,3) = dir_vec(1,3) / abs(dir_vec(1,3)); end
       direction = dir_vec * sing_step;
    end
    inter_node(1,:) = n_tree(i,1:3) + direction(1,:);
    line = robot_config(inter_node,F,l);
    X = round(line(2,1,3));
    Y = round(line(2,2,3));
    % Collision Checker
    collision = coll_check( line,xv,xu );
        if collision~=0
           step = 0;
           return;
        end
end




%% Collision Check

function collision = coll_check( line,xv,xu )
    collision = 0;
    scale = 10;
    % Define the four links of robot
    for i=1:size(line,3)
        v = round(line(:,:,i)/scale);
        for j=1:2:size(line,1)
            [a,b] = bresenham(v(j,1),v(j,2),v(j+1,1),v(j+1,2));
            l = [a(:,1)*scale,b(:,1)*scale];

            %Collision Check for Rectangle
            for idx=1:size(l,1)
                for k=1:1:size(xv,2)
                    if( l(idx,1)<0 || l(idx,2)<0 || inpolygon(l(idx,1),...
                            l(idx,2),xv(:,k),xu(:,k))==1 )
                        collision=1;
                        return;
                    end
                end
            end
            
        end
    end
end



%% Returns the new Coordinates of the robot
function line = robot_config(J,F,l)
    T = [1,0,1100;
        0,1,0;
        0,0,1];
    
    for i=1:size(F,1) 
        T = T * transf_M( F(i,:), J(1,i) );
        line(:,:,i) = transf_link(T, l(:,:,i)); 
    end
end


%% Returns the transformation matrices for each link
function T = transf_M(F , t)
    t = deg2rad(t);
    Rz = [ cos(t),-sin(t),0;
           sin(t),cos(t),0;
           0,0,1];
    
    theta = deg2rad(F(1,3));
    R = [ cos(theta),-sin(theta);
           sin(theta),cos(theta) ];
       
    T(1:2,1:2) = R;
    T(1,3) = F(1,1);
    T(2,3) = F(1,2);
    T(3,3) = 1;
    T = T*Rz;
end

%% Returns transformed links
function line = transf_link(T, l)
    points = [];
    for i=1:1:size(l,1)
        p = T*[l(i,1);l(i,2);1];
        p = p';
        points = [points; p(1,1:2)];
    end
    line = points;
end

%% Plots the robot
function plot_robot(line,color)
    for k=1:size(line,3)
        for i=1:2:size(line,1)
            X = [line(i,1,k),line(i+1,1,k)];
            Y = [line(i,2,k),line(i+1,2,k)];
            plot(X,Y,color,'LineWidth',1);
        end
    end
end

%% Define Circle as a Polygon for Collision Checking
function [xv,xu] = obstacle()
%     corner = [625,600];
%     r = 100;
%     corner1 = [900,600];
%     r1 = 100;
%     corner2 = [1175,600];
%     r2 = 100;
%     corner = [1100,700];
%     r = 50;
%     corner1 = [1675,575];
%     r1 = 40;
%     corner2 = [500,600];
%     r2 = 65;
%     corner3 = [1900,350];
%     r3 = 10;

    corner = [700,600];
    r = 100;
    corner1 = [1000,600];
    r1 = 100;
    corner2 = [1300,600];
    r2 = 100;
    
    rectangle('Position',[corner(1,1),corner(1,2),2*r,2*r],...
    'Curvature',[1,1],'FaceColor','g');
    rectangle('Position',[corner1(1,1),corner1(1,2),2*r1,2*r1],...
    'Curvature',[1,1],'FaceColor','g');
    rectangle('Position',[corner2(1,1),corner2(1,2),2*r2,2*r2],...
    'Curvature',[1,1],'FaceColor','g');
%     rectangle('Position',[corner3(1,1),corner3(1,2),2*r3,2*r3],...
%     'Curvature',[1,1],'FaceColor','g');
    
    xv = [];
    xu = [];
    for angle=1:1:360
        xv = [xv; [r*cos(deg2rad(angle)),0,1, ...
                    r1*cos(deg2rad(angle)),0,1, ...
                    r2*cos(deg2rad(angle)),0,1]];
                
        xu = [xu; [0,r*sin(deg2rad(angle)),1,...
                   0,r1*sin(deg2rad(angle)),1, ...
                   0,r2*sin(deg2rad(angle)),1]];
    end
    transf = [1,0,(corner(1,1)+r);
                0,1,(corner(1,2)+r);
                0,0,1];
    transf1 = [1,0,(corner1(1,1)+r1);
                0,1,(corner1(1,2)+r1);
                0,0,1];
    transf2 = [1,0,(corner2(1,1)+r2);
                0,1,(corner2(1,2)+r2);
                0,0,1];
%     transf3 = [1,0,(corner3(1,1)+r3);
%         0,1,(corner3(1,2)+r3);
%         0,0,1];
    
    xv(:,1:3) = (transf*xv(:,1:3)')';
    xu(:,1:3) = (transf*xu(:,1:3)')';
    xv(:,4:6) = (transf1*xv(:,4:6)')';
    xu(:,4:6) = (transf1*xu(:,4:6)')';
    xv(:,7:9) = (transf2*xv(:,7:9)')';
    xu(:,7:9) = (transf2*xu(:,7:9)')';
%     xv(:,10:12) = (transf3*xv(:,10:12)')';
%     xu(:,10:12) = (transf3*xu(:,10:12)')';
    
%     xv = [xv(:,1),xv(:,4),xv(:,7),xv(:,10)];
%     xu = [xu(:,2),xu(:,5),xu(:,8),xu(:,11)];
    xv = [xv(:,1),xv(:,4),xv(:,7)];
    xu = [xu(:,2),xu(:,5),xu(:,8)];
end