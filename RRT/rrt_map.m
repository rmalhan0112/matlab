%% RRT map
% This function gives out the polygons which are making the RRT map.

function P = rrt_map()
%     figure('units','normalized','outerposition',[0 0 1 1])

    scale = 1;
    col_matrix = [0.941176, 0.972549, 1];

    F(1,:) = [0,0,10,200];
    F(2,:) = [10,130, 60, 10];
    F(3,:) = [10, 190, 60, 10];
    F(4,:) = [70, 170, 60, 30];
    F(5,:) = [130, 190, 70, 10];
    F(6,:) = [70, 130, 60, 20];
    F(7,:) = [190, 130, 10, 60];
    F(8,:) = [170, 70, 30, 60];
    F(9,:) = [130, 70, 20, 60];
    F(10,:) = [190, 0, 10, 70];
    F(11,:) = [10, 0, 180, 10];
    F(12,:) = [10, 10, 120, 120];

    for i=1:size(F)
        x = F(i,1)*scale;
        y = F(i,2)*scale;
        w = F(i,3)*scale;
        h = F(i,4)*scale;

        v1 = [ x,y ];
        v2 = [ x+w,y ];
        v3 = [ x+w,y+h ];
        v4 = [ x,y+h ];

        P(:,:,i) = [v1;v2;v3;v4];
        chain_x = [ v1(1,1),v2(1,1),v3(1,1),v4(1,1),v1(1,1) ];
        chain_y = [ v1(1,2),v2(1,2),v3(1,2),v4(1,2),v1(1,2) ];
        fill(chain_x,chain_y,'g', 'LineStyle','none');
        hold on
    end
end