%% RosenBrock Steepest Descent

clear all
clc
close all

global a;
n = input('Enter the Dimension Number n\n');
a = input('Enter the Parameter Alpha\n');
x_int = input('Enter the Initial Point\n');
if (size(x_int,2)==n) x_int = x_int'; end
if (n~= size(x_int,1)) disp('Wrong Start Point. Shutting Down'); return; end

    
% n=2;
% a = 1;

%Plotting the Function
xopt = ones(n,1);
[x,y] = meshgrid(-5:0.1:5); %Rosenbrock
z = a*(y-x.^2).^2+(1-x).^2;
figure('units','normalized','outerposition',[0 0 1 1])
hold on;
surf(x,y,z)
xlabel('x1 value');
ylabel('x2 value');
zlabel('Function Value');
scatter3( xopt(1,1),xopt(2,1),func(xopt),500,'g','*','LineWidth',5 );
% view(-140,30);
view(0,90);

%% Initialization        
%Initial Value of Point from where to start search
% x_int = [-1.2;1];
x_c = x_int;
g = gradient(x_c);    %Gradient Evaluation
counter = 0;
iter = 5e4;
e = 1e-5;
scatter3( x_c(1,1),x_c(2,1),func(x_c),500,'k','*','LineWidth',3 );
flag=0;


%% Algorithm:
%Direction Vector for Search is Negation of Gradient
tic
while ( norm(g) > e && counter < iter )
    counter = counter + 1;
%     fprintf('Iteration: %d\n',counter)
    dir_vec = -g;
    dir_vec = dir_vec / norm(dir_vec);
%     s = armijo_search( x_c , dir_vec , @func, g);
    s = goldensection_search( x_c , dir_vec , g, @func );
%     s = bisection( x_c, dir_vec, @func );
    x_c = x_c + s*dir_vec;
    g = gradient(x_c);
    scatter3( x_c(1,1),x_c(2,1),func(x_c),300,'r','x','LineWidth',3 );
    pause(0.00000001)
end
toc
fprintf('\n\nOptimum Value:  \n');
disp(x_c(:,1))
fprintf('Function Value: %f\n',func(x_c));
fprintf('Gradient: %f\n',norm(g));
fprintf('Iterations to converge: %d\n',counter)







%% Function Definitions

% Function Handle
function y = func(X)
    global a;
    n = size(X,1);
    n = n/2;
    y = 0;
    for i=1:1:n
        y = y + ( a*( X(2*i,1)-X(2*i-1,1)^2 )^2 + ( 1-X(2*i-1,1) )^2 );
    end
end

% Gradient
function g = gradient(X)
    global a;
    n = size(X,1);
%     n = n/2;
    for i=1:2:n
        g(i,1) = ( -2*a*( X(i+1,1)-X(i,1)^2 )*( 2*X(i,1) ) - 2*( 1-X(i,1) ) );
        g(i+1,1) = 2*a*( X(i+1,1)-X(i,1)^2 );
    end
end

% Hessian
function h = hessian(X)
    global a;
    a = 1;
    n = size(X,1);
    for i=1:1:n
        for j=1:1:n
            % If row is odd
            if (mod(i,2)~=0)
                if (i==j) h(i,j)= -4*a*(X(i+1,1)-X(i,1)^2) + 8*a*X(i,1)^2 + 2; end
                if (j==i+1) h(i,j)= -4*a*X(i,1); end
                if (i~=j && j~=i+1) h(i,j)= 0; end
            end
            
            % If row is even
            if (mod(i,2)==0) 
                if (i==j) h(i,j)= 2*a; end
                if (j==i-1) h(i,j)= -4*a*X(i-1,1); end
                if (i~=j && j~=i-1) h(i,j)= 0; end
            end
        end
    end
end


% Armijo's Search
% This function takes, current point x_k and direction as input and gives 
%the best step length to be taken s as output.
function s = armijo_search( x_c , dir_vec , func_eval, g )

    alpha = 1e-8;
    eeta = 2;
    theta = 0.1;
    iter = 1e5;
    counter = 0;
    
%Check for Armijo Condition:
    if ( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = eeta * alpha;
        end
        alpha = alpha / eeta;
    elseif ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = alpha / eeta;
        end
        alpha = alpha / eeta;
    end
    s = alpha;
end

%% Golden section search
function step = goldensection_search( x_c , dir_vec , g, func_eval )
    % Golden Section Search Optimization    
    golden_ratio = 0.618;
    step = 1e-4;
    eeta = 2;
    f_xc = func_eval(x_c);
    while( (func_eval( x_c + step * dir_vec ) - f_xc) <= 0 )
            step = step*eeta;
    end
    a = 0;
    b = step;

    % Initialize
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    x_right = x_low + golden_ratio*(x_high-x_low);
    x_left = x_low + (1-golden_ratio)*(x_high-x_low);

    f_right = func_eval(x_right);
    f_left = func_eval(x_left);

    count = 0;

    tic
    while (norm(x_high-x_low) > 1e-4 && count<1e5)
        count = count + 1;

        if (f_right > f_left)
            x_high = x_right;
            x_right = x_left;
            f_right = f_left; 
            x_left = x_high - golden_ratio*(x_high-x_low);
            f_left = func_eval(x_left);
        else     
            x_low = x_left;
            x_left = x_right;
            f_left = f_right;
            x_right = x_low + golden_ratio*(x_high-x_low);
            f_right = func_eval(x_right);
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end



% Bisection Search
function step = bisection( x_c , dir_vec , func_eval )
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
            step = step*eeta;
    end
    a = 0;
    b = step;
    iter = 1e5;
    count=0;
    
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    e = 1e-4/2; % Distinguishability constant 2e = 0.01
    
    while (norm(x_high - x_low) > 1e-2 && count<iter)
        count = count + 1;
        x = (x_high + x_low)/2;
        x_right = x + e*dir_vec;
        x_left = x - e*dir_vec;
        f_right = func_eval(x_right);
        f_left = func_eval(x_left);
        if (f_right > f_left)
            x_high = x_right;
        else
            x_low = x_left;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end