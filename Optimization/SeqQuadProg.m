%% Problem-1 Sequential Quadratic Programming.

%% Answer-a

clc;
clear all;

x0=[3,0.75,20,7.5,7.5,3.4,5.3];
F=@(x)(0.7854*x(1)*(x(2)^2)*(3.3333*x(3)^2+14.9334*x(3)-43.0934)-1.5079*x(1)*...
    (x(6)^2+x(7)^2)+7.477*(x(6)^3+x(7)^3)+0.7854*(x(4)*x(6)^2+x(5)*x(7)^2));
A=[];
b=[];
Aeq=[];
beq=[];
lb=[];
ub=[];
a=@constr;
options = optimoptions('fmincon','Algorithm','sqp');

[x,fval,exitflag,output,lambda,grad] = fmincon(F,x0,A,b,Aeq,beq,lb,ub,a,options);

%The constraint function is defined at end of Code.

[d,deq]=constr(x);
InitialX=x0';
FinalX=x';

%Displaying the results in a table
rowname1={'x1','x2','x3','x4','x5','x6','x7'};
table(InitialX,FinalX,'Rownames',rowname1)
fprintf('The value of Function at minimum x0 is %d',fval)

%Sorting out active and inactive constraint
Actinact={};

for i=1:1:25
if(abs(d(i,1))<1e-5)
    Actinact(i,1)={'Yes'};
    d(i,1)=0;
else
    Actinact(i,1)={'No'};
end
i=i+1;
end
rowname2={'g1','g2','g3','g4','g5','g6','g7','g8','g9','g10','g11','g12',...
    'g13','g14','g15','g16','g17','g18','g19','g20','g21','g22','g23',...
    'g24','g25'};

ValueatOptimumX=d;
table(ValueatOptimumX,Actinact,'Rownames',rowname2)



%% Answer-b

%Let sensitivity matrix be S
%According to definition of sensitivity,

figure(1)
S=grad.*(x');
S=S/fval;
barh(S,'r')
title('Sensitivity Bar graph showing relative sensitivity');
xlabel('Value of relative sensitivity');
ylabel('Numbers representing the sequence of x');

%Comparing true and 1st order estimates.

ftrue=zeros(6,1);
f1storder=zeros(6,1);
g=grad';

k=1;
for i=[0.01,0.05,0.15]
    %True Value estimate
    xnew=x;
    xnew(1,7)=x(1,7)*(i+1);
    ftrue(k,1)=F(xnew);
    
    %First order Estimate
    Fnew= g(1,7)*i*x(1,7);
    f1storder(k,1)=Fnew+F(x);
    k=k+1;
end

k=4;
for i=[0.01,0.05,0.15]
    %True Value estimate
    xnew=x;
    xnew(1,7)=x(1,7)*(1-i);
    ftrue(k,1)=F(xnew);
    
    %First order Estimate
    Fnew= g(1,7)*i*x(1,7);
    f1storder(k,1)=F(x)-Fnew;
    k=k+1;
end

error=zeros(6,1);

%Calculating percentage error
for i=1:1:6
    error(i,1)=(ftrue(i,1)-f1storder(i,1))/ftrue(i,1)*100;
end

%Making a matrix of fval or the Function value at minimum x
fmin=zeros(6,1);
for i=1:1:6
    fmin(i,1)=fval;
end
variation={'+1%','+5%','+15%','-1%','-5%','-15%'};
table(fmin,ftrue,f1storder,error,'Rownames',variation)

%Defining the constraint function

function [c,ceq] = constr(x)
c1 =(27*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-1)))-1;
c2 =(397.5*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-2)))-1;
c3 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(4))^(3))*((x(6))^(-4)))-1;
c4 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(5))^(3))*((x(7))^(-4)))-1;
c5 = (((((745*(x(4))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(16.9*(10^6)))^(0.5))/(110*((x(6))^(3))))-1;
c6 = (((((745*(x(5))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(157.5*(10^6)))^(0.5))/(85*((x(7))^(3))))-1;
c7 = ((x(2)*x(3))/40)-1;
c8 =((x(2)*5)/(x(1)))-1;
c9 = ((x(1))/(12*(x(2))))-1;
c10 = 2.6-x(1);
c11 = x(1)-3.6;
c12 = 0.7-x(2);
c13 = x(2)-0.8;
c14 = 17-x(3);
c15 = x(3)-28;
c16 = 7.3-x(4);
c17 = x(4)-8.3;
c18 = 7.3-x(5);
c19 = x(5)-8.3;
c20 = 2.9-x(6);
c21 = x(6)-3.9;
c22 = 5-x(7);
c23 = x(7)-5.5;
c24 = ((((1.5*(x(6)))+1.9))*((x(4))^(-1)))-1;
c25 = ((((1.1*(x(7)))+1.9))*((x(5))^(-1)))-1;
ceq = [];
c=[c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13;c14;c15;c16;c17;c18;c19;c20;c21;c22;c23;c24;c25];

end
