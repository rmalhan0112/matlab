function [c,cq] = constraints(x)
t = 3.14/2;
rhomat=1450*3.6e-5; %Density of material from Kg/m^3 lb/in^3 (Plastic Kevlar)
Wfs=50; %Fuselage Load in lb
We=100; %Weight of Engine
U=100; %Cruise speed in mph
C=0.85; %Thrust specific fuel consumption
Cl=0.1; %Coefficient of lift
rhoair=1.225*3.6e-5; %Density of air in Kg/m3
R=50; %Range desirable in miles.
Cd=0.02;
c1 = ((((x(2))^4)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*((45*pi*cos(t))+(90*t*((sin(t))^4)...
    *cos(t))+(1800*t*((sin(t))^2)*((cos(t))^3))-(1800*t*...
    ((sin(t))^2)*cos(t))+(90*t*((cos(t))^5))-(300*t*((cos(t))^3))...
    -(48*((sin(t))^5))-(150*((sin(t))^3)*((cos(t))^2))+...
    (80*((sin(t))^3))-(90*((sin(t)))*((cos(t))^4))+(300*((sin(t)))...
    *((cos(t))^2))-32))/(23040*1.0602e+7*(pi*(x(3)^4)/64)))-12;

c2 = ((((x(2)^2)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*0.25*((-t*0.5*cos(t))-((sin(t)^3)/6)...
    +((sin(t))/2)))*((pi*(x(3)^4)/64)/(x(3)/2))/(pi*(x(3)^4)/64))...
    - 60045.6);

c3 = (((-(x(2)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*0.5*((t/2)-((sin(t)*cos(t))/2))))...
    *((2*x(3))/(3*pi))*(pi*(x(3)^2)/8))/((pi*(x(3)^4)/64)*...
    ((x(3))/2)))-(60045.6/2);

c4 = 25-x(1);

c5 = x(1)-150;

c6 = 50-x(2);

c7 = x(2)-500;

c8 = 2-x(3);

c9 = x(3)-20;

c10 = 25-x(4);

c11 = x(4)-150;

c = [c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11];

cq = [(0.5*0.000036*1760*1760*0.1*x(2)*x(4))-(50+100+(x(1))+(0.1*pi*(x(3)^2)*x(2)/4)...
    +((50+100+(x(1))+(0.1*pi*(x(3)^2)*x(2)/4))*((exp((50*0.85*...
    (0.02+((0.1^2)/(pi*0.97*(x(2)/x(4)))) + (0.01*(x(3)/x(4)))...
    +(0.523/((log(0.06*(0.000036*1760*x(4)/0.1366)))^2))))/...
    (100*0.1)))-1)))];
end



