%% Problem-2 Simulated Annealing
clc;
clear;

%% Answer-a

%Defining energy matrix
%E=[];

%Temperature is:
T=1000;
%Kb is:
kb=1;

%Defining a random matrix for values of x
xoo=[1 1 1 1 1 1 1]';

%Value of Rho and cooling rate in Percentage/100
Rho=1e12;
cr=0.1;

F=@(x)(0.7854*x(1)*(x(2)^2)*(3.3333*x(3)^2+14.9334*x(3)-43.0934)-1.5079*x(1)*...
    (x(6)^2+x(7)^2)+7.477*(x(6)^3+x(7)^3)+0.7854*(x(4)*x(6)^2+x(5)*x(7)^2));

%Defining the pseudo function:
[d,deq]=constr(xoo);

%Defining the Penalty function

for i=1:25
if d(i,1)>=0
    dx(i,1)=d(i,1);
else
    dx(i,1)=0;
end
end
D = dx.^2;
s = sum(D(:));
Pnew=s;
Pseudo= F(xoo)+Pnew*Rho;
E1=Pseudo;

%Consider the difference for perturbing value to be 0.04 & finding Pseudo
%new
    
diff=0.02;
counter=0;
while counter<75000

for i=1:7
    xnew(i,1)=(xoo(i,1)-diff)+(rand(1,1)*0.04);
end

[d1,deq]=constr(xnew);

for i=1:7
if d1(i,1)>0
    dx1(i,1)=d1(i,1);
else
    dx1(i,1)=0;
end
end
D = dx1.^2;
s = sum(D(:));
Pnew=s;

Pseudonew= F(xnew)+Pnew*Rho;
E2=Pseudonew;

if (E2<E1)
    xoo=xnew;
    E1=E2;
else
    %Metropolis Step
    e=exp(-(E2-E1)/(kb*T));
    v=rand(1);
    if e>v
        xoo=xnew;
        E1=E2;
        T=T*(1-cr);
    end
end

counter=counter+1;
end
xnew
E1

function [c,ceq] = constr(x)
c1 =(27*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-1)))-1;
c2 =(397.5*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-2)))-1;
c3 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(4))^(3))*((x(6))^(-4)))-1;
c4 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(5))^(3))*((x(7))^(-4)))-1;
c5 = (((((745*(x(4))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(16.9*(10^6)))^(0.5))/(110*((x(6))^(3))))-1;
c6 = (((((745*(x(5))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(157.5*(10^6)))^(0.5))/(85*((x(7))^(3))))-1;
c7 = ((x(2)*x(3))/40)-1;
c8 =((x(2)*5)/(x(1)))-1;
c9 = ((x(1))/(12*(x(2))))-1;
c10 = 2.6-x(1);
c11 = x(1)-3.6;
c12 = 0.7-x(2);
c13 = x(2)-0.8;
c14 = 17-x(3);
c15 = x(3)-28;
c16 = 7.3-x(4);
c17 = x(4)-8.3;
c18 = 7.3-x(5);
c19 = x(5)-8.3;
c20 = 2.9-x(6);
c21 = x(6)-3.9;
c22 = 5-x(7);
c23 = x(7)-5.5;
c24 = ((((1.5*(x(6)))+1.9))*((x(4))^(-1)))-1;
c25 = ((((1.1*(x(7)))+1.9))*((x(5))^(-1)))-1;
ceq = [];
c=[c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13;c14;c15;c16;c17;c18;c19;c20;c21;c22;c23;c24;c25];

end
