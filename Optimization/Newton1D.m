%% This is a 1-D Newton's Optimization method based on the second order approximation

clear
clc

syms x

% Initialization

%**************************************************************************
%TEST FUNCTIONS:
% [a,b] is the search interval. 

% %First Function
% a = 1;
% b = 4;
% fun = @(x) x.^3 - 3*x.^2;        %Function Handle
% xint = 1.2;         % Starting Point

%Second Function
a = 0;
b = 1;
fun = @(x) exp(x) - 2*x;
xint = 0.2;         % Starting Point

%**************************************************************************

e = 1e-8;     % Stopping condition or tolerance for gradient
count = 0;      % Counter
alpha = 0;      % Step-Length
theta = 0.5;    % Armijo's Parameter
X = [a:0.1:b];

figure(1)
plot (X,fun(X),'LineWidth',2);
hold on

G = diff(fun,x);    % Gradient
H = diff(G,x);    % Hessian

x_c = xint;
f_c = fun(x_c);
g = double(subs(G,x_c));      % Evaluate Gradient at x
h = double(subs(H,x_c));      % Evaluate Hessian at x

while (abs(g) > e && count<10)
    if (count==0) disp('Starting Newton Method Iterations'); end
    
    count = count + 1;
% Correcting for Hessian    
    if (h>0)    d = -g/h;   
    else        d = -g;
    end
% Correcting for Direction
    if (d>0)  alpha = min(1 , (b-x_c)/d);
    else      alpha = min(1 , (a-x_c)/d);
    end
%Updating the new Point
    x_nxt = x_c + alpha * d;
    f_nxt = fun(x_nxt);
%Correcting for Increase in Function than reduction
    while(f_nxt >= f_c)
        alpha = theta * alpha;
        x_nxt = x_c + alpha * d;
        f_nxt = fun(x_nxt);
    end
    x_c = x_nxt;
    f_c = f_nxt;
    scatter(x_c,f_c,200,'r','x');
    pause(0.5)
    g = double(subs(G , x_c));      % Evaluate Gradient at x
    h = double(subs(H , x_c));      % Evaluate Hessian at x
end

fprintf('Number of Iterations: %d\n',count)
xopt = x_c;
f = fun(xopt);
fprintf('Optimum Value of X: %f and Value of F is: %f\n',xopt,f);