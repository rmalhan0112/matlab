%% BFGS method for Rosenbrock Function

%This is Quasi Newton method with BFGS update which takes a multivariate function as
%input and calculates the minima for the function. It uses Golden Section to
%compute the step size.
clear all
clc
close all
warning('Off','all');

global a;
n = input('Enter the Dimension Number n\n');
a = input('Enter the Parameter Alpha\n');
x_int = input('Enter the Initial Point\n');
if (size(x_int,2)>1) x_int = x_int'; end
if (n~= size(x_int,1)) disp('Wrong Start Point. Shutting Down'); return; close all; end

%Plotting the Function
[X,Y] = meshgrid(-5:0.1:5); %Rosenbrock
Z = (1-X).^2 + a*(Y-X.^2).^2;

figure('units','normalized','outerposition',[0 0 1 1])
hold on;
surf(X,Y,Z)
xlabel('x1 value');
ylabel('x2 value');
zlabel('Function Value');
scatter3( 1,1,func([1,1]),500,'g','*','LineWidth',5 );
% view(160,40);
view(0,90);

%% Initialization        
%Initial Value of Point from where to start search
x_p = x_int;
g_p = gradient(x_p);    % Gradient Evaluation
H = eye(n);                % Identity matrix as Bk
counter = 0;
iter = 1e5;
e = 1e-5;
scatter3( x_p(1,1),x_p(2,1),func(x_p),500,'k','x','LineWidth',3 );
flag=0;


%% Algorithm:
% Direction Vector for Search is Negation of Gradient
tic
while ( norm(g_p) > e && counter < iter )
    counter = counter + 1;
%     fprintf('Iteration: %d\n',counter)
    if (counter ~= 1) 
%         [B_dash,flag] = update(B,dx,dg);
        [H_dash,flag] = update_H(H,dx,dg);
        if (flag==1) H = H_dash; end
    end
    dir_vec = H*(-g_p);
%     s = armijo_search( x_p, dir_vec, @func, g_p);
    s = goldensection_search( x_p, dir_vec , g_p, @func );
%     s = bisection( x_p, dir_vec , @func );
%     if (s>1) s=1; end
    x_c = x_p + s*dir_vec;
    g_c = gradient(x_c);
    dg = g_c - g_p;
    dx = x_c - x_p;
    g_p = g_c;
    x_p = x_c;
    scatter3( x_p(1,1),x_p(2,1),func(x_p),300,'r','x','LineWidth',3 );
%     pause(0.00000001)
end
toc
fprintf('\n\nOptimum Value:  \n');
disp(x_p(:,1))
fprintf('Function Value: %f\n',func(x_p));
fprintf('Gradient: %f\n',norm(g_p));
fprintf('Iterations to converge: %d\n',counter)










%% Function Definitions

% Function Handle
function y = func(X)
    global a;
    n = size(X,1);
    n = n/2;
    y = 0;
    for i=1:1:n
        y = y + ( a*( X(2*i,1)-X(2*i-1,1)^2 )^2 + ( 1-X(2*i-1,1) )^2 );
    end
end

% Gradient
function g = gradient(X)
    global a;
    n = size(X,1);
%     n = n/2;
    for i=1:2:n
        g(i,1) = ( -2*a*( X(i+1,1)-X(i,1)^2 )*( 2*X(i,1) ) - 2*( 1-X(i,1) ) );
        g(i+1,1) = 2*a*( X(i+1,1)-X(i,1)^2 );
    end
end

% Hessian
function h = hessian(X)
    global a;
    a = 1;
    n = size(X,1);
    for i=1:1:n
        for j=1:1:n
            % If row is odd
            if (mod(i,2)~=0)
                if (i==j) h(i,j)= -4*a*(X(i+1,1)-X(i,1)^2) + 8*a*X(i,1)^2 + 2; end
                if (j==i+1) h(i,j)= -4*a*X(i,1); end
                if (i~=j && j~=i+1) h(i,j)= 0; end
            end
            
            % If row is even
            if (mod(i,2)==0) 
                if (i==j) h(i,j)= 2*a; end
                if (j==i-1) h(i,j)= -4*a*X(i-1,1); end
                if (i~=j && j~=i-1) h(i,j)= 0; end
            end
        end
    end
end


% Armijo's Search
% This function takes, current point x_k and direction as input and gives 
%the best step length to be taken s as output.
function s = armijo_search( x_c , dir_vec , func_eval, g )

    alpha = 1e-8;
    eeta = 2;
    theta = 0.1;
    iter = 1e5;
    counter = 0;
    
%Check for Armijo Condition:
    if ( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = eeta * alpha;
        end
        alpha = alpha / eeta;
    elseif ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = alpha / eeta;
        end
        alpha = alpha / eeta;
    end
    s = alpha;
end

%% Golden section search
function step = goldensection_search( x_c , dir_vec , g, func_eval )
    % Golden Section Search Optimization    
    golden_ratio = 0.618;
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
            step = step*eeta;
    end
    a = 0;
    b = step;

    % Initialize
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    x_right = x_low + golden_ratio*(x_high-x_low);
    x_left = x_low + (1-golden_ratio)*(x_high-x_low);

    f_right = func_eval(x_right);
    f_left = func_eval(x_left);

    count = 0;

    tic
    while (norm(x_high-x_low) > 1e-4 && count<1e5)
        count = count + 1;

        if (f_right > f_left)
            x_high = x_right;
            x_right = x_left;
            f_right = f_left; 
            x_left = x_high - golden_ratio*(x_high-x_low);
            f_left = func_eval(x_left);
        else     
            x_low = x_left;
            x_left = x_right;
            f_left = f_right;
            x_right = x_low + golden_ratio*(x_high-x_low);
            f_right = func_eval(x_right);
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end



% Bisection Search
function step = bisection( x_c , dir_vec , func_eval )
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
            step = step*eeta;
    end
    a = 0;
    b = step;
    iter = 1e5;
    count=0;
    
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    e = 1e-4/2; % Distinguishability constant 2e = 0.01
    
    while (norm(x_high - x_low) > 1e-2 && count<iter)
        count = count + 1;
        x = (x_high + x_low)/2;
        x_right = x + e*dir_vec;
        x_left = x - e*dir_vec;
        f_right = func_eval(x_right);
        f_left = func_eval(x_left);
        if (f_right > f_left)
            x_high = x_right;
        else
            x_low = x_left;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end


function dir_vec = cholesky(h,g)
    % Cholesky Factorization
    [L,D] = ldl(h);
 
%     % Positive Definitiveness check
%     for k=1:size(h,1)
%         if (D(k,k)<=0) D(k,k)=1.2; end
%     end
    A = L*D*L';
    dir_vec = A\(-g);
    dir_vec = dir_vec / norm(dir_vec);
end


function [B,flag] = update(B,dx,dg)
    flag = 1;
    if (dg'*dx<0) flag=0; return; end
    u = dg;
    v = dx;
    v = -B*v;
    n = (size(u,1));
    [L,D] = ldl(B);
    D_cap = ones(n,n);
    L_cap = ones(n,n);
    u_dash = zeros(n,n);
    u_dash(:,1) = u;
    
    % Update u
    t0 = 1;
    for j=1:n
        if(j==1) 
            p(j,1) = u_dash(j,j);
            t(j,1) = t0 + p(j,1)^2/D(j,j);
            D_cap(j,j) = D(j,j)/t0;
            beta(j,1) = p(j,1)/(D(j,j)*t(j,1));
        end
        
        if(j~=1) 
            p(j,1) = u_dash(j,j);
            t(j,1) = t(j-1,1) + p(j,1)^2/D(j,j);
            D_cap(j,j) = D(j,j)/t(j-1,1);
            beta(j,1) = p(j,1)/(D(j,j)*t(j,1));
        end
        
        for r=(j+1):1:n
            u_dash(r,j+1) = u_dash(r,j) - p(j,1)*L(r,j);
            L_cap(r,j) = L(r,j) + beta(j,1)*u_dash(r,1);
        end
    end
    B = L_cap*D_cap*L_cap'; 
    
    L = L_cap;
    D = D_cap;
    v_dash = zeros(n,n+1);
    D_cap = ones(n,n);
    L_cap = ones(n,n);
    % Update v
    p = L\v;
    t(n+1,1) = 1-p'*inv(D)*p;
    if(t(n+1,1)<0.01) t(n+1,1) = 0.01; end
    v_dash(:,n) = v;
    
    for j=n:-1:1
        t(j,1) = t(j+1,1) + p(j,1)^2/D(j,j);
        D_cap(j,j) = D(j,j)*t(j+1,1)/t(j,1);
        beta(j,1) = -p(j,1)/(D(j,j)*t(j+1,1));
        v_dash(j,j) = p(j,1);
        if (j~=n)
            for r=(j+1):1:n
                L_cap(r,j) = L(r,j)+beta(j,1)*v_dash(r,j+1);
                v_dash(r,j) = v_dash(r,j+1) + p(j,1)*L(r,j);
            end
        end
    end
    B = L_cap*D_cap*L_cap';
end

function [H_dash,flag] = update_H(H,dx,dg)
    flag = 1;
    n = size(dx,1);
    rho = 1/ (dg'*dx);
    if (rho<=0) flag=0; return; end
    H_dash = (eye(n) - (rho*(dx*dg'))) * H * (eye(n) - rho*(dg*dx')) + rho*(dx*dx');
end