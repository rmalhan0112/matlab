%% Problem-2 Simulated Annealing

clc;
clear;

%% Answer-a

%Defining the reference matrix:

ref=[3.50000002;0.70000000;17;7.30000009;7.80000000;3.35021468;5.28668325];

%Taking initial values within the bounds
xi=[3,0.75,20,7.5,7.5,3.4,5.3];  %Initial Assumed values
lb=[2.6,0.7,17,7.3,7.3,2.9,5];
ub=[3.6,0.8,28,8.3,8.3,3.9,5.5];
options=saoptimset('TemperatureFcn',{@temperatureboltz},'MaxIter',2200,...
    'PlotFcns',{@saplotbestx,...
          @saplotbestf,@saplotx,@saplotf});
A=@constr;
disp('xfinal is the final values of x and fmin is the final value of function')
[xfinal,fmin]=simulannealbnd(A,xi,lb,ub,options)

%Finding the value of the gradients:

[gradient]=grad(xfinal);
[refgradient]=grad(ref);

%Comparison of the values:

rowname1={'x1','x2','x3','x4','x5','x6','x7'};
table(xfinal',ref,'Rownames',rowname1)
rowname2={'g1','g2','g3','g4','g5','g6','g7','g8','g9','g10','g11','g12',...
    'g13','g14','g15','g16','g17','g18','g19','g20','g21','g22','g23',...
    'g24','g25'};
table(gradient,refgradient,'Rownames',rowname2)

function Pseudo = constr(x)

%Defining the value of rho

Rho=1e10;

%Defining the Objective function.
F0=(0.7854*x(1)*(x(2)^2)*(3.3333*x(3)^2+14.9334*x(3)-43.0934)-1.5079*x(1)*...
    (x(6)^2+x(7)^2)+7.477*(x(6)^3+x(7)^3)+0.7854*(x(4)*x(6)^2+x(5)*x(7)^2));

%Defining the constraints

c1 =(27*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-1)))-1;
c2 =(397.5*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-2)))-1;
c3 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(4))^(3))*((x(6))^(-4)))-1;
c4 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(5))^(3))*((x(7))^(-4)))-1;
c5 = (((((745*(x(4))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(16.9*(10^6)))^(0.5))/(110*((x(6))^(3))))-1;
c6 = (((((745*(x(5))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(157.5*(10^6)))^(0.5))/(85*((x(7))^(3))))-1;
c7 = ((x(2)*x(3))/40)-1;
c8 =((x(2)*5)/(x(1)))-1;
c9 = ((x(1))/(12*(x(2))))-1;
c10 = ((((1.5*(x(6)))+1.9))*((x(4))^(-1)))-1;
c11 = ((((1.1*(x(7)))+1.9))*((x(5))^(-1)))-1;

%Defining the Pseudo Function

Pseudo= F0 + Rho*((max(0,c1))^2+(max(0,c2))^2+(max(0,c3))^2+(max(0,c4))^2+...
    (max(0,c5))^2+(max(0,c6))^2+(max(0,c7))^2+(max(0,c8))^2+(max(0,c9))^2+...
    (max(0,c10))^2+(max(0,c11))^2);

end


function g = grad(x)
c1 =(27*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-1)))-1;
c2 =(397.5*((x(1))^(-1))*((x(2))^(-2))*((x(3))^(-2)))-1;
c3 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(4))^(3))*((x(6))^(-4)))-1;
c4 = (1.93*((x(2))^(-1))*((x(3))^(-1))*((x(5))^(3))*((x(7))^(-4)))-1;
c5 = (((((745*(x(4))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(16.9*(10^6)))^(0.5))/(110*((x(6))^(3))))-1;
c6 = (((((745*(x(5))*((x(2))^(-1))*((x(3))^(-1)))^(2))+(157.5*(10^6)))^(0.5))/(85*((x(7))^(3))))-1;
c7 = ((x(2)*x(3))/40)-1;
c8 =((x(2)*5)/(x(1)))-1;
c9 = ((x(1))/(12*(x(2))))-1;
c10 = 2.6-x(1);
c11 = x(1)-3.6;
c12 = 0.7-x(2);
c13 = x(2)-0.8;
c14 = 17-x(3);
c15 = x(3)-28;
c16 = 7.3-x(4);
c17 = x(4)-8.3;
c18 = 7.3-x(5);
c19 = x(5)-8.3;
c20 = 2.9-x(6);
c21 = x(6)-3.9;
c22 = 5-x(7);
c23 = x(7)-5.5;
c24 = ((((1.5*(x(6)))+1.9))*((x(4))^(-1)))-1;
c25 = ((((1.1*(x(7)))+1.9))*((x(5))^(-1)))-1;

g=[c1;c2;c3;c4;c5;c6;c7;c8;c9;c10;c11;c12;c13;c14;c15;c16;c17;c18;c19;c20;c21;c22;c23;c24;c25];

end
