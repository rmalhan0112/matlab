function y=recursion(x)
if x>0
    x
    y=recursion(x-1);
else
    y=0;
end