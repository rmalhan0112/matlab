%% Rapidly Exploring Random Tree (RRT)

% This code is the implementation of RRT algorithm in a 2D obstacle space
% for a rectangular robot having three motion primitives. X,Y,theta.
clear
clc

%Declarations
node = [];
%Space Dimensions
x_len = 1000;
y_len = 1000;
theta_len = pi;
%Object Dimensions
length = 20;     % Along Y
width = 40;      % Along X

%Initialize
n_strt = [ 50,50,0 ];
n_goal = [ 850,900,pi/2 ];
figure(1)
xlim([0 x_len]);
ylim([0 y_len]);
hold on
vert_obj = draw_rect( n_strt(1,:), length, width );
plot_rect( vert_obj, 'r' )
vert_obj = draw_rect( n_goal(1,:), length, width );
plot_rect( vert_obj, 'k' )
xlabel('X value');
ylabel('Y value');


%% Algorithm
% Creation of Random Samples
tic
for(i=1:1:1e3)
    x = randsample( 1:0.1:x_len,1 );
    y = randsample( 1:0.1:y_len,1 );
    theta = randsample( 0:0.1:theta_len,1 );
    node = [ node; [x,y,theta] ];
    vert_obj = draw_rect( node(i,:), length, width );
    plot_rect( vert_obj, 'b' )
    pause(0.01)
end
toc



%% Functions

function p =  create_space()
    A=[0 0 10 200];B=[10 130 60 10];C=[10 190 60 10];D=[70 170 60 30];
    E=[130 190 70 10];F=[70 130 60 20];G=[190 130 10 60];H=[170 70 30 60];
    I=[130 70 20 60];J=[190 0 10 70];K=[10 0 180 10];L=[10 10 120 120];
    
    
end

function points = draw_rect(primitive, a, b)
    x = primitive(1,1);
    y = primitive(1,2);
    t = primitive(1,3);
    
    R = [ cos(t),-sin(t);
           sin(t),cos(t) ];
    T(1:2,1:2) = R;
    T(1,3) = x;
    T(2,3) = y;
    T(3,3) = 1;
    
% Create Rectangle at origin and transform points.
    p1 = T*[a/2;b/2;1];
    p2 = T*[-a/2;b/2;1];
    p3 = T*[-a/2;-b/2;1];
    p4 = T*[a/2;-b/2;1];
    
    points = [ p1(1,1),p1(2,1);
               p2(1,1),p2(2,1);
               p3(1,1),p3(2,1);
               p4(1,1),p4(2,1);];
end



function plot_rect( vert_obj, color )
    chain_x = [ vert_obj(1,1),vert_obj(2,1),vert_obj(3,1),vert_obj(4,1),vert_obj(1,1) ];
    chain_y = [ vert_obj(1,2),vert_obj(2,2),vert_obj(3,2),vert_obj(4,2),vert_obj(1,2) ];
    plot(chain_x,chain_y,color,'Linewidth',2);
end