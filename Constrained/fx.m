function [f] = fx(x,lambda)
%This function compute the value of the objective at given point
f=x(7)+lambda*(max(0,x(3)+x(4)-x(7))+max(0,x(5)+x(6)-x(7))+max(0,-x(3))+max(0,-x(4))+max(0,-x(5))+max(0,-x(6))...
    +abs(x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4))+abs(x(1)-14*x(2)+x(2)^2+x(2)^3-29-x(5)+x(6)));


end

