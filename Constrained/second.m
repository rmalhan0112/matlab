%% Explains how to run an ode system
% simulation parameters

% initial condition
x0 = [0.1; 0.1]; 
% simulation interval
Tspan = 0:0.05:10;

options = odeset('RelTol',1e-6,'AbsTol',1e-9);

figure()
hold on
% for a= -1:0.05:-0.01 
a=0
tic
[T,Y] = ode45(@example,Tspan,x0,options,a);
toc 
plot(Y(:,1),Y(:,2))
% end

% plot states in the phase plane
xlabel('x');
ylabel('y');
title('x-y graph, alpha is zero');

% % plot 1st state vs time
% plot(T,Y(:,1))
% 
% % plot 2nd state vs time
% plot(T,Y(:,2))