function [ xout,fout,flag,lambda ] = instance7approx1( x,Bk )
% this is the approximation 1 for instance 7
n=7;
% f1=x0(1)-2*x0(2)+5*x0(2)^2-x0(2)^3-13;
% f2=x0(1)-14*x0(2)+x0(2)^2+x0(2)^3-29;
% x=zeros(n,1);
% x(1)=x0(1);
% x(2)=x0(2);
% if f1>0
%     x(3)=f1;
% else
%     x(4)=-f1;
% end
% if f2>0
%     x(5)=f2;
% else
%     x(6)=-f2;
% end
% x(7)=max(x(3:6));


% d=zeros(7,1);
H=Bk;
f=[0 0 0 0 0 0 1]';
A=zeros(6,7);
A(1,3)=1;
A(1,4)=1;
A(1,7)=-1;
A(2,5)=1;
A(2,6)=1;
A(2,7)=-1;
A(3,3)=-1;
A(4,4)=-1;
A(5,5)=-1;
A(6,6)=-1;

Aeq=zeros(2,7);
Aeq(1,1)=1;
Aeq(1,2)=-2+10*x(2)-3*x(2)^2;
Aeq(1,3)=-1;
Aeq(1,4)=1;
Aeq(2,1)=1;
Aeq(2,2)=-14+2*x(2)+3*x(2)^2;
Aeq(2,5)=-1;
Aeq(2,6)=1;
Aeq
b=zeros(6,1);
b(1)=-x(3)-x(4)+x(7);
b(2)=-x(5)-x(6)+x(7);
b(3:6)=x(3:6);
beq=zeros(2,1);
beq(1)=-(x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4));
beq(2)=-(x(1)-14*x(2)+x(2)^2+x(2)^3-29-x(5)+x(6));

[xout,fout,flag,~,lambda] = quadprog(H,f,A,b,Aeq,beq);










end

