

x0 = [-0.5,-2];

iter=0;iter1=0;
n=7;
epsilon=10^(-5);
f1=x0(1)-2*x0(2)+5*x0(2)^2-x0(2)^3-13;
f2=x0(1)-14*x0(2)+x0(2)^2+x0(2)^3-29;
x=zeros(n,1);
x(1)=x0(1);
x(2)=x0(2);
if f1>0
    x(3)=f1;
else
    x(4)=-f1;
end
if f2>0
    x(5)=f2;
else
    x(6)=-f2;
end
x(7)=max(x(3:6));

% we have the first intital point to start our problem

Bc=eye(7);
xc=x;
muc=zeros(n+1,1);
muplus=zeros(n+1,1);
dc=xc;

[dout,~,flag,lambda]=instance7approx1(xc,Bc)
if flag==1
    eqc=lambda.eqlin;
    ineqc=lambda.ineqlin;
    muplus(1:6)=max(muc(1:6),ineqc);
    muplus(7:8)=max(muc(7:8),abs(eqc));
    dc=dout;
else
    [dout,~,~,~]=instance7approx2(xc,Bc,muc);
    
    dc=dout;
end
muc=muplus;
while dc'*dc>epsilon
    iter1=iter1+1;
    iter=iter+1;
    dc'*dc
    mu=max(muc)
    xplus=instance7linesearch(xc,dc,mu);
    xplus(1:2)
    deltak=xplus-xc;
    gammak=zeros(7,1);
    gammak(2)=muc(7)*(10*(xplus(2)-xc(2))-3*(xplus(2)^2-xc(2)^2))+muc(8)*...
        (2*(xplus(2)-xc(2))+3*(xplus(2)^2-xc(2)^2));
    if deltak'*gammak>=0.2*deltak'*Bc*deltak
        thetak=1;
    else
        thetak=(0.8*deltak'*Bc*deltak)/(deltak'*Bc*deltak-deltak'*gammak);
    end
    rk=thetak*gammak+(1-thetak)*Bc*deltak;
    Bc=Bc-(Bc*(deltak*deltak')*Bc)/(deltak'*Bc*deltak)+(rk*rk')/(deltak'*rk);
    Bc=(Bc+Bc')/2;
%     det(Bc)

    if iter1==10
        Bc=eye(7);
        iter1=0;
    end
        
        
    xc=xplus
    xc(1:2,1)
    [dout,~,flag,lambda]=instance7approx1(xc,Bc);
    if flag==1
        eqc=lambda.eqlin;
        ineqc=lambda.ineqlin;
        muplus(1:6)=max(muc(1:6),ineqc);
        muplus(7:8)=max(muc(7:8),abs(eqc));
        dc=dout;
    else
        [dout,~,~,~]=instance7approx2(xc,Bc,muc);
        
        dc=dout;
    end
    muc=muplus;
    


end

iter

% iter=1


dc'*dc


