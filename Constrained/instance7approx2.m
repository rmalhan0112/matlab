function [ xout,fout,flag,lambda ] = instance7approx2(x,Bk,mu)
% this is the approximation 2 for instance 7 
n=17;
% 
% lambda = 
% 
%   struct with fields:
% 
%     ineqlin: [6�1 double]
%       eqlin: [2�1 double]
%       lower: [17�1 double]
%       upper: [17�1 double]
% 


% xout=zeros(17,1);
% order
% d1 d2 d3 d4 d5 d6 d7 u1 u2 u3 u4 u5 u6 vp1 vm1 vp2 vm2
% 01 02 03 04 05 06 07 08 09 10 11 12 13 14  15  16  17
% mu=ones(8,1);
% Bk=eye(7);

% f1=x0(1)-2*x0(2)+5*x0(2)^2-x0(2)^3-13;
% f2=x0(1)-14*x0(2)+x0(2)^2+x0(2)^3-29;
% x=zeros(n,1);
% x(1)=x0(1);
% x(2)=x0(2);
% if f1>0
%     x(3)=f1;
% else
%     x(4)=-f1;
% end
% if f2>0
%     x(5)=f2;
% else
%     x(6)=-f2;
% end
% x(7)=max(x(3:6));
% 

% d=zeros(7,1);
H=zeros(17,17);

H(1:7,1:7)=Bk;

f=[0 0 0 0 0 0 1 mu(1:7)' mu(7) mu(8) mu(8)]';
lb=zeros(17,1);
lb(1:7)=inf*(-1)*ones(7,1);
A=zeros(6,17);
A(1,3)=1;
A(1,4)=1;
A(1,7)=-1;
A(1,8)=-1;

A(2,5)=1;
A(2,6)=1;
A(2,7)=-1;
A(2,9)=-1;

A(3,3)=-1;
A(3,10)=-1;

A(4,4)=-1;
A(4,11)=-1;

A(5,5)=-1;
A(5,12)=-1;

A(6,6)=-1;
A(6,13)=-1;

Aeq=zeros(2,17);
Aeq(1,1)=1;
Aeq(1,2)=-2+10*x(2)-3*x(2)^2;
Aeq(1,3)=-1;
Aeq(1,4)=1;
Aeq(1,14)=-1;
Aeq(1,15)=1;

Aeq(2,1)=1;
Aeq(2,2)=-14+2*x(2)+3*x(2)^2;
Aeq(2,5)=-1;
Aeq(2,6)=1;
Aeq(2,16)=-1;
Aeq(2,17)=1;

b=zeros(6,1);
b(1)=-x(3)-x(4)+x(7);
b(2)=-x(5)-x(6)+x(7);
b(3:6)=x(3:6);
beq=zeros(2,1);
beq(1)=-(x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4));
beq(2)=-(x(1)-14*x(2)+x(2)^2+x(2)^3-29-x(5)+x(6));
[xout1,fout,flag,~,lambda] = quadprog(H,f,A,b,Aeq,beq,lb,[]);
if flag ==1
xout=xout1(1:7);
end






end

