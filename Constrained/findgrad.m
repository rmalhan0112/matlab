function [fdx] = findgrad( x,d,lambda )

% Each element of the gradient is computed as max(ci(x),0) where each
% element of gradient is ci(x) if ci(x)>0 or 0 if it is less than 0. 

ingr=zeros(9,1);
ingr(1)=d(1);




if x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4)>0
    ingr(8)=d(1)+d(2)*(-2+10*x(2)-3*x(2)^2)-d(3)+d(4);
elseif x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4)<0
    ingr(8)=-(d(1)+d(2)*(-2+10*x(2)-3*x(2)^2)-d(3)+d(4));
else
    if d(1)+d(2)*(-2+10*x(2)-3*x(2)^2)-d(3)+d(4)>0
        ingr(8)=d(1)+d(2)*(-2+10*x(2)-3*x(2)^2)-d(3)+d(4);
    else 
        ingr(8)=-(d(1)+d(2)*(-2+10*x(2)-3*x(2)^2)-d(3)+d(4));
    end
     
end


if x(1)-14*x(2)+x(2)^2+x(2)^3-29+x(5)+x(6)>0
    ingr(9)=d(1)+d(2)*(-14+2*x(2)+3*x(2)^2)-d(5)+d(6);
elseif x(1)-14*x(2)+x(2)^2+x(2)^3-29+x(5)+x(6)<0
    ingr(9)=-(d(1)+d(2)*(-14+2*x(2)+3*x(2)^2)-d(5)+d(6));
else
        if d(1)+d(2)*(-14+2*x(2)+3*x(2)^2)-d(5)+d(6)>0
            ingr(9)=d(1)+d(2)*(-14+2*x(2)+3*x(2)^2)-d(5)+d(6);
        else 
            ingr(9)=-(d(1)+d(2)*(-14+2*x(2)+3*x(2)^2)-d(5)+d(6));
        end
     
end


if x(5)+x(6)-x(7)>0
    ingr(3)=d(5)+d(6)-d(7);
else
    if x(5)+x(6)-x(7)==0
        
        if d(5)+d(6)-d(7)>0
            ingr(3)=d(5)+d(6)-d(7);
        end
    end 
end




if -x(3)>0
    ingr(4)=-d(3);
else
    if -x(3)==0
        
        if -d(3)>0
            ingr(4)=-d(3);
        end
    end 
end

if -x(4)>0
    ingr(5)=-d(4);
else
    if -x(4)==0
        
        if -d(4)>0
            ingr(5)=-d(4);
        end
    end 
end

if x(3)+x(4)-x(7)>0
    ingr(2)=d(3)+d(4)-d(7);
else
    if x(3)+x(4)-x(7)==0
        
        if d(3)+d(4)-d(7)>0
            ingr(2)=d(3)+d(4)-d(7);
        end
    end 
end


if -x(5)>0
    ingr(6)=-d(5);
else
    if -x(5)==0
        
        if -d(5)>0
            ingr(6)=-d(5);
        end
    end 
end


if -x(6)>0
    ingr(7)=-d(6);
else
    if -x(6)==0
        
        if -d(6)>0
            ingr(7)=-d(6);
        end
    end 
end



fdx=lambda*sum(ingr(2:9))+ingr(1);    
    
    
end