%% Freudenstein-Roth Function Constrained Optimization

clear all;
clc;
close all;

global f_eval;
f_eval = 0;
% Take the initial Point
x_int = [-0.5;-2];

% Parameters for the Optimization
N = 7;          % Dimension
tol = 1e-6;     % Tolerance
count = 0;      % BFGS update reset counter
counter = 0;    % Iteration Counter

% Initializations
x_c = zeros(7,1);           % A 7 dimensional Vector of the form [x1,x2,u1,w1,u2,w2,z]
B = eye(N);               % BFGS hessian B

% Initial Point for the tranformed problem
x_c(1,1) = x_int(1,1);
x_c(2,1) = x_int(2,1);

f1_eval = x_int(1,1) - 2 * x_int(2,1) + 5 * x_int(2,1)^2 - x_int(2,1)^3 - 13;
f2_eval = x_int(1,1) - 14 * x_int(2,1) + x_int(2,1)^2 + x_int(2,1)^3 - 29;
if (f1_eval>0) x_c(3,1) = f1_eval;
else x_c(4,1) = -f1_eval;   
end
if (f2_eval>0) x_c(5,1) = f2_eval;
else x_c(6,1) = -f2_eval;
end

x_c(7,1) = max( abs(f1_eval),abs(f2_eval) );    % Objective Function variable z transformed   
Mu_cons = zeros(8,1);     % Mu values for constraints



dir_vec = ones(7,1);
%% Algorithm
tic
while (norm(dir_vec)^2 > tol)
    count = count + 1;
    counter = counter + 1;
    fprintf("Iteration Reached:  %d\n",counter);
    
    % Storing the data into matrix for display
    iterates(counter,:) = [ counter,x_c(1,1),x_c(2,1),func_eval(x_c(1:2,1)) ];
    
    % Sending the Instance to QP solver and getting a feasible direction of
    % descent First approximate is QP transformation of the Objective and
    % seconds approximation is the Penalty function approximation of the objective.
    
    [ dir_vec,~,flag,lamdas ] = func_approx_1( x_c,B );
    % If Solution is not found by the First approximation, shift to second
    % approximation by the penalty function
    if flag==1
        eq_lamdas = lamdas.eqlin;
        ineq_lamdas = lamdas.ineqlin;
        Mu_cons(1:6)=max(Mu_cons(1:6),ineq_lamdas);
        Mu_cons(7:8)=max(Mu_cons(7:8),abs(eq_lamdas));
    else
        [ dir_vec,~,flag,lamdas ] = func_approx_2( x_c,B,Mu_cons );
    end
    
    % Line Search principle using Armijo Search
    Muline_search = max(Mu_cons);
    x_p = armijo_search( Muline_search,x_c,dir_vec );
    % Update sk and gammak
    dx = x_p - x_c;
    dg = zeros(7,1);
    dg(2,1) = Mu_cons(7,1)*(10*(x_p(2,1)-x_c(2,1))-3*(x_p(2,1)^2-x_c(2,1)^2))+Mu_cons(8,1)*...
        (2*(x_p(2,1)-x_c(2,1))+3*(x_p(2,1)^2-x_c(2,1)^2));
    if dx'*dg >= 0.2*dx'*B*dx
        thetak = 1;
    else
        thetak = (0.8*dx'*B*dx)/(dx'*B*dx-dx'*dg);
    end
    rk = thetak*dg+(1-thetak)*B*dx;
    % Update BFGS hessian B
    B = B - (B*(dx*dx')*B)/(dx'*B*dx)+(rk*rk')/(dx'*rk);
    B = (B+B')/2;
    
    if count==10
        B = eye(7);
        count=0;
    end    
    x_c = x_p;
end
toc

fprintf("\nOptimum Point is: \n%f\n%f\n",x_c(1,1),x_c(2,1));
fprintf("\nObjective Function Value at this point is: %f\n",func_eval( x_c(1:2,1) ));
fprintf("Number of Function evaluations:  %d\n",f_eval);
fprintf("Number of Iterations:  %d\n",counter);

disp("Iteration Number, x1, x2 and Max(|f1|,|f2|)");
disp(iterates)







%% Functions used

function [ dir_nxt,fout,flag,lamdas ] = func_approx_1( x_c,B )

    global f_eval;
% Finds a minimum for a problem specified by
% min0.5*x.transpose * H * x + f.transpose * x
% 
% such that:
% A*x <= b
% Aeq*x = beq
% lb <= x <= ub % lb and ub are lower and upper bounds respectively
% H, A, and Aeq are matrices, and f, b, beq, lb, ub, and x are vectors.

    % f in the equation
    f = [0;0;0;0;0;0;1];
    
    % A in the equation
    ineq_cons_coeff = [ 0,0,1,1,0,0,-1;
                        0,0,0,0,1,1,-1;
                        0,0,-1,0,0,0,0;
                        0,0,0,-1,0,0,0;
                        0,0,0,0,-1,0,0;
                        0,0,0,0,0,-1,0;
                        ];
    
    % Aeq in the equation
    eq_cons_coeff = [   1,-2+10*x_c(2,1)-3*x_c(2,1)^2,-1,1,0,0,0;
                        1,-14+2*x_c(2,1)+3*x_c(2,1)^2,0,0,-1,1,0;
                        ];
                    
    
    % b in the equation
    b = [ -x_c(3,1)-x_c(4,1)+x_c(7,1);
          -x_c(5,1)-x_c(6,1)+x_c(7,1);
          x_c(3,1);
          x_c(4,1);
          x_c(5,1);
          x_c(6,1);];
      
    %beq in the equation
    beq = [ -(x_c(1,1)-2*x_c(2,1)+5*x_c(2,1)^2-x_c(2,1)^3-13-x_c(3,1)+x_c(4,1));
            -(x_c(1,1)-14*x_c(2,1)+x_c(2,1)^2+x_c(2,1)^3-29-x_c(5,1)+x_c(6,1));];
    opts1=  optimset('display','off');
    [ dir_nxt,fout,flag,~,lamdas ] = quadprog( B,f,ineq_cons_coeff,b,eq_cons_coeff,beq,[],[],[],opts1);
end


function [ dir_nxt,fout,flag,lamdas ] = func_approx_2( x,B,Mu_cons )
    global f_eval;
    n = 17;
    H = zeros(17,17);

    H(1:7,1:7) = B;

    f = [ 0;0;0;0;0;0;1;Mu_cons(1,1);Mu_cons(2,1);Mu_cons(3,1);Mu_cons(4,1);...
                    Mu_cons(5,1);Mu_cons(6,1);Mu_cons(7,1);Mu_cons(7,1);Mu_cons(8,1);Mu_cons(8,1) ];
    % Defining the Coefficient matrix for inequality constraints
    A=zeros(6,17);
    A(1,3) = 1;
    A(1,4) = 1;
    A(1,7) = -1;
    A(1,8) = -1;
    A(2,5) = 1;
    A(2,6) = 1;
    A(2,7) = -1;
    A(2,9) = -1;
    A(3,3) = -1;
    A(3,10) = -1;
    A(4,4) = -1;
    A(4,11) = -1;
    A(5,5) = -1;
    A(5,12) = -1;
    A(6,6) = -1;
    A(6,13) = -1;
    
    % Defining the Coefficient matrix for equality constraints
    Aeq=zeros(2,17);
    Aeq(1,1) = 1;
    Aeq(1,2) = -2+10*x(2)-3*x(2)^2;
    Aeq(1,3) = -1;
    Aeq(1,4) = 1;
    Aeq(1,14) = -1;
    Aeq(1,15) = 1;
    Aeq(2,1) = 1;
    Aeq(2,2) = -14+2*x(2)+3*x(2)^2;
    Aeq(2,5) = -1;
    Aeq(2,6) = 1;
    Aeq(2,16) = -1;
    Aeq(2,17) = 1;
    
    % Defining the b and beq
    b=zeros(6,1);
    b(1,1) = -x(3,1)-x(4,1)+x(7,1);
    b(2,1) = -x(5,1)-x(6,1)+x(7,1);
    b(3:6) = x(3:6);
    
    beq=zeros(2,1);
    beq(1)=-(x(1)-2*x(2)+5*x(2)^2-x(2)^3-13-x(3)+x(4));
    beq(2)=-(x(1)-14*x(2)+x(2)^2+x(2)^3-29-x(5)+x(6));
    
    % Defining the lower bounds on equations
    lb = zeros(17,1);
    lb(1:7) = inf*(-1)*ones(7,1);
    opts1=  optimset('display','off');
    [ dir_nxt,fout,flag,lamdas ] = quadprog(H,f,A,b,Aeq,beq,lb,[],[],opts1);
    dir_nxt = dir_nxt(1:7,1);
end




function [ x_nxt ] = armijo_search( Mu,x_c,dir_vec )
    global f_eval;
    fdx = compute_gradient( Mu,x_c,dir_vec );
    % directional derivative depending on active constraints
    eeta = 2;
    theta = 0.5;
    alpa = 1;
    x_nxt = x_c + alpa*dir_vec;  

    % Objective Function evaluation at x_c
    f_c = fx( x_c,Mu );
    f_eval = f_eval + 1;
    f_c = f_c + theta*alpa*fdx;
    f_nxt = eval_obj( x_nxt,Mu );
    f_eval = f_eval + 1;

    if (f_c < f_nxt)
    % Backtracking
        while (f_c < f_nxt )
            alpa = alpa / eeta;
            x_nxt = x_c + alpa*dir_vec;
            f_c = f_nxt + theta*alpa*fdx;
            f_nxt = eval_obj( x_nxt,Mu );
            f_eval = f_eval + 1;
        end
    else
    % Forward Moving
        while ( f_c >= f_nxt )
            alpa = alpa*eeta;
            x_nxt = x_c + alpa*dir_vec;
            f_c = f_c + theta*alpa*fdx;
            f_nxt = eval_obj( x_nxt,Mu );
            f_eval = f_eval + 1;
        end
        alpa = alpa/eeta;
        x_nxt = x_c + alpa*dir_vec;
    end
end


function gradient = compute_gradient( Mu,x_c,dir_vec )

    % Each element of the gradient is computed as max(ci(x),0) where each
    % element of gradient is ci(x) if ci(x)>0 or 0 if it is less than 0. 

    gradient = zeros(9,1);
    gradient(1,1) = dir_vec(1,1);

    c1 = x_c(3,1)+x_c(4,1)-x_c(7,1);
    if (c1>0)
        gradient(2,1) = dir_vec(3,1)+dir_vec(4,1)-dir_vec(7,1);
    elseif (c1==0)
        if (dir_vec(3,1)+dir_vec(4,1)-dir_vec(7,1)>0)
            gradient(2,1) = dir_vec(3,1)+dir_vec(4,1)-dir_vec(7,1);
        end 
    end

    c2 = x_c(5)+x_c(6)-x_c(7);
    if (c2>0)
        gradient(3,1) = dir_vec(5,1)+dir_vec(6,1)-dir_vec(7,1);
    elseif (c2==0)
        if (dir_vec(5,1)+dir_vec(6,1)-dir_vec(7,1)>0)
            gradient(3,1) = dir_vec(5,1)+dir_vec(6,1)-dir_vec(7,1);
        end
    end

    if (-x_c(3,1)>0)
        gradient(4,1) = -dir_vec(3,1);
    elseif (-x_c(3,1)==0)
            if (-dir_vec(3,1)>0)
                gradient(4,1) = -dir_vec(3,1);
            end
    end

    if (-x_c(4,1)>0)
        gradient(5,1) = -dir_vec(4,1);
    elseif (-x_c(4,1)==0)
        if (-dir_vec(4,1)>0)
            gradient(5,1) = -dir_vec(4,1);
        end
    end

    if (-x_c(5,1)>0)
        gradient(6,1) = -dir_vec(5,1);
    elseif (-x_c(5,1)==0)        
        if (-dir_vec(5,1)>0)
            gradient(6,1) = -dir_vec(5,1);
        end
    end

    if (-x_c(6,1)>0)
        gradient(7,1) = -dir_vec(6,1);
    elseif (-x_c(6,1)==0)
        if (-dir_vec(6,1)>0)
            gradient(7,1) = -dir_vec(6,1);
        end 
    end

    c8 = x_c(1,1)-2*x_c(2,1)+5*x_c(2,1)^2-x_c(2,1)^3-13-x_c(3,1)+x_c(4,1);
    if (c8>0)
        gradient(8,1) = dir_vec(1,1)+dir_vec(2,1)*(-2+10*x_c(2,1)-3*x_c(2,1)^2)-dir_vec(3,1)+dir_vec(4,1);
    elseif (c8<0)
        gradient(8,1) = -(dir_vec(1,1)+dir_vec(2,1)*(-2+10*x_c(2,1)-3*x_c(2,1)^2)-dir_vec(3,1)+dir_vec(4,1));
    elseif (dir_vec(1,1)+dir_vec(2,1)*(-2+10*x_c(2)-3*x_c(2)^2)-dir_vec(3,1)+dir_vec(4,1)>0)
        gradient(8,1) = dir_vec(1,1)+dir_vec(2,1)*(-2+10*x_c(2,1)-3*x_c(2,1)^2)-dir_vec(3,1)+dir_vec(4,1);
    else 
        gradient(8,1) = -(dir_vec(1,1)+dir_vec(2,1)*(-2+10*x_c(2,1)-3*x_c(2,1)^2)-dir_vec(3,1)+dir_vec(4,1));

    end


    if (x_c(1,1)-14*x_c(2,1)+x_c(2,1)^2+x_c(2,1)^3-29+x_c(5,1)+x_c(6,1)>0)
        gradient(9) = dir_vec(1,1)+dir_vec(2,1)*(-14+2*x_c(2,1)+3*x_c(2,1)^2)-dir_vec(5,1)+dir_vec(6,1);
    elseif (x_c(1,1)-14*x_c(2,1)+x_c(2,1)^2+x_c(2,1)^3-29+x_c(5,1)+x_c(6,1)<0)
        gradient(9) = -(dir_vec(1,1)+dir_vec(2,1)*(-14+2*x_c(2,1)+3*x_c(2,1)^2)-dir_vec(5,1)+dir_vec(6,1));
    elseif (dir_vec(1,1)+dir_vec(2,1)*(-14+2*x_c(2,1)+3*x_c(2,1)^2)-dir_vec(5,1)+dir_vec(6,1)>0)
        gradient(9,1) = dir_vec(1,1)+dir_vec(2,1)*(-14+2*x_c(2)+3*x_c(2)^2)-dir_vec(5,1)+dir_vec(6,1);
    else 
        gradient(9,1) = -(dir_vec(1,1)+dir_vec(2,1)*(-14+2*x_c(2,1)+3*x_c(2,1)^2)-dir_vec(5,1)+dir_vec(6,1));
    end
    
    gradient = Mu*sum(gradient(2:9))+gradient(1,1);
end
    
function [f] = eval_obj( x,Mu )
    f = x(7,1)+Mu*(max(0,x(3,1)+x(4,1)-x(7,1))+max(0,x(5,1)+x(6,1)-x(7,1))+max(0,-x(3,1))+max(0,-x(4,1))+max(0,-x(5,1))+max(0,-x(6,1))...
        +abs(x(1,1)-2*x(2,1)+5*x(2,1)^2-x(2,1)^3-13-x(3,1)+x(4,1))+abs(x(1,1)-14*x(2,1)+x(2,1)^2+x(2,1)^3-29-x(5,1)+x(6,1)));
end

function eval  = func_eval( x )
    f1_eval = x(1,1) - 2 * x(2,1) + 5 * x(2,1)^2 - x(2,1)^3 - 13;
    f2_eval = x(1,1) - 14 * x(2,1) + x(2,1)^2 + x(2,1)^3 - 29;
    
    eval = max(abs(f1_eval),abs(f2_eval));
end