function [xnew] = instance7linesearch(xin,din,lambda)
% This is a function to compute a new point in Armijo's line search method

% xin=x;
% this computes the gradient
% gx= findgrad(xin,alpha);
display('line search')
fdx=findgrad(xin,din,lambda)
% directional derivative depending on active constraints
theta=0.5;
eta=2;
alp=1;
% to compute the change along the input direction
% i=1;
% din
% disp(['Search Direction:  ',num2str(din)]);
% disp(din);
xnew=xin+alp*din;  
% evaluates the objective function at xin
fin=fx(xin,lambda);
flin=fin+theta*alp*fdx;
fnew=fx(xnew,lambda);
% i=1;
if flin<fnew
%     disp('Method: backward');
    while flin<fnew
%         i=i+1
        alp=alp/eta;
        xnew=xin+alp*din;
        flin=fin+theta*alp*fdx;
        fnew=fx(xnew,lambda);
    end
%     disp(['Final point x:  ',num2str(xnew)]);
else
%         disp('Method: forward');
        while flin>=fnew
%         i=i+1
        alp=alp*eta;
        xnew=xin+alp*din;
        flin=fin+theta*alp*fdx;
        fnew=fx(xnew,lambda);
        end
        alp=alp/eta;
        xnew=xin+alp*din;
end
%     disp(['Final point x:  ',num2str(xnew)]);
end
