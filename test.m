
clear all;
clc
syms x1;
syms x2;
syms x3;
syms x4;
syms a;
global a
a= 100;
% f = ( ( a*(x2-x1^2)^2 + (1-x1)^2) + ( a*(x4-x3^2)^2 + (1-x3)^2));
% t1 = diff(f,x1)
% t2 = diff(f,x2)
% t3 = diff(f,x3)
% t4 = diff(f,x4)
% 
% [ diff(t1,x1),diff(t1,x2),diff(t1,x3),diff(t1,x4);
%     diff(t2,x1),diff(t2,x2),diff(t2,x3),diff(t2,x4);
%     diff(t3,x1),diff(t3,x2),diff(t3,x3),diff(t3,x4);
%     diff(t4,x1),diff(t4,x2),diff(t4,x3),diff(t4,x4)]

X = [1;5;4;-2];
% A = [ 5,5,2;3,5,4;5,6,9 ]
% [U,S,V] = svd(A)
% U*S*V'
% eig(A)
% % func(X)
% % norm(gradient(X))
gradient(X)
h = hessian(X)
% [L,D] = ldl(A)
% L*D*L'

%% Function Definitions

% Function Handle
function y = func(X)
    global a;
    n = size(X,1);
    n = n/2;
    y = 0;
    for i=1:1:n
        y = y + ( a*( X(2*i,1)-X(2*i-1,1)^2 )^2 + ( 1-X(2*i-1,1) )^2 );
    end
end

% Gradient
function g = gradient(X)
    global a;
    n = size(X,1);
%     n = n/2;
    for i=1:2:n
        g(i,1) = ( -2*a*( X(i+1,1)-X(i,1)^2 )*( 2*X(i,1) ) - 2*( 1-X(i,1) ) );
        g(i+1,1) = 2*a*( X(i+1,1)-X(i,1)^2 );
    end
end

% Hessian
function h = hessian(X)
    global a;
    n = size(X,1);
    for i=1:1:n
        for j=1:1:n
            % If row is odd
            if (mod(i,2)~=0)
                if (i==j) h(i,j)= -4*a*(X(i+1,1)-X(i,1)^2) + 8*a*X(i,1)^2 + 2; end
                if (j==i+1) h(i,j)= -4*a*X(i,1); end
                if (i~=j && j~=i+1) h(i,j)= 0; end
            end
            
            % If row is even
            if (mod(i,2)==0) 
                if (i==j) h(i,j)= 2*a; end
                if (j==i-1) h(i,j)= -4*a*X(i-1,1); end
                if (i~=j && j~=i-1) h(i,j)= 0; end
            end
        end
    end
end