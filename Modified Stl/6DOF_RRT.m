%% This code is used to view the stls of different links of robot.
clear
clc
close all;

F = zeros(6,6);
F(1,:) = [0,0,0,0,0,0];
F(2,:) = [0,0,196.5,0,0,0];
F(3,:) = [0,100,123.5,0,90,0];
F(4,:) = [-250,0,0,0,0,0];
F(5,:) = [0,0,0,90,0,0];
F(6,:) = [0,0,-250,-90,0,0];
F(7,:) = [0,65,0,90,0,0];

% Choose what color the CAD part needs to be displayed.
col_matrix = [0.941176, 0.972549, 1];
[v, f, n, c, stltitle] = stlread('C3_BASE_S.STL');
figure('units','normalized','outerposition',[0 0 1 1])
view(-100,5)
patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
hold on;

% Placing Obstacle in system:
obs_v = plot_obs();
collision = 0;

for(j=-10:5:20)
    T = [1,0,0,0;
     0,1,0,0;
     0,0,1,0;
     0,0,0,1];
 
%     J_rot = [j,0,0,0,0,0];
    J_rot = [j,j*1.2,j*1.1,j*1.4,j,j*1.2];
% J_rot = [40,50,90,-10,20,30];
% J_rot = [20,-10,50,-40,60,10];
tic
    for(i=1:1:6)
        file = ['C3_ARM' num2str(i) '.STL'];
%         file_v2 = ['New_C3_ARM' num2str(i) '.STL'];
        [v, f, n, c, stltitle] = stlread(file);
        Rz(1:3,1:3) = rotz(J_rot(i));
        Rz(4,4) = 1;
        T = T*transf(F(i+1,:))*Rz;
        vertices = zeros( size(v,1),4 );
        vertices(:,1:3) = v;
        vertices(:,4) = 1;
        temp = (T * vertices')'; 
        v(:,1:3) = temp(:,1:3);
%         stlwrite(file_v2, f, v);
        
%Check for Collision
        obj = patch('Faces',f,'Vertices',v);
        collision = GJK(obs_v,obj,10);
        if (collision==1) fprintf('Collision Detected\n'); break; end
        
        patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
    end
    if (collision==1) collision=0; break; end
    X = T(1,4);
    Y = T(2,4);
    Z = T(3,4);
    eul = rotm2eul(T(1:3,1:3));
    A = rad2deg(eul(1,1));
    B = rad2deg(eul(1,2));
    C = rad2deg(eul(1,3));
toc
    fprintf('X,Y,Z,A,B,C: %f %f %f %f %f %f\n', X,Y,Z,A,B,C)
    pause(0.2)
    cla
    [v, f, n, c, stltitle] = stlread('C3_BASE_S.STL');
    patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
    obs = plot_obs();
end
close all

% Tranf function finds the transformation matrix for F1 frame
function T = transf(F1)
    % x,y,z are the three translations in XYZ system
    T(1,4) = F1(1,1);
    T(2,4) = F1(1,2);
    T(3,4) = F1(1,3);
    R = eul2rotm([ deg2rad(F1(1,6)) deg2rad(F1(1,5)) deg2rad(F1(1,4)) ],'ZYX');
    T(1:3,1:3) = R;
    T(4,4) = 1;
end

function obs_v = plot_obs()
    col_matrix = [0.941176, 0.972549, 1];

    T_obs = [ 1,0,0,-200;
          0,1,0,0;
          0,0,1,0;
          0,0,0,1];

    [v, f, n, c, stltitle] = stlread('Obstacle.STL');
    vertices = zeros( size(v,1),4 );
    vertices(:,1:3) = v;
    vertices(:,4) = 1;
    temp = (T_obs * vertices')'; 
    v(:,1:3) = temp(:,1:3);
    obs_v = patch('Faces',f,'Vertices',v);
    patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
end