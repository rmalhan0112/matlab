% circle
C = [0,0]; % [x,y] coordinate of center of the circle
R = 2; % radius of the circle

% line 1
A1 = [0,0]; % [x,y] coordinate of point A1
B1 = [3,3]; % [x,y] coordinate of point B1
% line 2
A2 = [3.5,3.5];
B2 = [5,5];
% line 3
A3 = [-5,2];
B3 = [5,2];
% line 4
A4 = [0,3];
B4 = [3,5];
% line 5
A5 = [-5,-5];
B5 = [-2.5,-2.5];
% line 6
A6 = [-2.5,1.5];
B6 = [0,-3];
% line 7
A7 = [5,1.5];
B7 = [-5,-3];

% check for intersection
    % delta > 0 : no intersection
    % delta = 0 : tangent
    % delta < 0 : intersection
    tic
delta1 = circle_line_intersection(A1, B1, C, R)
    toc
delta2 = circle_line_intersection(A2, B2, C, R)
delta3 = circle_line_intersection(A3, B3, C, R)
delta4 = circle_line_intersection(A4, B4, C, R)
delta5 = circle_line_intersection(A5, B5, C, R)
delta6 = circle_line_intersection(A6, B6, C, R)
delta7 = circle_line_intersection(A7, B7, C, R)

% plot
hold on
pbaspect([1 1 1]);
plot( [A1(1), B1(1)], [A1(2), B1(2)], 'r'); % line 1
plot( [A2(1), B2(1)], [A2(2), B2(2)], 'g'); % line 2
plot( [A3(1), B3(1)], [A3(2), B3(2)], 'b'); % line 3
plot( [A4(1), B4(1)], [A4(2), B4(2)], 'k'); % line 4
plot( [A5(1), B5(1)], [A5(2), B5(2)], 'c'); % line 5
plot( [A6(1), B6(1)], [A6(2), B6(2)], 'm'); % line 6
plot( [A7(1), B7(1)], [A7(2), B7(2)], 'y'); % line 7
viscircles(C,R); % circles

