%% RRT for 6 DOF robot.

clear
clc
close all;

F = zeros(6,6);
F(1,:) = [0,0,0,0,0,0];
F(2,:) = [0,0,196.5,0,0,0];
F(3,:) = [0,100,123.5,0,90,0];
F(4,:) = [-250,0,0,0,0,0];
F(5,:) = [0,0,0,90,0,0];
F(6,:) = [0,0,-250,-90,0,0];
F(7,:) = [0,65,0,90,0,0];

% Choose what color the CAD part needs to be displayed.
col_matrix = [0.941176, 0.972549, 1];
figure('units','normalized','outerposition',[0 0 1 1])
% view(-130,10)
view(0,90)
hold on;

% Placing Obstacle in system:
obs_v = plot_obs();
collision = 0;

% Declarations

n_tree = [];
J_strt = [35,-20,0,0,0,0];
plot_robot(J_strt,[1,0,0],F)

J_goal = [140,-20,0,0,0,0];
plot_robot(J_goal,[0,0,1],F)
count = 0;
idx = 0;
flag=0;

x = input('Press 1 to continue or 2 to cancel\n');
if (x==1)
    disp('Running RRT');
else
    return;
end

n_tree = [n_tree; [J_strt,1]];
%% Algorithm

s = 4;
tic
while(flag==0 && idx<1e4)
    idx = idx + 1;
    if (mod(idx,100)==0) fprintf('%d Iterations reached:\nDistance to Goal: %f\n\n',idx,dist_to_goal); end
    t1 = randsample( -100:s:100,1 );
    t2 = randsample( -160:s:65,1 );
    t3 = randsample( -51:s:100,1 );
    t4 = randsample( -200:s:200,1 );
    t5 = randsample( -135:s:135,1 );
    t6 = randsample( -360:s:360,1 );
    p = randsample(0:0.1:1,1);
    
    J_node = round([t1,t2,t3,t4,t5,t6]);
    
    % If Probaility sampling > 0.9
    if (p<0.9)
        [step,direction,parent] = valid_step( J_node, n_tree,F,obs_v );
    end
    
    % If Probaility sampling < 0.9
    if (p>=0.9)
        [step,direction,parent] = valid_step( J_node, n_tree,F,obs_v );
    end
    if (step~=0)
        count = count + 1;
        inter_node(1,1:6) = n_tree(parent,1:6) + direction(1,:);
        [X1,Y1,Z1] = robot_xy(inter_node,F);
        [X2,Y2,Z2] = robot_xy(n_tree(parent,1:6),F);
        dist_to_goal = norm([X2,Y2,Z2] - [X1,Y1,Z1]);
        
        %STOPPING CONDITION
%         dist_to_goal = norm(J_goal(1,1:6)-inter_node(1,1:6));
        
        if ( dist_to_goal < 10 )
            disp('Goal State Reached')
            inter_node = [J_goal];
            flag = 1;
        end
        
        scatter3(X2,Y2,Z2,150,'.','g','LineWidth',3)
        plot3( [X2,X1] ,...
            [Y2,Y1],[Z2,Z1],'k','LineWidth',0.5);
        n_tree = [n_tree; [inter_node(1:6),parent]];
    
        pause(0.00000001)
%         cla
%         [v, f, n, c, stltitle] = stlread('C3_BASE_S.STL');
%         plot_stl(f, v, col_matrix);
%         obs_v = plot_obs();
    end
end
toc

%% Define Path for RRT
path = [];
if (flag==1)
    x = input('Path Found. Press 1 to Plot\n');
    pause(3);
    disp('Plotting');
    
    path = [path; J_goal];
    parent = n_tree(size(n_tree,1),7);
    while(parent~=1)
        path = [path; n_tree(parent,1:6)];
        parent = n_tree(parent,7);
    end
    path = [path; J_strt];
%     % Plot the motion and path
%     for (i=size(path,1)-1:-1:3)
%         plot( [path(i,4),path(i-1,4)], [path(i,5),path(i-1,5)], 'k', 'LineWidth',0.5 )
%         pause(0.1)
%     end

    for (i=size(path,1)-1:-1:1)
        obs = plot_obs();
%         plot_robot(J_strt,[1,0,0],F)
%         plot_robot(J_goal,[0,0,1],F)
        
        plot_robot(path(i,:),[0.941176, 0.972549, 1],F)
        pause(0.000000001)
        delete(obs)
        if(i~=1) cla; end
    end
else 
    disp('Path Not Found')
end

% close all





%% Function Definitions

function [step,direction,i] = valid_step( node, n_tree,F,obs_v )
    dist=[]; %Distance Storage
    for iter=1:1:size(n_tree,1)
        dt1 = node(1,1) - n_tree(iter,1);
        dt2 = node(1,2) - n_tree(iter,2);
        dt3 = node(1,3) - n_tree(iter,3);
        dt4 = node(1,4) - n_tree(iter,4);
        dt5 = node(1,5) - n_tree(iter,5);
        dt6 = node(1,6) - n_tree(iter,6);
    
        d = (dt1^2 + dt2^2 + dt3^2 + dt4^2 + dt5^2 + dt6^2);
        dist = [dist; d];
    end
    [val,i] = min(dist); %i is the iteration or row number
    
    %Linear Interpolation for resolution:
    dir_vec = node(1,:) - n_tree(i,1:6);
    
    step = 0.3;
    direction = round(dir_vec*step);
    sing_step = 15;
    if (direction(1,1)^2 + direction(1,2)^2 + direction(1,3)^2 +...
        direction(1,4)^2 + direction(1,5)^2 + direction(1,6)^2> 6*sing_step^2)
       if (dir_vec(1,1)~=0) dir_vec(1,1) = dir_vec(1,1) / abs(dir_vec(1,1)); end
       if (dir_vec(1,2)~=0) dir_vec(1,2) = dir_vec(1,2) / abs(dir_vec(1,2)); end
       if (dir_vec(1,3)~=0) dir_vec(1,3) = dir_vec(1,3) / abs(dir_vec(1,3)); end
       if (dir_vec(1,4)~=0) dir_vec(1,4) = dir_vec(1,4) / abs(dir_vec(1,4)); end
       if (dir_vec(1,5)~=0) dir_vec(1,5) = dir_vec(1,5) / abs(dir_vec(1,5)); end
       if (dir_vec(1,6)~=0) dir_vec(1,6) = dir_vec(1,6) / abs(dir_vec(1,6)); end
       
       direction = dir_vec * sing_step;
    end
    inter_node(1,:) = n_tree(i,1:6) + direction(1,:);
 
    % Collision Checker
    collision = coll_check(inter_node,obs_v,F);
    if collision~=0
       step = 0;
       return;
    end
end


function collision = coll_check(J_rot,obs_v,F)
    T = [1,0,0,0;
     0,1,0,0;
     0,0,1,0;
     0,0,0,1];
    
    for(i=1:1:6)
        file = ['C3_ARM' num2str(i) '.STL'];
        [v, f, n, c, stltitle] = stlread(file);
        Rz(1:3,1:3) = rotz(J_rot(i));
        Rz(4,4) = 1;
        T = T*transf(F(i+1,:))*Rz;
        vertices = zeros( size(v,1),4 );
        vertices(:,1:3) = v;
        vertices(:,4) = 1;
        temp = (T * vertices')'; 
        v(:,1:3) = temp(:,1:3);

%Check for Collision
        obj = patch('Faces',f,'Vertices',v);
        set(obj,'visible','off');
        collision = GJK(obs_v,obj,10);
        delete(obj);
        if (collision==1) return; end
    end
end

% Tranf function finds the transformation matrix for F1 frame
function T = transf(F1)
    % x,y,z are the three translations in XYZ system
    T(1,4) = F1(1,1);
    T(2,4) = F1(1,2);
    T(3,4) = F1(1,3);
    R = eul2rotm([ deg2rad(F1(1,6)) deg2rad(F1(1,5)) deg2rad(F1(1,4)) ],'ZYX');
    T(1:3,1:3) = R;
    T(4,4) = 1;
end

function [x,y,z] = robot_xy(J_rot,F)
    T = [1,0,0,0;
     0,1,0,0;
     0,0,1,0;
     0,0,0,1];
    
    for i=1:1:6
        Rz(1:3,1:3) = rotz(J_rot(i));
        Rz(4,4) = 1;
        T = T*transf(F(i+1,:))*Rz;
    end
    x = T(1,4);
    y = T(2,4);
    z = T(3,4);
end

function obs_v = plot_obs()
    col_matrix = [0.941176, 0.972549, 1];

    T_obs = [ 1,0,0,-350;
          0,1,0,0;
          0,0,1,0;
          0,0,0,1];

    [v, f, n, c, stltitle] = stlread('Obstacle.STL');
    vertices = zeros( size(v,1),4 );
    vertices(:,1:3) = v;
    vertices(:,4) = 1;
    temp = (T_obs * vertices')'; 
    v(:,1:3) = temp(:,1:3);
    obs_v = patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);     
end

function plot_stl(f, v, col)
    patch('Faces',f,'Vertices',v,'FaceVertexCData',col,'FaceColor',col);
end

function plot_robot(J_rot,col,F)
    T = [1,0,0,0;
         0,1,0,0;
         0,0,1,0;
         0,0,0,1];
    
    [v, f, n, c, stltitle] = stlread('C3_BASE_S.STL');
    plot_stl(f, v, col)
    for(i=1:1:6)
        file = ['C3_ARM' num2str(i) '.STL'];
        [v, f, n, c, stltitle] = stlread(file);
        Rz(1:3,1:3) = rotz(J_rot(i));
        Rz(4,4) = 1;
        T = T*transf(F(i+1,:))*Rz;
        vertices = zeros( size(v,1),4 );
        vertices(:,1:3) = v;
        vertices(:,4) = 1;
        temp = (T * vertices')'; 
        v(:,1:3) = temp(:,1:3);
        plot_stl(f, v, col)
    end
end


% %Actual Angles
% t1 = randsample( -170:s:170,1 );
% t2 = randsample( -160:s:65,1 );
% t3 = randsample( -51:s:225,1 );
% t4 = randsample( -200:s:200,1 );
% t5 = randsample( -135:s:135,1 );
% t6 = randsample( -360:s:360,1 );