%% Compute Normals

% This function computes the normals for a group os points provided the
% informtion form STL about the faces and vertices and normals of the part.

function [normals] = compute_normals(v,f,n,points)
    normals = [];
    for i=1:size(points)
        for j=1:size(f)
            p1 = v(f(j,1),:);
            p2 = v(f(j,2),:);
            p3 = v(f(j,3),:);
            p = [p1;p2;p3];
%             triangle = delaunayn([p(:,1),p(:,2),p(:,3)]);
%             tri_search = tsearchn([points(:,1),points(:,2),points(:,3)],triangle,'xyz');
%             IsInside = ~isnan(tri_search) % Returns 1 is Point lies within the triangle
            in = inhull([points(:,1),points(:,2),points(:,3)],p,[],1.e-13*mean(abs(p(:))))
            if in(1)
                normals = [normals; n(j,:)];
            end
        end
    end
end