%% This Function computes the Curvature for the paths being read from the file.

function curvature = compute_curvature(points)
    pts_size = size(points);
    curvature = [];
    for i=1:pts_size(1,1)-2
%         tang_vec_curr = points(i+1,:) - points(i,:);
%         tang_vec_next = points(i+2,:) - points(i+1,:);
%         angle = acos( dot(tang_vec_curr,tang_vec_next) / norm(tang_vec_curr)/norm(tang_vec_next) );
%         dist = norm(points(i+1,:) - points(i,:));

        [center,rad,v1,v2] = circlefit3d(points(i,:),points(i+1,:),points(i+2,:));
%         curvature = [curvature; (angle / dist)];
        if (rad~=0)
            curvature = [curvature; (1/rad)];
        else
            curvature = [curvature; 0];
        end
    end
end