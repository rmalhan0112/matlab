%% Tool Tip Number Computing

% This funtion computes which tool tip is to be used at the given point. It
% takes the Point array and Normal Vector as argument and spits out the
% tool number array which defines the tool number to be used. Tool-1 is the
% one at bottom of roller. Tool-2 is the back of the roller and Tool-3 is
% the front of the roller.

function tool_num = compute_Tool_num(points, normals)
    tool_num = [];
    gz = [0,0,1];

    size_pts = size(points,1);
    for j=1:size_pts
%         if j==size_pts
%         else
%            direction = points(j+1,:) - points(j,:);
%            dir_vec = direction / norm(direction);
%         end
%         bx = cross(gz , dir_vec);
%         bx = bx / norm(bx);
%         by = cross(bx,gz);
%         by = by / norm(by);
%         
%         dot_prdct = dot(by , normals(j,:))/norm(by)/norm(normals(j,:));
%         angle = rad2deg(acos(dot_prdct));
%         if (angle>=0.6981 & angle<=2.4435 || angle>=-2.4435 & angle<=0.6981)
%             tool_num = [tool_num; 1];
%         end
%         if angle>-40 & angle<40
%             tool_num = [tool_num; 2];
%         end
%         if (angle>2.4435 & angle<=3.1416 || angle>=-3.1416 & angle<2.4435)
%             tool_num = [tool_num; 3];
%         end

%% For concave Mold
%         if (points(j,3)>-60 & points(j,2)<100)
%             tool_num = [tool_num; 2];
%         end
%         if (points(j,3)>-60 & points(j,2)>100)
%             tool_num = [tool_num; 3];
%         end
%         if (points(j,3)<=-60)
%             tool_num = [tool_num; 1];
%         end
          tool_num = [tool_num; 1];
    end
    if (size(tool_num,1)~=size(points,1))
        disp('Error')
    end
end