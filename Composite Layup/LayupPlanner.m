%% Automated Composite Sheet Layup Planner

% This code allows an interface for the user to select points on a CAD part
% which represent the start and end points for every move. The planner
% converts these points into structured trajectories and writes
% instructions to the robot to follow.

clear
clc
close all
format short


%% Define part and Plot it

% Define the name of the part stored in the working directory
% part = 'GE90.stl';
part = 'GE90.stl';
% part = 'Mold-Final.stl';
global roller_width;
roller_width = 24;
% Note changing direction of Z axis in stl origin. make sure to change the
% dot product in filtering faces in curve_fit

%% Load Part and all the info
%STLREAD is a function obtaiend from matlab exchange. Refer to the file for
%more details.
[v, f, n, c, stltitle] = stlread(part);
delete(gca);
close all;    
    
% Choose what color the CAD part needs to be displayed.
col_matrix = [0.941176, 0.972549, 1];

% Plotting the CAD part in Figure-1
figure(1)
patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
% set(gca,'zdir','reverse')
xlabel('X-Axis')
ylabel('Y-Axis')
zlabel('Z-Axis')
hold on




%% User-Interface for Selection of Points
  
% Define object for Data Cursor
dcm_obj = datacursormode(figure(1));
set(dcm_obj,'SnapToDataVertex','off')
strt_pts = [];
end_pts = [];
key=0;
flag = 0;
points=[];
grp=[];
normals = [];
idx = 1;
faces=[];
tool_num = [];
xyz_cba = [];
xyz=[];
cba=[];

% Keep Selecting Start and End points alternatively.
while 1
    key=0;
    fprintf('Select Start Point')
    while key==0
        try key = waitforbuttonpress; catch flag=1; break; end 
    end
    if flag==1 
        fprintf('\nSelection Compelte\n'); 
        break; 
    end
    c_info = getCursorInfo(dcm_obj);
    strt_pt = c_info.Position;
    strt_pts = [strt_pts;strt_pt];
    
    key=0;
    fprintf('Select End Point')
    while key==0
        try key = waitforbuttonpress; catch flag=1; break; end
    end
    if flag==1 
        fprintf('\nSelection Complete\n'); 
        break; 
    end
    c_info = getCursorInfo(dcm_obj);
    end_pt = c_info.Position;
    end_pts = [end_pts;end_pt];
    
    [temp_pts,temp_normals,temp_faces] = curve_fit(strt_pt,end_pt,v,f,n);  %Call the curve_fit function
%     temp_pts = concave_Sort(temp_pts);
%     temp_tool_num = compute_Tool_num(temp_pts,temp_normals); % Compute the Tool_Num FOR CONCAVE MOLD
    faces = [faces;temp_faces]; % Appendng the faces to the matrix
    points = [points;temp_pts]; % Appending the Points in curve
    [tranf_pts,norm_vec,T] =  robot_to_part(temp_pts,temp_normals); %Transform the Points and Normals to Robot
    
    xyz = [xyz; tranf_pts]; 
    normals = [normals; norm_vec]; %Appending the normals in curve
    
    %Append the Group Indices
    size_points = size(tranf_pts);
    idx_start = idx;
    idx = idx + size_points(1,1)-1;
    idx_end = idx;
    range = [idx_start,idx_end];
    grp = [grp;range]; %Append the Group Indices
    % Appending Group Indices complete
    
    [bx,by,bz,strt_idx,end_idx] = compute_TCP(xyz,range,normals); %compute_TCP function
    cba = compute_euler(bx,by,bz); % Compute the Euler Angles for the Points
    temp_tool_num = compute_Tool_num(tranf_pts,norm_vec); % Compute the Tool_Num
    tool_num = [tool_num; temp_tool_num];
    xyz_cba = [xyz_cba; horzcat(tranf_pts,cba,temp_tool_num)];
    
    plot_tcp(bx,by,bz,strt_idx,end_idx,points);
    plot_tool_num(temp_tool_num,temp_pts);
    
    %Plot the Points
    scatter3(points(:,1),points(:,2),points(:,3),200,'*','b'); %Plot the Points
%     roller_line(bx,temp_pts,roller_width);
    idx = idx + 1;
end
[v,n] =  robot_to_part(v,n);
figure(3)
patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
% plot_normals(f,v,n)
plot_tcp(xyz_cba)
csvwrite('xyz_abc.txt',xyz_cba);
csvwrite('Group_IDX.txt',grp);


function pts = concave_Sort(temp_pts)
    frst_hlf = [];
    scnd_hlf = [];
    for i=1:size(temp_pts,1)
        if (temp_pts(i,2) < 201.09/2)
            frst_hlf = [frst_hlf; temp_pts(i,:)];
        else
            scnd_hlf = [scnd_hlf; temp_pts(i,:)];
        end
    end
    frst_hlf = sortrows(frst_hlf,3,'descend');
    scnd_hlf = sortrows(scnd_hlf,3,'ascend');
    pts = vertcat(frst_hlf,scnd_hlf);
end

%% Function which computes the Line of contact of roller
function roller_line(bx,temp_pts,roller_width)
   
    for i=1:size(temp_pts)
        a = temp_pts(i,:)+(roller_width*0.5)*bx(i,:);
        b = temp_pts(i,:)-(roller_width*0.5)*bx(i,:);
        pt=[a;b];
        L = line(pt(:,1),pt(:,2),pt(:,3));
%         L.Color = [0.960784, 0.870588, 0.701961];
        L.color = ['yellow'];
        L.LineWidth = 1;
        hold on
    end
end

%% Plot the Tool_TCP

function plot_tcp(bx,by,bz,strt_idx,end_idx,points)
    bx = bx .* -1;
    by = by .* -1;
    bz = bz .* -1;
    
    hold on
    % Plot the X Vector
    quiver3(points(strt_idx:end_idx,1),points(strt_idx:end_idx,2),points(strt_idx:end_idx,3),...
       bx(:,1),bx(:,2),bx(:,3),'r','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
    hold on
    % Plot the Y Vector
    quiver3(points(strt_idx:end_idx,1),points(strt_idx:end_idx,2),points(strt_idx:end_idx,3),...
       by(:,1),by(:,2),by(:,3),'g','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
    hold on
    % Plot the Z vector
    quiver3(points(strt_idx:end_idx,1),points(strt_idx:end_idx,2),points(strt_idx:end_idx,3),...
       bz(:,1),bz(:,2),bz(:,3),'b','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
end

% function plot_tcp(xyz_cba)
%     length = 5;
%     figure(3)
%     hold on;
%     for i=1:size(xyz_cba,1)
%         Gx = [1;0;0];
%         Gy = [0;1;0];
%         Gz = [0;0;1];
%         R = eul2rotm(xyz_cba(i,4:6),'ZYX');
%         bx = R*Gx;
%         by = R*Gy;
%         bz = R*Gz;
%         bx = bx'/norm(bx);
%         by = by'/norm(by);
%         bz = bz'/norm(bz);
%         point = xyz_cba(i,1:3);
%         plot3([point(1,1),point(1,1)+bx(1,1)*length],...
%             [point(1,2),point(1,2)+bx(1,2)*length],...
%             [point(1,3),point(1,3)+bx(1,3)*length],'r','linewidth',3);
%         plot3([point(1,1),point(1,1)+by(1,1)*length],...
%             [point(1,2),point(1,2)+by(1,2)*length],...
%             [point(1,3),point(1,3)+by(1,3)*length],'g','linewidth',3);
%         plot3([point(1,1),point(1,1)+bz(1,1)*length],...
%             [point(1,2),point(1,2)+bz(1,2)*length],...
%             [point(1,3),point(1,3)+bz(1,3)*length],'b','linewidth',3);
%     end
% end


%% Plot Tool_Number

function plot_tool_num(tool_num , temp_pts)
    for i=1:size(tool_num)
        if tool_num(i,1) ==1
            scatter3(temp_pts(i,1),temp_pts(i,2),temp_pts(i,3),100,'.','r');
        end
        if tool_num(i,1) == 2
            scatter3(temp_pts(i,1),temp_pts(i,2),temp_pts(i,3),100,'.','g');
        end
        if tool_num(i,1) ==3
            scatter3(temp_pts(i,1),temp_pts(i,2),temp_pts(i,3),100,'.','b');
        end
    end
end

%% Plot Normals

function plot_normals(faces,points,normals)
    length = 10;
    hold on;
    for i=1:size(faces,1)
        v1 = [points(faces(i,1),1), points(faces(i,1),2), points(faces(i,1),3)];
        v2 = [points(faces(i,2),1), points(faces(i,2),2), points(faces(i,2),3)];
        v3 = [points(faces(i,3),1), points(faces(i,3),2), points(faces(i,3),3)];
        centroid = (v1 + v2 + v3)/3;
        plot3([centroid(1,1),centroid(1,1)+normals(i,1)*length],...
            [centroid(1,2),centroid(1,2)+normals(i,2)*length],...
            [centroid(1,3),centroid(1,3)+normals(i,3)*length],'k','linewidth',3);
    end
end