%% This Script reads a Point File and Group IDX file containing the Curve 
% Information and Outputs the Curvatures at those Points

clc
clear
format short

values = csvread('xyz_abc.txt');
points = values(:,1:3);
curvature = real(compute_curvature(points));
count=1;
figure(1)
hold on
for i=1:size(curvature)
    scatter(count, curvature(i,1)*10,50,'b','.')
    count =count + 1;
end