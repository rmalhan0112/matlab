%% Curvature Plot for any curve with a and b as the lengths.

clear
clc

a = 2;
b = 5;
syms t;

%x = a*1.41*cos(t)/(sin(t)^2 + 1);
%y = a*1.41*sin(t)*cos(t)/(sin(t)^2 + 1);

x = a*cos(t);
y = b*sin(t);

xdash = diff(x,t);
xddash = diff(diff(x,t),t);

ydash = diff(y,t);
yddash = diff(diff(y,t),t);
figure('units','normalized','outerposition',[0 0 1 1])

for t=0:0.05:6.28
    k = (subs(xdash,t) * subs(yddash,t) - subs(ydash,t) * subs(xddash,t))...
        / (subs(xdash,t)^2 + subs(ydash,t)^2)^1.5;
    
    tempx = subs(x,t);
    tempy = subs(y,t);
    subplot(1,2,1)
    scatter(tempx,tempy,'fill','r')
    hold on
    
    subplot(1,2,2)
    scatter(t,k,'fill','b')
    pause(0.001)
    hold on
end

subplot(1,2,2)
xlabel('Parameter t');
ylabel('Curvature k');

subplot(1,2,1)
xlabel('X coordinate');
ylabel('Y Coordinate');