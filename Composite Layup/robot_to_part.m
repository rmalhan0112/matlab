%% Compute Transformation of Points and Euler Angles from part to Robot

function [points,normals,T] = robot_to_part(points,normals)
    xyz_abc = [];

% GE90
%     part_points = [ 0 , 0 , 0;
%                     17.44 , 295.35 , -26.11;
%                     136.93 , 0 , -72.7;
%                     133.85, 284.32, 7.95
%                     ];
% 
%     robot_points = [ 509.25, 174.84, 92.07;
%                     805.22, 177.18, 72.33;
%                     518.02, 38.22, 19.75;
%                     800.58, 60.45, 104.83
%                     ];

% Concave
    part_points = [ 0 , 0 , 0;
                    225, 11.25, 0;
                    0, 100.54, -56.25;
                    0, 201.09, 0
                    ];

    robot_points = [ 509.26, 177.53, 97.44;
                    520.72, -46.10, 97.28;
                    607.13, 180.04, 44.15;
                    708.32, 180.74, 102.70
                    ];
     [R,t] = point_pairs_transformation(part_points,robot_points);
     T = zeros(4,4);
     T(1:3,1:3) = R;
     T(1:3,4) = t;
     T(4,4) = 1;
     Rdash = T;
     Rdash(1:3,4) = [0;0;0];
     for i=1:size(points)
         homogeneous_pt = zeros(1,4);
         homogeneous_pt(1,1:3) = points(i,:);
         homogeneous_pt(1,4) = 1;
         transformed_pt = T*homogeneous_pt(1,:)';
         pt = transformed_pt';
         points(i,:) = pt(1,1:3);
     end
     
     for i=1:size(normals)
         homogeneous_norm = zeros(1,4);
         homogeneous_norm(1,1:3) = normals(i,:);
         homogeneous_norm(1,4) = 1;
         transformed_norm = Rdash*homogeneous_norm(1,:)';
         norm = transformed_norm';
         normals(i,:) = norm(1,1:3);
     end
     
    % This function computes the Rotation and Translation matrix. 
    function [R,T] = point_pairs_transformation(A,B)
        centroid_A = mean(A);
        centroid_B = mean(B);
        A = A - centroid_A;
        B = B - centroid_B;
        H = A'*B;
        [U,S,V] = svd(H);
        R = V*U';
        
        if det(R)<0
            V(3,:) = V(3,:) .* -1;
            R = V*U';
        end
        T = -R*centroid_A' + centroid_B';
    end
end