%% Curvature Plot for Ellipse with a and b as the lengths.

clear
clc

a = 5;
b = 10;

x = a*cos(t);
y = b*sin(t);

for (t=0:0.01:1)
    xdash = diff(x,t);
    xddash = diff(diff(x,t),t);

    ydash = diff(y,t);
    yddash = diff(diff(y,t),t);

    k = abs(xdash*yddash - ydash*xddash) / (xdash^2 + ydash^2)^1.5;
    scatter(t,k)
    hold on
end

xlabel('Parameter t');
ylabel('Curvature k');