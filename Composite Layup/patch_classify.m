%% Trajectory classification accoridng to patches.

clear
clc
close all

grp_idx = csvread('Group_IDX.txt');
points = csvread('xyz_abc.txt');

% Determining the maximum number of Points in a trajectory
max_pts = size(grp_idx,1);
patch = [];
patch = [patch; 1];
delta = 50;
count = 1;

for i=2:size(grp_idx,1) 
    tr1_strt_pt = points(grp_idx(i-1,1),1:3);
    tr1_end_pt = points(grp_idx(i-1,2),1:3);
    tr2_strt_pt = points(grp_idx(i,1),1:3);
    tr2_end_pt = points(grp_idx(i,2),1:3);
    if ( norm(tr2_strt_pt-tr1_strt_pt)<delta || norm(tr2_end_pt-tr1_end_pt)<delta )
        patch = [patch; i];
    else
        disp(patch')
        patch = [];
        patch = [patch; i];
    end
end
disp(patch')