%% This is a composite layup interface to define paths for robot from start and end points.

clear
clc

resolution = 2; %Resolution of the Grid
part = 'Mold-Final.stl';
col_matrix = [0.862745, 0.862745, 0.862745];

[v, f, n, c, stltitle] = stlread(part);

close all
length = floor(max(v(:,1)) - min(v(:,1)));
width = floor(max(v(:,2)) - min(v(:,2)));
height = floor(max(v(:,3)) - min(v(:,3)));
faces_size = size(f);

%% Grid Creation:
while mod(length,resolution)~=0
    length = length + 1;
end
while mod(width,resolution)~=0
    width = width + 1;
end

grid = [];
for i=0:(length/resolution)
    for j=0:(width/resolution)
        grid = [grid;[i*resolution],[j*resolution]]; 
    end
end
grid_size = size(grid);

%% Filtering the faces which lie on surface only
idx_face = [];
for i=1:faces_size(1,1)
    c = dot([0.0,0.0,-1],n(i,:)); %Dot product between negative Z and normal
    if c>0 & c<=1
        idx_face = [idx_face;[i]];
    end
end
faces_size = size(idx_face);




%% Plotting the Vertices of the corresponding faces
% This is a debugging Tool. Uncomment only to check Face filtering

% for i=1:faces_size(1,1)
%     scatter3( v(f(idx_face(i),1),1), v(f(idx_face(i),1),2), v(f(idx_face(i),1),3) )
%     scatter3( v(f(idx_face(i),2),1), v(f(idx_face(i),2),2), v(f(idx_face(i),2),3) )
%     scatter3( v(f(idx_face(i),3),1), v(f(idx_face(i),3),2), v(f(idx_face(i),3),3) )
%     hold on
% end



%% Mapping Points to the 3D Surface

% This section takes a point on the grid, shifts the triangle to XY plane
% and then checks if point lies in the bounds. If it does, it dictates
% equation of plane of triangle in 3D and solves for it finding the new
% coordinates and stores it in matrix.

xyz = [];
for j=1:faces_size(1,1)
    p1 = v(f(idx_face(j),1),:);
    p2 = v(f(idx_face(j),2),:);
    p3 = v(f(idx_face(j),3),:);
    triangle = [p1;p2;p3;p1];
    % Check if the all grid points lie inside any triangle for face j.
    in = inpolygon(grid(:,1),grid(:,2),triangle(:,1),triangle(:,2));
    % Find the row numbers of elements that actually lie inside.
    k = find(in);
    
    if isempty(k)
    else
        temp = [grid(k,1),grid(k,2)];
        % Transfer the point to the plane
        a = ((p2(2)-p1(2))*(p3(3)-p1(3)))-((p3(2)-p1(2))*(p2(3)-p1(3)));
        b = ((p2(3)-p1(3))*(p3(1)-p1(1)))-((p3(3)-p1(3))*(p2(1)-p1(1)));
        c = ((p2(1)-p1(1))*(p3(2)-p1(2)))-((p3(1)-p1(1))*(p2(2)-p1(2)));
        d = -(a*p1(1))-(b*p1(2))-(c*p1(3));
        size_k = size(k);

        for count = 1:size_k(1,1)
            zval = ((-d-(a*temp(count,1))-(b*temp(count,2)))/c)-0.08;
            xyz = [xyz;[temp(count,1)],[temp(count,2)],[zval]];
        end
    end
end

figure(1);
x = xyz(:,1);
y = xyz(:,2);
z = xyz(:,3);

%Scatter Plot of all Points
scatter3(x,y,z,100,'.','r')

patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
set(gca,'zdir','reverse')
hold on
dcm_obj = datacursormode(figure(1));
set(dcm_obj,'SnapToDataVertex','off')

[normals,curvature] = findPointNormals([x,y,z],[],[0,0,10],true);

%Converting Curvature to Printable text
format short
for i=1:size(curvature)
    if curvature(i,1)<0.0001
        curvature(i,1) = 0;
    end
end

%Plot Points and Normals with curvature
size_xyz = size(xyz);

%Fix Normals
for i=1:size(normals)
    if dot([0,0,-1],normals(i,:))<0
        normals(i,:) = -normals(i,:);
    end
end
% plot normals and colour the surface by the curvature
hold on;
quiver3(x,y,z,normals(:,1),normals(:,2),normals(:,3),'k');
% axis equal;
% [K,H,P1,P2] = surfature()

% h = figure(1);
% set(h,'WindowKeyPressFcn',@KeyPressFcn);
% 
%  function KeyPressFcn(~,evnt,dcm_obj)
%     if (evnt.Key=='return')
%         disp('Fuck You')
%         if isnan(getCursorInfo(dcm_obj))
%             disp('No Cursor Input')
%         else
%         disp(c_info)
%         end
%     end
% end
