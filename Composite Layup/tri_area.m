%% Area Calculation enclosed by group of Triangles

% Input is the face and vertex array

function area = tri_area(f,v)
    area=0;
    for i=1:size(f)
        P1 = v(f(i,1),:);
        P2 = v(f(i,2),:);
        P3 = v(f(i,3),:);    
        V = [P1;P2;P3];
        p1 = [V(1,2) V(1,3) 1; V(2,2) V(2,3) 1; V(3,2) V(3,3) 1];      
        p2 = [V(1,3) V(1,1) 1; V(2,3) V(2,1) 1; V(3,3) V(3,1) 1];
        p3 = [V(1,1) V(1,2) 1; V(2,1) V(2,2) 1; V(3,1) V(3,2) 1];
        area = area+0.5 * sqrt((det(p1))^2 + (det(p2))^2 + (det(p3))^2) ;
    end
end
 
%    1.0065e+05