%% Color map for the grid computed using Dijkstra for start node [90,90]

clear all;
close all;
clc

grid = csvread("Grid_2_FMM.csv");
for i=1:size(grid,1)
    for j=1:size(grid,2)
        if grid(i,j)==500
            grid(i,j)=-0.5;
        end
    end
end

costs = [];
for i=1:size(grid,1)
    costs = [costs; max(grid(i,:))];
end
max_cost = max(costs);

for i=1:size(grid,1)
    for j=1:size(grid,2)
        if grid(i,j)==-0.5
            grid(i,j)=-0.5;
        else
            grid(i,j) = grid(i,j) / max_cost;
        end
    end
end
imagesc(grid)
colormap hot;
hold on

% for i=1:2:size(grid,1)
%     for j=1:2:size(grid,2)
%         if grid(i,j)~=-0.5
%             curr_node = [i,j];
%             gradient = -grad_Eval(curr_node,grid);
%             quiver(curr_node(1,1),curr_node(1,2),curr_node(1,1)+gradient(1,1),curr_node(1,2)+gradient(1,2),...
%                 'k','AutoScale','on','AutoScaleFactor',0.02,'LineWidth',1,'MaxHeadSize',1);
%         end
%     end
% end
% 
% function grad_Val = grad_Eval(curr_node,grid)
%     grad_Val(1,1) = ( grid(curr_node(1,1)+1) - grid(curr_node(1,1)) );
%     grad_Val(1,2) = ( grid(curr_node(1,2)+1) - grid(curr_node(1,2)) );
% end