%% Nodes Properties

classdef node_prop < handle
    properties
    id;
    cost;
    map_key;
    is_open;
    is_closed;
    parent_id;
    end
end