%% Color map for the grid computed using Dijkstra for start node [90,90]

grid = csvread("Grid_2_FMM.csv");
for i=1:size(grid,1)
    for j=1:size(grid,2)
        if grid(i,j)==500
            grid(i,j)=-1;
        end
    end
image(grid)
colormap winter;