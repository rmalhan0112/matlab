%% Updating the learning data in 5D

clear all;
clc;
close all;

addpath('\\DESKTOP-U1S7SN9\KUKA_log\Learning_Data_5D_updated\');
doe = csvread('Learning_DOE_5D.csv');
addpath('\\DESKTOP-U1S7SN9\KUKA_log\Learning Data 5D\');

for i=1:size(doe,1)
    if(mod(i,10)==0) fprintf('File Number:  ',i); end
    curr_data = csvread([num2str(i) '.csv']);
    csvwrite(['\\DESKTOP-U1S7SN9\KUKA_log\Learning_Data_5D_updated\' num2str(i) '.csv'],curr_data(1:(end-10),:));
    clear curr_data;
end