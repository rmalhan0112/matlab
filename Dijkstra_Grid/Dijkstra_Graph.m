%% Dijkstra on a Graph

clear all;
clc;
close all;

global m;
global n;
m = 50;
n = 50;

strt_node = [1,1];
goal_node = [45,30];

% INITIALIZE CODE
% figure(1)
% hold on
% xlim = [0,m];
% ylim = [0,n];
queue = PriorityQueue();
grid = create_grid(m,n);
NODES_MAP = containers.Map;
terminate = false;
curr_node = create_node(strt_node,[],0,true,false);
NODES_MAP(curr_node.map_key) = curr_node;
count = 1;

%% DIIJKSTRA ALGORITHM
disp("Running Diijkstra.....");

while (terminate==false)
    if(mod(count,10)==0) fprintf("Iteration Number:  %d\n",count); end
%     scatter(curr_node.id(1,1),curr_node.id(1,2),50,'r','.')
    neighbors = get_neighbors(curr_node.id);
    for i=1:size(neighbors,1)
        cost = ( get_Cost(curr_node.id, [neighbors(i,1),neighbors(i,2)]) + curr_node.cost );
        grid(neighbors(i,1),neighbors(i,2)) = 3;
        %Check if the Node already exits
        node_exists = isKey(NODES_MAP,create_key([neighbors(i,1),neighbors(i,2)]));

        if ( node_exists==false )
            neighbr_node = create_node([neighbors(i,1),neighbors(i,2)],curr_node.id,cost,true,false);
            NODES_MAP(neighbr_node.map_key) = neighbr_node;
            queue.push(str2num(neighbr_node.map_key),neighbr_node.cost,0);

        elseif ( (node_exists==true) && (NODES_MAP(create_key([neighbors(i,1),neighbors(i,2)])).is_open == true) )
            temp_node = NODES_MAP(create_key([neighbors(i,1),neighbors(i,2)]));
            if( cost < temp_node.cost )
                temp_node.cost = cost;
                temp_node.parent_id = curr_node.parent_id;
                temp_node.is_closed = false;
                temp_node.is_open = true;
                queue.push( str2num(temp_node.map_key),temp_node.cost,0 );
                NODES_MAP(create_key([neighbors(i,1),neighbors(i,2)])) = temp_node;
            end
        end
    end
    temp_node = NODES_MAP(curr_node.map_key);
    temp_node.is_open = false;
    temp_node.is_closed = true;
    NODES_MAP(curr_node.map_key) = temp_node;
    if (count==1) queue.pop(); end
    %Check if Node popped from Queue is Visited or Not.
    %Also includes termination Criteria
    is_visited = true;
    while is_visited==true
        if (queue.size()~=0)
            curr_node = NODES_MAP(num2str(queue.top()));
            queue.pop();
        else
            terminate = true;
            break;
        end
        if (curr_node.is_closed==false)
            is_visited = false;
        end
    end
    count = count + 1;
    pause(1e-32)
end


%% Generate Path:
disp("Generating Path......")
% Generate Path
path = [];
path_complete = false;
parent = NODES_MAP(create_key(goal_node)).parent_id;
while path_complete==false
    temp_node = NODES_MAP(create_key(parent));
    path = [path; temp_node.id];
    parent = temp_node.parent_id;
    if (isempty(parent))
        path_complete = true;
    end
end

tot_cost = 0;
for i=1:size(path,1)
    % print([ path[i][0],path[i][1] ])
    grid(path(i,1),path(i,2)) = 2;
    tot_cost = tot_cost + NODES_MAP(create_key([path(i,1),path(i,2)])).cost;
end

% PLOT MAP
grid(strt_node(1,1),strt_node(1,2)) = 1;
grid(goal_node(1,1),goal_node(1,2)) = 1;

hold on;
for i=1:m
    for j=1:n
%         key = create_key([i,j]);
%         grid(i,j) = NODES_MAP(key).cost;
        if (grid(i,j)==1)
            scatter(i,j,500,'k','.');
        end
        if(grid(i,j)==2)
            scatter(i,j,500,'y','.');
        end
        if(grid(i,j)==3)
            scatter(i,j,500,'b','.');
        end
    end
end






%% Functions Required
function neighbors = get_neighbors(node_id)
    global m;
    global n;
    grid = create_grid(m,n);
    grid_rows = size(grid,1);
    grid_cols = size(grid,2);

    idx_row = node_id(1,1);
    idx_col = node_id(1,2);

    neighbors = [];
    for i=-1:1:1
        for j=-1:1:1
            neigh_row = idx_row + i;
            neigh_col = idx_col + j;
            if ( (neigh_row>grid_rows) || (neigh_col>grid_cols) || (neigh_row<1) || (neigh_col<1) )
                continue;
            end
            if (neigh_row==idx_row && neigh_col==idx_col)
                continue;
            end
            neighbors = [neighbors; [neigh_row,neigh_col]];
        end
    end
end

function grid = create_grid(m,n)
    for i=1:m
        for j=1:n
            grid(i,j) = inf;
        end
    end
end

function cost = get_Cost(node_1,node_2)
    idx_row_1 = node_1(1,1);
    idx_col_1 = node_1(1,2);

    idx_row_2 = node_2(1,1);
    idx_col_2 = node_2(1,2);

    if ( idx_row_1~=idx_row_2 && idx_col_1~=idx_col_2 )
        cost = 1.41;
    else
        cost = 1;
    end
end

function new_node = create_node(id,parent,cost,open,close)
    new_node = node_prop();
    new_node.map_key = create_key(id);
    new_node.cost = cost;
    new_node.is_open = open;
    new_node.is_closed = close;
    new_node.id = [id(1,1), id(1,2)];
    if(~isempty(parent))
        new_node.parent_id = [parent(1,1),parent(1,2)];
    end
end

function key = create_key(id)
    global n
%     key = strcat(num2str(id(1,1)),"_", num2str(id(1,2)));
    % unique key creation with Kr+c. K is number of columns. 
    k = n;
    r = id(1,1);
    c = id(1,2);
    key = strcat( num2str( k*r + c ) );
end