%% Optimal Controller

clear all;
close all;
clc;

controllers = csvread("Controllers.csv");
doe = csvread('Stiffness_du DOE_.csv');
size_doe = 37000;

for i=1:size(controllers,1)
    if(mod(i,10)==0) fprintf("Iteration Reached: %d\n",i); end
    % Place Controller in the score array
    score_array(i,1:4) = controllers(i,:);
    case_count = 1;
    for j=1:size_doe
        curr_ctrl = doe(j,1:4);
        if(isequal(controllers(i,:),curr_ctrl))
            filename = strcat(num2str(j),".csv");
            curr_data = csvread(filename);
            score_array(i,4+case_count) =  compute_cost(curr_data);
            case_count = case_count + 1;
        end
    end
end

last_col = size(score_array,2);
for i=1:size(score_array,1)
    score_array(i,last_col+1) = sum(score_array(i,5:end));
end

csvwrite("Score_Matrix_Level2.csv",score_array);

score_array = csvread("Score_Matrix_Level2.csv");
B = sortrows(score_array,size(score_array,2));

disp("Minimum Cost Controller:")
disp(B(1,1:4));
fprintf("Cost is:  %f\n",B(1,end));

disp("Maximum Cost Controller:")
disp(B(end,1:4));
fprintf("Cost is:  %f\n",B(end,end));

figure(1)
hold on;
% plot([1:size(score_array,1)],score_array(:,end),'r','LineWidth',2);
scatter([1:size(score_array,1)],score_array(:,end),50,'r','.');

figure(2)
x = [1:size(score_array,1)];
[mu,s,muci,sci] = normfit(score_array(:,end));
cost  = normpdf(x,mu,s);  
plot(x,cost)

% csvwrite("Best_50_Level2.csv",[B(1:50,1:6),score_array(1:50,end)]);
% csvwrite("Worst_50_Level2.csv",[B(end-50:end,1:6),score_array(end-50:end,end)]);



function cost = compute_cost(curr_data)
    % Define Weights:
    fx = 33;
    fy = 33;
    fz = 33;
    tx = 33;
    ty = 33;
    tz = 33;
    dx = 50;
    dy = 50;
    da = 1000;
    db = 2500;
    dc = 6500;
    
    reward = 0;
    if(max(curr_data(:,9))>15)
        reward = 50;
    end
    
% %     cost = max(curr_data(:,1))*fx +...
% %             max(curr_data(:,2))*fy +...
% %             max(curr_data(:,3))*fz +...
% %             max(curr_data(:,4))*tx +...
% %             max(curr_data(:,5))*ty +...
% %             max(curr_data(:,6))*tz +...
%            cost =  ...
%             max(curr_data(:,3))*fz +...
%             max(curr_data(:,7))*dx +...
%             max(curr_data(:,8))*dy +...
%             max(curr_data(:,10))*da +...
%             max(curr_data(:,11))*db +...
%             max(curr_data(:,12))*dc -...
%             reward;  

        cost = -reward;
end