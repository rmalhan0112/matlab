%% Generate a full factorial design provided the number of levels and factors.

clear all
clc

design = fullfact([ 5 5 5 4 3 3 3 3 3]);
size(design,1)*2/3600/24

stiffnessx = [ 1000, 2000, 3000, 4000, 5000 ];
stiffnessy = [ 1000, 2000, 3000, 4000, 5000 ];
stiffnessz = [ 1000, 2000, 3000, 4000, 5000 ];
stiffnesst = [ 100, 150, 200, 250 ];
dx = [ 0, 1, 2 ];
dy = [ 0, 1, 2 ];
da = [ 0, 0.035, 0.06 ];
db = [ 0, 0.035, 0.06 ];
dc = [ 0, 0.035, 0.06 ];

for i=1:size(design)
    design(i,1) = stiffnessx( 1,design(i,1) );
    design(i,2) = stiffnessy( 1,design(i,2) );
    design(i,3) = stiffnessz( 1,design(i,3) );
    design(i,4) = stiffnesst( 1,design(i,4) );
    design(i,5) = dx( 1,design(i,5) );
    design(i,6) = dy( 1,design(i,6) );
    design(i,7) = da( 1,design(i,7) );
    design(i,8) = db( 1,design(i,8) );
    design(i,9) = dc( 1,design(i,9) );
end

csvwrite('Stiffness_du DOE_.csv',design)