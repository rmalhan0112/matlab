%% Interpreting the readings of Force and Deflection for Best and Worst Cases
clear all;
close all;
clc;

addpath('\\DESKTOP-U1S7SN9\KUKA_log\Assembly_Optimum_Controller');

best_ctrl = csvread("Best_Controller.csv");
wrst_ctrl = csvread("Worst_Controller.csv");

hold on;


%% Forces
bound = 10;

figure(1)
set(gcf,'color','w');
set(gca,'fontsize',25)
% Fx vs Time
plot(best_ctrl(:,end),abs(rad2deg(best_ctrl(:,10))),'r','Linewidth',1.5);
ylim([-5,bound]);
xlabel('Time in milliseconds','fontsize',50)
ylabel('Delta C in degrees','fontsize',50)
yticks([-5:1:bound]);
% title('C* controller','fontsize',50)
plot(wrst_ctrl(:,end),abs(rad2deg(wrst_ctrl(:,12))),'b','Linewidth',1.5);

legend({'C* Controller','Cw Controller'},'Fontsize',25);

% figure(2)
% set(gcf,'color','w');
% plot(wrst_ctrl(:,end),wrst_ctrl(:,1),'b','Linewidth',1.5);
% ylim([-bound,bound]);
% xlabel('Time in milliseconds','fontsize',50)
% ylabel('Force X in Newton','fontsize',50)
% yticks([-bound:1:bound]);
% title('Cw controller','fontsize',50)

% figure
% % Fy vs Time
% plot(best_ctrl(:,end),best_ctrl(:,2),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Force Y')
% title('C* controller')
% 
% figure
% % Fz vs Time
% plot(best_ctrl(:,end),best_ctrl(:,3),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Force Z')
% title('C* controller')
% 
% 
% 
% 
% figure
% %Fy vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,2),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Force Y')
% title('Cw controller')
% 
% figure
% %Fz vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,3),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Force Z')
% title('Cw controller')

%% Torques
% bound = 10;
% 
% figure
% % Tx vs Time
% plot(best_ctrl(:,end),best_ctrl(:,4),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque X')
% title('C* controller')
% 
% figure
% % Ty vs Time
% plot(best_ctrl(:,end),best_ctrl(:,5),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque Y')
% title('C* controller')
% 
% figure
% % Tz vs Time
% plot(best_ctrl(:,end),best_ctrl(:,5),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque Y')
% title('C* controller')
% 
% 
% 
% 
% 
% figure
% %Tx vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,4),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque X')
% title('Cw controller')
% 
% figure
% %Ty vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,5),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque Y')
% title('Cw controller')
% 
% figure
% %Tz vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,6),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Torque Z')
% title('Cw controller')


% %% Deflection X and Y and Z
% bound = 10;
% 
% figure
% % dx vs Time
% plot(best_ctrl(:,end),best_ctrl(:,5),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta X')
% title('C* controller')
% 
% figure
% % dy vs Time
% plot(best_ctrl(:,end),best_ctrl(:,6),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta Y')
% title('C* controller')
% 
% figure
% % dz vs Time
% plot(best_ctrl(:,end),best_ctrl(:,7),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta Z')
% title('C* controller')
% 
% 
% 
% figure
% %dx vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,5),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta X')
% title('Cw controller')
% 
% figure
% %dy vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,6),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta Y')
% title('Cw controller')
% 
% figure
% %dz vs Time
% plot(wrst_ctrl(:,end),wrst_ctrl(:,7),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta Z')
% title('Cw controller')


% %% Deflections da.db.dc
% 
% bound = 10;
% 
% figure
% % da vs Time
% plot(best_ctrl(:,end),rad2deg(best_ctrl(:,8)),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta A')
% title('C* controller')
% 
% figure
% % db vs Time
% plot(best_ctrl(:,end),rad2deg(best_ctrl(:,9)),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta B')
% title('C* controller')
% 
% figure
% % dc vs Time
% plot(best_ctrl(:,end),rad2deg(best_ctrl(:,10)),'r','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta C')
% title('C* controller')
% 
% 
% 
% figure
% %da vs Time
% plot(wrst_ctrl(:,end),rad2deg(wrst_ctrl(:,8)),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta A')
% title('Cw controller')
% 
% figure
% %db vs Time
% plot(wrst_ctrl(:,end),rad2deg(wrst_ctrl(:,9)),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta B')
% title('Cw controller')
% 
% figure
% %dc vs Time
% plot(wrst_ctrl(:,end),rad2deg(wrst_ctrl(:,10)),'b','Linewidth',2);
% ylim([-bound,bound]);
% xlabel('Time')
% ylabel('Delta C')
% title('Cw controller')