%% Generate a full factorial design provided the number of levels and factors.

clear all
clc

design = fullfact([ 4 4 4 3 3 3 2 2 2 2 2]);
size(design,1)*2/3600/24

stiffnessx = [ 3750, 4000, 4250, 4500 ];
stiffnessy = [ 3750, 4000, 4250, 4500 ];
stiffnessz = [ 750, 1000, 1250, 1500 ];
stiffnessta = [ 100, 150, 200 ];
stiffnesstb = [ 100, 150, 200 ];
stiffnesstc = [ 100, 150, 200 ];
dx = [ 0.5, 2 ];
dy = [ 0.5, 2 ];
da = [ 0.035, 0.06 ];
db = [ 0.035, 0.06 ];
dc = [ 0.035, 0.06 ];

for i=1:size(design)
    design(i,1) = stiffnessx( 1,design(i,1) );
    design(i,2) = stiffnessy( 1,design(i,2) );
    design(i,3) = stiffnessz( 1,design(i,3) );
    design(i,4) = stiffnessta( 1,design(i,4) );
    design(i,5) = stiffnesstb( 1,design(i,5) );
    design(i,6) = stiffnesstc( 1,design(i,6) );
    design(i,7) = dx( 1,design(i,7) );
    design(i,8) = dy( 1,design(i,8) );
    design(i,9) = da( 1,design(i,9) );
    design(i,10) = db( 1,design(i,10) );
    design(i,11) = dc( 1,design(i,11) );
end

csvwrite('Stiffness_du DOE Level2_.csv',design)