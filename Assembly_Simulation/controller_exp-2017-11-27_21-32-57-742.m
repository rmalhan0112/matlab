%% Experiments to be conducted on these values as an additonal data set for experimentation.

close all;
clear all;
clc;


exp = [];
cart_bound = 1;
theta_bound = 0.03;

% for i=1:1:20
%     dx = randsample(-cart_bound:0.15:cart_bound,1);
%     dy = randsample(-cart_bound:0.15:cart_bound,1);
%     da = randsample(-theta_bound:0.005:theta_bound,1);
%     db = randsample(-theta_bound:0.005:theta_bound,1);
%     dc = randsample(-theta_bound:0.005:theta_bound,1);
%     exp = [exp; [dx,dy,da,db,dc]];
% end

% cart_pop = [0.9,-0.9,0.8,-0.8,0.7,-0.7];
% theta_pop = [0.02,-0.02,0.015,-0.015,0.01,-0.01];
% 
% for i=1:1:50
%     dx = randsample(cart_pop,1);
%     dy = randsample(cart_pop,1);
%     da = randsample(theta_pop,1);
%     db = randsample(theta_pop,1);
%     dc = randsample(theta_pop,1);
%     exp = [exp; [dx,dy,da,db,dc]];
% end
% 
% csvwrite("controllers_test.csv",exp);
bst_mean_fz = [];
wrst_mean_fz = [];
cntbst = 0;
cntwrst = 0;

figure(1)
set(gcf,'color','w');
set(gca,'fontsize',30)
hold on;
for i=1:50
    filename1 = ['Worst_controller_test/',num2str(i),'.csv'];
    filename2 = ['Best_controller_test/',num2str(i),'.csv'];
    wrst_controller = csvread(filename1);
    bst_controller = csvread(filename2);
    bst_mean_fz = [bst_mean_fz; max((bst_controller(:,3)))];
    wrst_mean_fz = [wrst_mean_fz; max((wrst_controller(:,3)))];
    
%     scatter(i,bst_mean_fz,50,'r','*');
%     scatter(i,wrst_mean_fz,50,'b','*');
end

plot([1:1:50],bst_mean_fz(:,1),'r','Linewidth',2);
plot([1:1:50],wrst_mean_fz(:,1),'b','Linewidth',2);
xlabel('Experiment Number','fontsize',50)
ylabel('Force in Z (Newtons)','fontsize',50)
legend({'C* Controller','Cw Controller'},'Fontsize',35);