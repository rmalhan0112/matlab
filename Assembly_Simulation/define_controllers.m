%% Creating a a list of controllers and assigning each of them an ID.
clear all;
clc;
close all;

% stiffnessx = [ 1000, 2000, 3000, 4000, 5000 ];
% stiffnessy = [ 1000, 2000, 3000, 4000, 5000 ];
% stiffnessz = [ 1000, 2000, 3000, 4000, 5000 ];
% stiffnesst = [ 100, 150, 200, 250 ];


stiffnessx = [ 3750, 4000, 4250, 4500 ];
stiffnessy = [ 3750, 4000, 4250, 4500 ];
stiffnessz = [ 750, 1000, 1250, 1500 ];
stiffnessta = [ 100, 150, 200 ];
stiffnesstb = [ 100, 150, 200 ];
stiffnesstc = [ 100, 150, 200 ];


% there will be 5x5x5x4 possible combination of controllers.
controller = [];
for a=1:size(stiffnessx,2)
    for b=1:size(stiffnessy,2)
        for c=1:size(stiffnessz,2)
            for d=1:size(stiffnessta,2)
                for e=1:size(stiffnesstb,2)
                    for f=1:size(stiffnesstb,2)
                        controller = [controller; [stiffnessx(1,a),stiffnessy(1,b),...
                                    stiffnessz(1,c),stiffnessta(1,d),...
                                    stiffnesstb(1,d),stiffnesstc(1,d)]];
                        
                    end
                end
            end
        end
    end
end
csvwrite('Controllers_Level2.csv',controller);