%% Uniqueness of values.

clear all;
close all;
clc;

% M = csvread("taux.csv");
% size(M,1)
% vec = unique(M(:,3));
% size(vec,1)

figure(1)
set(gcf,'color','w');
set(gca,'fontsize',30)
hold on;
V = {};
V{end+1} = [2.8,2.8,0.04;
            -0.825,0.225,-0.051;
            -0.6,0.6,0.021;
            0.45,0.75,0.048;
            0.075,0.025,0.0035];

V{end+1} = [2.8,-2.8,0.04;
            0.45,0.075,-0.0525;
            -1.125,-0.075,0.051;
            0.975,-0.75,0.039;
            0.375,0.2,0.006;
            0.01,-0.02,-0.003];
        
V{end+1} = [-2.8,2.8,-0.04;
            0.3,0.6,0.0525;
            0.225,0.3,-0.0285;
            0.15,0,0.0165
            0.1,-0.15,0.0075];
   
V{end+1} = [-2.8,-2.8,-0.04;
            -0.075,-0.05,0.012];

        
xlim([-3,3]);
ylim([-3,3]);
zlim([-3,3.5]);
xlabel('dx in mm','fontsize',30,'fontweight','bold');
ylabel('dy in mm','fontsize',30,'fontweight','bold');
zlabel('da in Degrees','fontsize',30,'fontweight','bold');
title('Learning Based Search','fontsize',30,'fontweight','bold')
x = 0;
y = 0;
z = 0;
c = ['r','b','g','c'];

for i=1:size(V,2)
    v = V{i};
    x = x + v(1,1);
    y = y + v(1,2);
    z = z + rad2deg(v(1,3));
    scatter3(v(1,1),v(1,2),rad2deg(v(1,3)),300,'k','*','Linewidth',5);
    text(v(1,1),v(1,2),rad2deg(v(1,3)),'   Initial Point','fontsize',30,'fontweight','bold');
    scatter3(v(end,1),v(end,2),rad2deg(v(end,3)),300,'k','*','Linewidth',5);
    plot3(v(:,1),v(:,2),rad2deg(v(:,3)),c(1,i),'Linewidth',2);
end

T = [1,0,0,x;
    0,1,0,y;
    0,0,1,z;
    0,0,0,1];

[x y z] = sphere(128);
r = 0.7;
h = surfl(x*r+T(1,4), y*r+T(2,4), z*r+T(3,4)); 
set(h, 'FaceAlpha', 0.1)
shading interp