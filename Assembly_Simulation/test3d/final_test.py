import numpy as np
import pandas as pd
import os
os.chdir('C:\\Users\\shaha\\Desktop\\Assembly\\Learning_data_3D')          # directory which has below 4 csv files 
testdata = pd.read_csv('testdata1.csv', names =["del_x","del_y","theta"])         # 59319 pairs of dx,dy,theta
pred_tau_x = pd.read_csv('taux.csv', names = ["meantaux","vartaux"])       # 2d targets for tau_x
pred_tau_y = pd.read_csv('tauy.csv', names = ["meantauy","vartauy"])       # 2d targets for tau_y
pred_tau_z = pd.read_csv('tauz.csv', names = ["meantauz","vartauz"])       # 2d targets for tau_z

def find_value(filename):
    testing = pd.read_csv(filename + ".csv", names = ["tau_x","tau_y","tau_z"]) # read torque file
    
    mmv_tx = np.array([testing.tau_x.mean(), testing.tau_x.var()]) # find max mean var tx
    mmv_ty = np.array([testing.tau_y.mean(), testing.tau_y.var()])# find max mean var ty
    mmv_tz = np.array([testing.tau_z.mean(), testing.tau_z.var()])# find max mean var tz

    p1 = pred_tau_x - mmv_tx
    diff1 = np.linalg.norm((p1),axis=1)                                 # euclidean distances for tau_x
    dat1 = pd.DataFrame(diff1)
    sort_dat1 = dat1.sort_values(by=[0]).head(1)

    p2 = pred_tau_y - mmv_ty
    diff2 = np.linalg.norm((p2),axis=1)                                 # euclidean distance for tau_y
    dat2 = pd.DataFrame(diff2)
    sort_dat2 = dat2.sort_values(by=[0]).head(1)

    p3 = pred_tau_z - mmv_tz
    diff3 = np.linalg.norm((p3),axis=1)                                 # euclidean distance for tau_z
    dat3 = pd.DataFrame(diff3)
    sort_dat3 = dat3.sort_values(by=[0]).head(1)

    q1 = testdata.loc[sort_dat1.index]
    q2 = testdata.loc[sort_dat2.index]
    q3 = testdata.loc[sort_dat3.index]

##    mno= np.zeros((5,1))
##    mno1 = np.zeros((5,1))
##    mno2 = np.zeros((5,1))
##                                                                        # find intersections
##    for i in range(0,5):
##        vb = np.linalg.norm(q1.iloc[i]-q2, axis =1)
##        mno[i] = np.min(vb)
##        mno1[i] = np.argmin(vb)

##    sa = q2.iloc[mno1.flatten()]
##    score = pd.DataFrame(mno.flatten())
##    stt = np.hstack((q1,sa,score))
##    inm = np.argmin(stt[:,4])
##    rw = stt[int(inm)]
    q = (q1 + q2 + q3)/3
##    delta_x = (rw[0]+ rw[2])/2
##    delta_y = (rw[1]+ rw[3])/2

    delx = q.del_x
    dely = q.del_y
    thet = q.theta
                  
    return delx,dely,thet

[dx,dy,theta] = find_value('trq')

print(dx)
print(dy)
print(theta)
