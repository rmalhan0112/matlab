%% Machine Learning DOE

% Generate a full factorial design provided the number of levels and factors.

clear all;
clc;
close all;

% design = fullfact([ 50 50 ]);
% 
% dx = [ -3 : 0.12 : 3  ];
% dy = [ -3 : 0.12 : 3 ];
% 
% for i=1:size(design)
%     design(i,1) = dx( 1,design(i,1) );
%     design(i,2) = dy( 1,design(i,2) );
% end
% 
% csvwrite('Learning_DOE.csv',design)


% design = fullfact(ones(1,5)*9);
% time = size(design,1)*3/3600/24
% 
% dx = [ -3 : 6/8 : 3  ];
% dy = [ -3 : 6/8 : 3 ];
% dA = [ -0.06:0.12/8:0.06 ];
% dB = [ -0.06:0.12/8:0.06 ];
% dC = [ -0.06:0.12/8:0.06 ];
% 
% for i=1:size(design)
%     design(i,1) = dx( 1,design(i,1) );
%     design(i,2) = dy( 1,design(i,2) );
%     design(i,3) = dA( 1,design(i,3) );
%     design(i,4) = dB( 1,design(i,4) );
%     design(i,5) = dC( 1,design(i,5) );
%     
% end
% 
% csvwrite('Learning_DOE_5D.csv',design)

n = 16;

design = fullfact(ones(1,3)*n);
time = size(design,1)*3/3600/24

dx = [ -3 : 6/n : 3  ];
dy = [ -3 : 6/n : 3 ];
dA = [ -0.06:0.12/n:0.06 ];

for i=1:size(design)
    design(i,1) = dx( 1,design(i,1) );
    design(i,2) = dy( 1,design(i,2) );
    design(i,3) = dA( 1,design(i,3) );
end
size(design,1)
csvwrite('Learning_DOE_3D.csv',design)