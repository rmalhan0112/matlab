clc
clear

%%Defining Tcp/Ip port

design = fullfact([3,3,3]);

size(design)

for r=1:27
    for c=1:3
        if (c==2) %%Velocity
            if design(r,c) == 1
                design(r,c) = 10;
            elseif design(r,c) == 2
                design(r,c) = 25;
            elseif design(r,c) == 3
                design(r,c) = 40;
            end 
            
        elseif (c==1) %%Force
            if design(r,c) == 1
                design(r,c) = 5;
            elseif design(r,c) == 2
                design(r,c) = 22.5;
            elseif design(r,c) == 3
                design(r,c) = 40;
            end 
        elseif (c==3) %%Theta
            if design(r,c) == 1
                design(r,c) = 40;
            elseif design(r,c) == 2
                design(r,c) = 55;
            elseif design(r,c) == 3
                design(r,c) = 70;
            end 
        end
    end
end

design(:,1)
design(:,2)
design(:,3)