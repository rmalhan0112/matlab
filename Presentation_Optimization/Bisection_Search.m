%% BiSection Search Optimization
clear
clc

a = 0;
b = 3;

figure(1)
X = [a:0.01:b];
% fun = @(x) (x-20).^2 - 1; %Parabola
fun = @(x) x.^3 - 3*x.^2;

plot(X,fun(X));
hold on

x_high = b;
x_low = a;
e = 0.01/2; % Distinguishability constant 2e = 0.01
hold on;
count=0;
tic
while (abs(x_high - x_low) > 2e-2 && count<1000)
    count = count + 1;
    x = (x_high + x_low)/2;
    x_right = x + e;
    x_left = x - e;
    f_right = fun(x_right);
    f_left = fun(x_left);
    f = fun(x);
    scatter(x,f,50,'r','x');
    
    if (f_right > f_left)
        x_high = x_right;
    else
        x_low = x_left;
    end
%     pause(0.3);
end
toc
fprintf('Number of Iterations: %d\n',count)
xopt = (x_high + x_low)/2;
f = fun(xopt);
fprintf('Optimum Value of X: %f with a function value of:  %f\n',xopt,f)