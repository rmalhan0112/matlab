%% Problem-2
%The Best Value is obtained as a part of a graph using default function.
%Discussion is provided in the comments itself. So please have a look at it
%before reducing my marks. I usually provide discussion in comments. 
%% Answer-A

clear
clc

disp('Answer-A')
%Values of a b c as given in Problem Statement
a=1e-1;
b=1e-4;
c=1e-3;

syms x1 x2 x3;
F=@(x)(x(1)/a)^4+(x(2)/b)^6+(x(3)/c)^8;

Fdash=(x1/a)^4+(x2/b)^6+(x3/c)^8;

Jacobian=[diff(Fdash,x1);
    diff(Fdash,x2);
    diff(Fdash,x3)]

Hessian=[diff(diff(Fdash,x1),x1),diff(diff(Fdash,x1),x2),diff(diff(Fdash,x1),x3);
        diff(diff(Fdash,x2),x1),diff(diff(Fdash,x2),x2),diff(diff(Fdash,x2),x3);
        diff(diff(Fdash,x3),x1),diff(diff(Fdash,x3),x2),diff(diff(Fdash,x3),x3)]

    
%% Answer-B

disp('Answer-B')
J=@(x)([(4*x(1)^3)*(1/a^4);(6*x(2)^5)*(1/b^6);(8*x(3)^7)*(1/c^8)]);
H=@(x)([(12*x(1)^2)*(1/a^4),0,0;0,(30*x(2)^4)*(1/b^6),0;0,0,(56*x(3)^6)*(1/c^8)]);

x0=[1,1,1];

options = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','PlotFcn',@optimplotfval);
[x,fval,a,b,c,Hess] = fminunc({F,J,H},x0,options);
fprintf('\nValue of the optimum values of x is %f, %f, %f',x(1,1),x(1,2),x(1,3))
fprintf('\nOptimum Value of the Function F1 is %f\n\n',fval)

Hessdiag=[Hess(1,1),Hess(2,2),Hess(3,3)];
disp('Hessian before scaling is')
Hess
fprintf('Ratio of Max to Min in Hessian diagonal elements is: %d\n\n\n',max(Hessdiag)/min(Hessdiag))
clear
%% Answer-C
disp('Answer-C')
a=1e-1;
b=1e-4;
c=1e-3;
%Scaling new Design Variables:

%Previous Diagonal Cooefficients in hessian were: 12e4 , 3e25 , 5.6e25
%Scaling is done in such a manner that the diagonal cooeficient values tend to be approx 1

%Scaled Coefficients are
A=a/(12^(2.5));   
B=b/(30^(0.005));
C=c/(56^(0.73));

%Note: Ax1 is actually y1. And so on.
x0=[1,1,1];

F=@(x)(A*(x(1)))^4 + (B*(x(2)))^6 + (C*(x(3)))^8; 

J=@(x)[(4*A*x(1)^3);(6*B*x(2)^5);(8*C*x(3)^7)];
H=@(x)[(12*A*x(1)^2),0,0;
    0,(30*B*x(2)^4),0;
    0,0,(56*C*x(3)^6)];

options = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','PlotFcn',@optimplotfval);
[y,fval,a,b,c,Hess] = fminunc({F,J,H},x0,options);

x(1,1)=y(1,1)*A;
x(1,2)=y(1,2)*B;
x(1,3)=y(1,3)*C;

fprintf('\nValue of the optimum values of x is %f, %f, %f',x(1,1),x(1,2),x(1,3))
fprintf('\nOptimum Value of the Function F1 is %f\n',fval)

%Displaying Hessian to find the ratio of Max to Min values of diagonal
%elements. This ratio should be approx equal to 1 for design variable
%scaling
Hessdiag=[Hess(1,1),Hess(2,2),Hess(3,3)];
disp('Hessian after scaling is')
Hess
fprintf('Ratio of Max to Min in after scaling Hessian diagonal elements is: %d',max(Hessdiag)/min(Hessdiag))

%% The main difference which comes after scaling is the Hessian. The ratio comes down from 3.8e12 to just 1.04.
%The function can reach more close to the minimum value after scaling.
%% The minimums and best values found have been displayed in the output.