x = sym('x',[10,1]);

g = 0;
for i = 1:9
    g = g + (10-i)*(x(i)^2-x(i+1))^2;
end

F = ((x(1)-1)^2+(x(10)-1)^2+10*g);

for i=1:N
    grad(i,1) = diff(F,x(i));
end
    
for i=1:N
    for j=1:N
        hessian(i,j) = diff(diff(F,x(i)),x(j));
    end
end
