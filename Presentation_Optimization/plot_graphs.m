%% I used this script to plot the results or comparison of optimizers.

close all;
clc;
clear all;

% % Time comparison for all optimizers for different line searches.
% bfgs_arm = [0.027, inf,  0.02916, 0.055, 3.6].*0.9;
% bfgs_gold = [0.000268, inf, 0.000255, 0.000312, 0.00018].*0.9;
% bfgs_bi = [0.038, inf, 0.4264, 0.842, 2.29].*0.9;
% 
% dfp_arm = [0.026, 0.02314, 0.0375, 0.032, inf].*0.9;
% dfp_gold = [0.000193, 0.000237, 0.000287, 0.000244, inf].*0.9;
% dfp_bi = [0.355, inf, 0.409, 0.822, inf].*0.9;
% 
% newton_arm = [0.03042, 0.09, 0.0267, 0.028, 0.09].*0.9;
% newton_gold = [0.000218, 0.0532, 0.000326, 0.000285, 0.000197].*0.9;
% newton_bi = [0.0268, 0.106, 0.0322, 0.00376, 0.075].*0.9;
% 
% grad_arm = [2.994, 0.044, 0.14, 0.968, 0.244].*0.9;
% grad_gold = [0.000253, 0.000314, 0.00028, 0.000263, 0.000288].*0.9;
% grad_bi = [1.35, 0.168, 0.2314, 1.98, 0.127].*0.9;


% Number of Iterations comparison for all optimizers for different line searches.
bfgs_arm = [103, inf, 119, 52, inf];
bfgs_gold = [104, inf, 103, 39, inf];
bfgs_bi = [93, inf, 94, 35, inf];

dfp_arm = [108, 25, 120, 59,inf];
dfp_gold = [1642, 44, 96, 32,inf];
dfp_bi = [105, inf, 96, 30,inf];

newton_arm = [55, 5, 56, 17, 636];
newton_gold = [48, 4, 49, 18, 627];
newton_bi = [50, 5, 55, 20, 636];

grad_arm = [inf, 409, 2916, 29553, 4576];
grad_gold = [inf, 1225, 6547, inf,7292];
grad_bi = [inf, 6154, 10934, 56633,6934];


cases = [1,2,3,4,5];

width = 3;

figure(1)
set(gcf,'color','w');
set(gca,'fontsize',25)
hold on;
xlabel('Case Index','fontsize',25);
ylabel('Iterations','fontsize',25);
scatter(cases,bfgs_arm,200,'*','r','Linewidth',width);
scatter(cases,dfp_arm,200,'+','r','Linewidth',width);
scatter(cases,newton_arm,200,'.','r','Linewidth',width);
scatter(cases,grad_arm,200,'x','r','Linewidth',width);

scatter(cases,bfgs_gold,200,'*','g','Linewidth',width);
scatter(cases,dfp_gold,200,'+','g','Linewidth',width);
scatter(cases,newton_gold,200,'.','g','Linewidth',width);
scatter(cases,grad_gold,200,'x','g','Linewidth',width);

scatter(cases,bfgs_bi,200,'*','b','Linewidth',width);
scatter(cases,dfp_bi,200,'+','b','Linewidth',width);
scatter(cases,newton_bi,200,'.','b','Linewidth',width);
scatter(cases,grad_bi,200,'x','b','Linewidth',width);

legend('BFGS','DFP','Newton','Gradient Descent')
% legend('Armijo','Golden Section','Bi-Section');

figure(2)
xlabel('Case Index','fontsize',25);
ylabel('Iterations','fontsize',25);
set(gcf,'color','w');
set(gca,'fontsize',25)
hold on;
ylim([0,120])
scatter(cases,bfgs_arm,200,'*','r','Linewidth',width);
scatter(cases,dfp_arm,200,'+','r','Linewidth',width);
scatter(cases,newton_arm,200,'.','r','Linewidth',width);
scatter(cases,grad_arm,200,'x','r','Linewidth',width);

scatter(cases,bfgs_gold,200,'*','g','Linewidth',width);
scatter(cases,dfp_gold,200,'+','g','Linewidth',width);
scatter(cases,newton_gold,200,'.','g','Linewidth',width);
scatter(cases,grad_gold,200,'x','g','Linewidth',width);

scatter(cases,bfgs_bi,200,'*','b');
scatter(cases,dfp_bi,200,'+','b');
scatter(cases,newton_bi,200,'.','b');
scatter(cases,grad_bi,200,'x','b');

legend('BFGS','DFP','Newton','Gradient Descent')