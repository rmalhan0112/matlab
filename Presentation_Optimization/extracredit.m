%% Extra Credit Homework
clc;
clear;
warning off;

%% Wing Design

%Defining the variables. For function handler
% x(1) = Payload to be carried
% x(2) = Full wing-span of aircraft
% x(3) = Thickness of the spar or diameter of circular cross-section
% x(4) = Chord length

%Weights of the Objective functions in MDO problem
w1=1; %Weight given to Payload
w2=0.7; %Weight given to spar
w3=0.2;  %Weight given to fuselage and engine
w4=1; %Weight given to fuel

rhomat=1450*3.6e-5; %Density of material from Kg/m^3 lb/in^3 (Plastic Kevlar)
Wfs=50; %Fuselage Load in lb
We=100; %Weight of Engine in lb
U=100*63360; %Cruise speed in inch per hour
C=0.85; %Thrust specific fuel consumption
Cl=0.1; %Coefficient of lift
rhoair=1.225*3.6e-5; %Density of air in Kg/m3
R=50*63360; %Range desirable in inches.
Cd=0.02;

%Defining the objective function
F =@(x) (-w1*x(1))+...
    (w2*(rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (w3*(Wfs+We))+... %Weight of fuselage and engine
     (w4*(0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))); %Weight of Fuel= Wi(1-e-(RCCd/UCl))
    
lb = [50,250,2,25];
ub = [200,500,20,150];
x0 = [150 100 3 70];

options = optimoptions('fmincon','Algorithm','sqp');
[x,fval] = fmincon(F,x0,[],[],[],[],lb,ub,@constraints,options);

fprintf('\nValue of the optimum values of x is %f, %f, %f,%f',x(1,1),x(1,2),x(1,3),x(1,4));
Payloadinlbs=x(1,1);
Wingspaninmeter=x(1,2)*0.0254;
Thicknessininches=x(1,3);
Chordininches=x(1,4);
rowname={'Values for Plastic Kevlar'};
table(Payloadinlbs,Wingspaninmeter,Thicknessininches,Chordininches,'Rownames',rowname)


function [c,cq] = constraints(x)
t = 3.14/2;
rhomat=1450*3.6e-5; %Density of material from Kg/m^3 lb/in^3 (Plastic Kevlar)
Wfs=50; %Fuselage Load in lb
We=100; %Weight of Engine
U=100; %Cruise speed in mph
C=0.85; %Thrust specific fuel consumption
Cl=0.1; %Coefficient of lift
rhoair=1.225*3.6e-5; %Density of air in Kg/m3
R=50; %Range desirable in miles.
Cd=0.02;

%c1 constraint comes from deflection formula
c1 = ((((x(2))^4)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*((45*pi*cos(t))+(90*t*((sin(t))^4)...
    *cos(t))+(1800*t*((sin(t))^2)*((cos(t))^3))-(1800*t*...
    ((sin(t))^2)*cos(t))+(90*t*((cos(t))^5))-(300*t*((cos(t))^3))...
    -(48*((sin(t))^5))-(150*((sin(t))^3)*((cos(t))^2))+...
    (80*((sin(t))^3))-(90*((sin(t)))*((cos(t))^4))+(300*((sin(t)))...
    *((cos(t))^2))-32))/(23040*18999943.6*(pi*(x(3)^4)/64)))-12;

%Bending Stress constraint
c2 = (((-(x(2)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*0.5*((t/2)-((sin(t)*cos(t))/2))))...
    *((2*x(3))/(3*pi))*(pi*(x(3)^2)/8))/((pi*(x(3)^4)/64)*...
    ((x(3))/2)))-(179846.8/2); %Yield of plastic kevlar

c3 = ((((x(2)^2)*((4*((x(1) + (rhomat*pi*(x(3)^2)*x(2)/4)) + ... %Payload and Spar weight
    (Wfs+We+x(1))+... %Weight of fuselage and engine
   (0.5*rhoair*U^2*Cl*x(2)*x(4)*(1-exp(-R*C*Cd/(U*Cl))))...%Weight of Fuel= Wi(1-e-(RCCd/UCl))
    ))/(x(2)*pi))*0.25*((-t*0.5*cos(t))-((sin(t)^3)/6)...
    +((sin(t))/2)))*((pi*(x(3)^4)/64)/(x(3)/2))/(pi*(x(3)^4)/64))...
    - 179846.8);

c = [c1;c2;c3];

cq = [(0.5*0.000036*1760*1760*0.1*x(2)*x(4))-(50+100+(x(1))+(0.1*pi*(x(3)^2)*x(2)/4)...
    +((50+100+(x(1))+(0.1*pi*(x(3)^2)*x(2)/4))*((exp((50*0.85*...
    (0.02+((0.1^2)/(pi*0.97*(x(2)/x(4)))) + (0.01*(x(3)/x(4)))...
    +(0.523/((log(0.06*(0.000036*1760*x(4)/0.1366)))^2))))/...
    (100*0.1)))-1)))];
end