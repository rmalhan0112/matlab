%% Golden Section Search Optimization
clear
clc
format long

golden_ratio = 0.618;
syms y
syms x

a = 0;
b = 3;

figure(1)
X = [a:0.01:b];
% fun = @(x) (x-20).^2 - 1; %Parabola
% fun = @(x) x.^3 - 3*x.^2;
fun = @(x) (20./x)+x.^2;

plot(X,fun(X));
hold on

%% Initialize
x_low = a;
x_high = b;
x_right = x_low + golden_ratio*(x_high-x_low);
x_left = x_low + (1-golden_ratio)*(x_high-x_low);

f_right = fun(x_right);
f_left = fun(x_left);

count =0;

tic
while (abs(x_high-x_low) > 1e-4 && count<100)
    count = count + 1;
    hold on;
    
    if (f_right > f_left)
        x_high = x_right;
        x_right = x_left;
        f_right = f_left; 
        x_left = x_high - golden_ratio*(x_high-x_low);
        f_left = fun(x_left);        
        xopt = (x_high + x_low)/2
        f = fun(xopt);
        scatter(xopt,f,200,'r','x');
%         pause(0.3)
    
    else     
        x_low = x_left;
        x_left = x_right;
        f_left = f_right;
        x_right = x_low + golden_ratio*(x_high-x_low);
        f_right = fun(x_right);
        xopt = (x_high + x_low)/2
        f = fun(xopt);
        scatter(xopt,f,200,'r','x');
%         pause(0.3)
    end
end
toc

fprintf('Number of Iterations: %d\n',count)
xopt = (x_high+x_low)/2;
f = fun(xopt);
fprintf('Optimum Value of X: %f and Value of F is: %f',xopt,f);