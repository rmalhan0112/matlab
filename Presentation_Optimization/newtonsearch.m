%% Newtons Method for 1-D search
clear
clc

syms x

a = 0;
b = 3;

figure(1)
X = [a:0.01:b];
% fun = @(x) (x-5).^2 - 1; %Parabola
fun = @(x) x.^3 - 3*x.^2;

plot(X,fun(X));
hold on

gradient = diff(fun,x);
hessian = diff(gradient,x);
xint = 1.2;
val = double(xint - subs(gradient,xint)/subs(hessian,xint));

count=0;
while (subs(gradient,val)>1e-2 && count<10)
    count = count + 1;
    if (subs(hessian,val)<1e-7)
        disp('Hessian Overruled')
        break
    end
    val = double(val - subs(gradient,val)/subs(hessian,val));
    disp(val)
    f = fun(val);
    scatter(val,f,50,'r','x');
    pause(1)
end