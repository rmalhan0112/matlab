%% Problem-3
%I AM USING SEQUENTIAL QUADRATIC PROGRAMMING OPTIMIZATION FOR MY PURPOSES
%% Answer-A

clc;
clear;
close all;
m=1;
n=1;
a1 = 3;
b1 = 10;
c1 = 1/3;
a2 = 1/2;
b2 = 20;
c2 = 1/2;

for x2=-3:0.01:3
    for x1=-3:0.01:3
   f1(n,m) = (a1*(1-x1)^2)*exp((-(x1)^2-((x2)+1)^2))- b1*((x1)/5-(x1)^3-...
       (x2)^5)*exp((-(x1)^2-(x2)^2))-c1*exp((-((x1)+1)^2-(x2)^2));
   
   f2(n,m) = (a2*(1-x1)^2)*exp((-(x1)^2-((x2)-1)^2))+ b2*(-(x1)/5-(x1)^3+...
       (x2)^5)*exp((-(x1)^2-(x2)^2))-c2*exp((-((x1)-1)^2-(x2)^2));
   
   f3(n,m) = f1(n,m)+ f2(n,m);    
   m=m+1;
    end
    n=n+1;
    m=1;
end


x1=-3:0.01:3;
x2=-3:0.01:3;

figure(1)
contourf(x1,x2,f1,'edgecolor','none')
colorbar
title('Contour Plot of Function F1')
xlabel('x1 Value')
ylabel('x2 Value')

figure(2)
contourf(x1,x2,f2,'edgecolor','none')
colorbar
title('Contour Plot of Function F2')
xlabel('x1 Value')
ylabel('x2 Value')

figure(3)
contourf(x1,x2,f3,'edgecolor','none')
colorbar
title('Contour Plot of Function F3')
xlabel('x1 Value')
ylabel('x2 Value')

%% Answer-B

x0=[0.5,-1];
lb=[-3,-3];
ub=[3,3];
nonlcon=@unitdisk;
a1 = 3;
b1 = 10;
c1 = 1/3;
a2 = 1/2;
b2 = 20;
c2 = 1/2;

%Optimum Values for f1
F1=@(x)(a1*(1-x(1))^2)*exp((-x(1))^2-((x(2))+1)^2)- b1*((x(1))/5-(x(1))^3-...
(x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c1*exp((-((x(1))+1)^2-(x(2))^2));

options = optimoptions('fmincon','TolCon',1e-6,'Algorithm','sqp');
[x,fval] = fmincon(F1,x0,[],[],[],[],lb,ub,nonlcon,options);
fprintf('\nValue of the optimum values of x is %f, %f, %f',x(1,1),x(1,2))
fprintf('\nOptimum Value of the Function F1 is %f\n',fval)

%Optimum Values for f2
F2 =@(x)(a2*(1-x(1))^2)*exp((-(x(1))^2-((x(2))-1)^2))+ b2*(-(x(1))/5-...
    (x(1))^3+(x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c2*exp((-((x(1))-1)^2-(x(2))^2));

x0=[0.5,-1.5];
lb=[-3,-3];
ub=[3,3];
nonlcon=@unitdisk;
options = optimoptions('fmincon','TolCon',1e-6,'Algorithm','sqp');
[x,fval] = fmincon(F2,x0,[],[],[],[],lb,ub,nonlcon,options);
fprintf('\nValue of the optimum values of x is %f, %f, %f',x(1,1),x(1,2))
fprintf('\nOptimum Value of the Function F2 is %f',fval)

%Optimum Values for f3
F3 =@(x)((a1*(1-x(1))^2)*exp((-x(1))^2-((x(2))+1)^2)- b1*((x(1))/5-...
    (x(1))^3-(x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c1*exp((-((x(1))+1)^2-(x(2))^2))+...
    (a2*(1-x(1))^2)*exp((-(x(1))^2-((x(2))-1)^2))+ b2*(-(x(1))/5-(x(1))^3+...
    (x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c2*exp((-((x(1))-1)^2-(x(2))^2)));

options = optimoptions('fmincon','TolCon',1e-6,'Algorithm','sqp');
[x,fval] = fmincon(F3,x0,[],[],[],[],lb,ub,nonlcon,options);
fprintf('\nValue of the optimum values of x is %f, %f, %f',x(1,1),x(1,2))
fprintf('\nOptimum Value of the Function F3 is %f',fval)

%%  Answer-C

a1 = 3;
a2 = 1/2;
b1 = 10;
b2 = 20;
c1 = 1/3;
c2 = 1/2;

%Creating a Fullfact design
design = fullfact([75 75]);

%Assigning values to each element in design matrix:
%For 75 levels, the increment is 0.0811 or 6/74
for col=1:2
    for i=1:1:5625
        multiplier=design(i,col);
        design(i,col)=-3+multiplier*0.0811;
    end
end

%Substituting values in f1 and f2 for scatter plot

f1scat= @(x) ((a1*((1-x(1))^2))*(exp(-((x(1))^(2))-((x(2)+1)^(2)))))...
    - (b1*(((x(1))/(5))-((x(1))^(3))-((x(2))^(5)))*exp(-((x(1))^(2))-(x(2)^(2))))...
    -(c1*exp(-((x(1)+1)^(2))-(x(2)^(2))));
   
f2scat= @(x) ((a2*((1-x(1))^2))*(exp(-((x(1))^(2))-((x(2)-1)^(2)))))...
    + (b2*(-(x(1)/(5))-((x(1))^(3))+((x(2))^(5)))*exp(-((x(1))^(2))-((x(2))^(2))))...
    -(c2*exp(-((x(1)-1)^(2))-(x(2)^(2))));
   
for i=1:5625
    p1 = design(i,:);
    f1dash(i,1) = f1scat(p1);
    f2dash(i,1) = f2scat(p1);
end

figure(4);
scatter(f1dash,f2dash,'b')
title('Scatter Plot for f2 vs f1 DOE')
xlabel('f1 Value')
ylabel('f2 Value')

%% Answer-D

%I compared the values of F1 and F2 in the DOE generated above
%and removed the values if smaller values were gound and put them in non
%dominated species.

k=0;
counter=0;
for j=1:1:5625
    for i=1:1:5625
        if (f1dash(i,1)<f1dash(j,1)&&f2dash(i,1)<f2dash(j,1))
            
        else
            counter=counter+1;
        end
    end
    if (counter==5625)
       k=k+1;
       nondom1(k,1)=f1dash(j,1);
       nondom2(k,1)=f2dash(j,1);       
    end
counter =0;
end
nondom = [nondom1 nondom2];
fprintf('\n\nThe Non-Dominating value Matrix is:\n')
disp(nondom)

figure;
scatter(f1dash,f2dash,'b')
title('Scatter Plot f2 vs f1 with Non-Dominated Species')
xlabel('f1 Value')
ylabel('f2 Value')
hold on;
nondom = sortrows(nondom,1);
scatter(nondom(:,1),nondom(:,2),'k','filled');

figure;
scatter(f1dash,f2dash,'b')
title('Zoomed-In Scatter Plot f2 vs f1 with Non-Dominated Species')
xlabel('f1 Value')
ylabel('f2 Value')
hold on;
nondom = [nondom1 nondom2];
nondom = sortrows(nondom,1);
plot(nondom(:,1),nondom(:,2));
ylim([-18 -14]);
xlim([-7 -5]);

%% Answer-E Pareto Front

%Defining the fitness function

fitness1=@(x)[(a1*(1-x(1))^2)*exp((-x(1))^2-((x(2))+1)^2)- b1*((x(1))/5-(x(1))^3-...
(x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c1*exp((-((x(1))+1)^2-(x(2))^2)),(a2*(1-x(1))^2)*...
exp((-(x(1))^2-((x(2))-1)^2))+ b2*(-(x(1))/5-...
    (x(1))^3+(x(2))^5)*exp((-(x(1))^2-(x(2))^2))-c2*exp((-((x(1))-1)^2-(x(2))^2))];

%Using the gamultiobj function

disp('Values of x are:')
x=gamultiobj(fitness1,2,[],[],[],[],[-3;-3],[3;3])
%rng default
figure
[n,m]=size(x);
for i=1:n
    x1=x(i,:);
    f(i,1)=f1scat(x1);
    f(i,2)=f2scat(x1);
end

scatter(f(:,1),f(:,2),'b')
title('Pareto Front')
xlabel('f1')
ylabel('f1')

%Overlaying the Pareto Front against the Non-Dominating set of Designs
figure
scatter(f(:,1),f(:,2),'b')
title('Pareto Front with Non dominating set')
xlabel('f1')
ylabel('f1')
hold on
plot(nondom(:,1),nondom(:,2));
legend('Pareto Front','Non Dominating set')

function [c,ceq] = unitdisk(x)
c = [];
ceq = [];
end