%% Problem-1 Heuristics

%% Answer-A

clear
clc
%Defining the Eggcrate function:

F=@(x)(x(1)^2+x(2)^2+25*(sin(x(1))^2+sin(x(2))^2));

%Defining the parameters of the GA function in Matlab

lb=[-2*pi];
ub=[2*pi];      %Lower and Upper bounds respectively
A=[];
Aeq=[];
b=[];
beq=[];         %Linear Constraints

nov=2;          %Giving ga function number of variables

%Generating initial population matrix

Popx1=-6.28+rand(100,1)*6.28*2;
Popx2=-6.28+rand(100,1)*6.28*2;

for i=1:1:100
    Pop(i,1)=Popx1(i,1);
    Pop(i,2)=Popx2(i,1);
end

%Creating Egg-Crate function with Initial Population Values
a=-2*pi:0.01:2*pi;
[x,y]=meshgrid(a);
levels=1:3:300;
f = x.^2 + y.^2+ 25.*(sin(y).^2+sin(x).^2);
figure(1)
contour(x,y,f,levels)
hold on
scatter(Popx1,Popx2,'k','filled')
title('Initial Population for Genetic Algorithm')
xlabel('x1 value')
ylabel('x2 value')
legend('Contour Plot','Black dot represents initial population')
hold off

opts = gaoptimset('PopulationSize',100,'TolCon',1e-6,'TolFun',1e-6,...
    'Generations',100,'CrossoverFraction',0.8,'InitialPopulation',Pop,'PlotFcn',...
    ({@gaplotbestf, @gaplotexpectation,@gaplotselection,@gaplotbestindiv,@gaplotstopping,@gaplotscores}));

[xf,fval,exitFlag,a,finalpop] = ...
    ga(F,nov,A,Aeq,b,beq,lb,ub,[],opts);
disp('Value of final x is')
disp(xf)
disp('Value of Fminimum is')
disp(fval)

for i=1:1:100
    Popfx1(i,1)=finalpop(i,1);
    Popfx2(i,1)=finalpop(i,2);
end

%Creating Egg-Crate function with Final Population Values
a=-2*pi:0.01:2*pi;
[x,y]=meshgrid(a);
levels=1:3:300;
f = x.^2 + y.^2+ 25.*(sin(y).^2+sin(x).^2);
figure(3)
contour(x,y,f,levels)
hold on
scatter(Popfx1,Popfx2,'k','filled')
title('Final Population for GA')
xlabel('x1 value')
ylabel('x2 value')
legend('Contour Plot','Black dot is final population')
hold off


%% Answer-B

%Generating initial population matrix

Popx1=-6.28+rand(100,1)*6.28*2;
Popx2=-6.28+rand(100,1)*6.28*2;

for i=1:1:100
    Pop(i,1)=Popx1(i,1);
    Pop(i,2)=Popx2(i,1);
end

%Creating Egg-Crate function with Initial Population Values
a=-2*pi:0.01:2*pi;
[x,y]=meshgrid(a);
levels=1:3:300;
f = x.^2 + y.^2+ 25.*(sin(y).^2+sin(x).^2);
figure(4)
contour(x,y,f,levels)
hold on
scatter(Popx1,Popx2,'k','filled')
title('Initial Population for Particle Swarm')
xlabel('x1 value')
ylabel('x2 value')
legend('Contour Plot','Black dot represents initial population')
hold off

%Note: SelfAdjustmentWeight is Self Confidence here. 
%SocialAdjustmentWeight is swarm confidence
options = optimoptions('particleswarm','SwarmSize',100,'SelfAdjustmentWeight',1.5,...
    'FunctionTolerance',1e-6,'SocialAdjustmentWeight',2,'InitialSwarmMatrix',Pop,...
    'InertiaRange',[0.1,0.8],'MaxTime',50,'PlotFcn',...
    ({@pswplotbestf}));


[xf,fval] = ...
    particleswarm(F,nov,lb,ub,options);
disp('Value of final x after particle-swarm is')
disp(xf)
disp('Value of Fminimum after particle swarm is')
disp(fval)

%Creating Egg-Crate function with Final Population Values
a=-2*pi:0.01:2*pi;
[x,y]=meshgrid(a);
levels=1:3:300;
f = x.^2 + y.^2+ 25.*(sin(y).^2+sin(x).^2);
figure
contour(x,y,f,levels)
hold on
scatter(xf(1,1),xf(1,2),'k','filled')
title('Final Population for Particle Swam')
xlabel('x1 value')
ylabel('x2 value')
legend('Contour Plot','Black dot represents final population')
hold off