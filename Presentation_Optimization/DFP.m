%% BFGS method for Rosenbrock Function

%This is Quasi Newton method with BFGS update which takes a multivariate function as
%input and calculates the minima for the function. It uses Golden Section to
%compute the step size.
clear all
clc
close all
warning('Off','all');
global pdloss
global feval
global geval
pdloss = 0;
geval = 0;
feval = 0;


% run f282.m
% run f283.m
% run f287.m
% run f288.m
run freudenstein.m

%% Initialization        
%Initial Value of Point from where to start search
x_p = x_int;
g_p = gradient(x_p);    % Gradient Evaluation
H = eye(N);                % Identity matrix as Bk
counter = 0;
iter = 1e5;
e = 1e-5;
flag=0;


%% Algorithm:
% Direction Vector for Search is Negation of Gradient
tic
while ( norm(g_p) > e && counter < iter )
    counter = counter + 1;
%     fprintf('Iteration: %d\n',counter)
    if (counter ~= 1) 
        [H_dash,flag] = update_H(H,dx,dg);
        if (flag==1) H = H_dash; end
    end
    dir_vec = H*(-g_p);
%     s = armijo_search( x_p, dir_vec, g_p, func_eval);
%     s = goldensection_search( x_p, dir_vec , g_p, func_eval );
    s = bisection( x_p, dir_vec , func_eval );
    x_c = x_p + s*dir_vec;
    g_c = gradient(x_c);
    geval = geval + 1;
    dg = g_c - g_p;
    dx = x_c - x_p;
    g_p = g_c;
    x_p = x_c;
end
toc
fprintf('\n\nOptimum Value:  \n');
disp(x_p(:,1))
fprintf('Function Value: %f\n',func_eval(x_p));
fprintf('Gradient: %f\n',norm(g_p));
fprintf('Function Evaluations: %d\n',feval)
fprintf('Gradient Evaluations: %d\n',geval)
fprintf('Positive Definiteness Loss: %d\n',pdloss)
fprintf('Iterations to converge: %d\n',counter)










%% Function Definitions

% Armijo's Search
% This function takes, current point x_k and direction as input and gives 
%the best step length to be taken s as output.
function s = armijo_search( x_c , dir_vec, g, func_eval)
    global feval
    alpha = 1e-8;
    eeta = 2;
    theta = 0.1;
    iter = 1e5;
    counter = 0;
    f_c = func_eval(x_c);
    
%Check for Armijo Condition:
    if ( func_eval(x_c + alpha * dir_vec) <= f_c + theta*alpha*(g'*dir_vec) )
        while( func_eval(x_c + alpha * dir_vec) <= f_c + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            feval = feval + 1;
            alpha = eeta * alpha;
        end
        alpha = alpha / eeta;
    elseif ( func_eval(x_c + alpha * dir_vec) > f_c + theta*alpha*(g'*dir_vec) ) 
        while ( func_eval(x_c + alpha * dir_vec) > f_c + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = alpha / eeta;
            feval = feval + 1;
        end
        alpha = alpha / eeta;
    end
    s = alpha;
end




%% Golden section search
function step = goldensection_search( x_c , dir_vec , g, func_eval )
    global feval;

    % Golden Section Search Optimization    
    golden_ratio = 0.618;
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
%     feval = feval + 1;
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
%         feval = feval + 1;
        step = step*eeta;
    end
    a = 0;
    b = step;

    % Initialize
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    x_right = x_low + golden_ratio*(x_high-x_low);
    x_left = x_low + (1-golden_ratio)*(x_high-x_low);

    f_right = func_eval(x_right);
    feval = feval + 1;
    f_left = func_eval(x_left);
    feval = feval + 1;

    count = 0;

    tic
    while (norm(x_high-x_low) > 1e-4 && count<1e5)
        count = count + 1;

        if (f_right > f_left)
            x_high = x_right;
            x_right = x_left;
            f_right = f_left; 
            x_left = x_high - golden_ratio*(x_high-x_low);
            f_left = func_eval(x_left);
            feval = feval + 1;
        else     
            x_low = x_left;
            x_left = x_right;
            f_left = f_right;
            x_right = x_low + golden_ratio*(x_high-x_low);
            f_right = func_eval(x_right);
            feval = feval + 1;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end



% Bisection Search
function step = bisection( x_c , dir_vec , func_eval )
    global feval;
    
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
%     feval = feval + 1;
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
%         feval = feval + 1;
        step = step*eeta;
    end
    a = 0;
    b = step;
    iter = 1e5;
    count=0;
    
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    e = 1e-4/2; % Distinguishability constant 2e = 0.01
    
    while (norm(x_high - x_low) > 1e-2 && count<iter)
        count = count + 1;
        x = (x_high + x_low)/2;
        x_right = x + e*dir_vec;
        x_left = x - e*dir_vec;
        f_right = func_eval(x_right);
        f_left = func_eval(x_left);
        feval = feval + 1;
        feval = feval + 1;
        if (f_right > f_left)
            x_high = x_right;
        else
            x_low = x_left;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end

function [H_dash,flag] = update_H(H,dx,dg)
    flag = 1;
    n = size(dx,1);
    rho = 1/ (dg'*dx);
    if (rho<=0) flag=0; H_dash=H; return; end
    H_dash = (eye(n) - (rho*(dx*dg'))) * H * (eye(n) - rho*(dg*dx')) + rho*(dx*dx');
end