%% Newton's Method for N-Dimension

%This is Newton's method which takes a multivariate function as
%input and calculates the minima for the function. It uses Golden Section to
%compute the step size.
clear all
clc
close all

warning('Off','all');
global pdloss
global feval
global geval
global hesseval;
pdloss = 0;
geval = 0;
feval = 0;
hesseval = 0;

% run f282.m
% run f283.m
% run f287.m
% run f288.m
run freudenstein.m

%% Initialization        
%Initial Value of Point from where to start search

x_c = x_int;
g = gradient(x_c);    %Gradient Evaluation
geval = geval + 1;
h = hessian(x_c);     % Hessian Evaluation
hesseval = hesseval + 1;
counter = 0;
iter = 1e4;
e = 1e-5;

rate = [];
dist_to_opt = [];
xopt = [11.4,-0.89];
%% Algorithm:
% Direction Vector for Search is Negation of Gradient
tic
while ( norm(g) > e && counter < iter )
    counter = counter + 1;
%     fprintf('Iteration: %d\n',counter)
    
    % Cholesky Factorization
    [L,D] = ldl(h);
 
    % Positive Definitiveness check
    for k=1:size(x_c)
        if (D(k,k)<0) D(k,k)=1.2; pdloss = pdloss + 1; end
    end
    A = L*D*L';
    dir_vec = A\(-g);
    dir_vec = dir_vec / norm(dir_vec);
%     s = armijo_search( x_c, dir_vec, g , func_eval);
    s = goldensection_search( x_c , dir_vec , g, func_eval );
%     s = bisection( x_c, dir_vec, func_eval );    
    if (s>1) s=1; end
    rate = [rate; (norm(x_c + s*dir_vec - xopt)/norm(x_c-xopt))];
    dist_to_opt = [dist_to_opt; norm(xopt-x_c)];
    x_c = x_c + s*dir_vec;
    g = gradient(x_c);
    geval = geval + 1;
    h = hessian(x_c);
    hesseval = hesseval + 1;
end
toc

figure(1)
set(gcf,'color','w')
set(gca,'fontsize',35)
plot([1:counter],dist_to_opt,'r','Linewidth',3)
xlabel('Iterations','fontsize',25);
ylabel('Distance to Optimum','fontsize',25);
legend('Golden-Section Search')

fprintf('\nRate of Convergence:  %f\n',max(rate))
fprintf('\n\nOptimum Value:  \n');
disp(x_c(:,1))
fprintf('Function Value: %f\n',func_eval(x_c));
fprintf('Gradient: %f\n',norm(g));
fprintf('Function Evaluations: %d\n',feval)
fprintf('Gradient Evaluations: %d\n',geval)
fprintf('Positive Definiteness Loss: %d\n',pdloss)
fprintf('Iterations to converge: %d\n',counter)







%% Function Definitions

% Armijo's Search
% This function takes, current point x_k and direction as input and gives 
%the best step length to be taken s as output.
function s = armijo_search( x_c , dir_vec, g, func_eval)
    global feval
    alpha = 1e-8;
    eeta = 2;
    theta = 0.1;
    iter = 1e5;
    counter = 0;
    f_c = func_eval(x_c);
    
%Check for Armijo Condition:
    if ( func_eval(x_c + alpha * dir_vec) <= f_c + theta*alpha*(g'*dir_vec) )
        while( func_eval(x_c + alpha * dir_vec) <= f_c + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            feval = feval + 1;
            alpha = eeta * alpha;
        end
        alpha = alpha / eeta;
    elseif ( func_eval(x_c + alpha * dir_vec) > f_c + theta*alpha*(g'*dir_vec) ) 
        while ( func_eval(x_c + alpha * dir_vec) > f_c + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = alpha / eeta;
            feval = feval + 1;
        end
        alpha = alpha / eeta;
    end
    s = alpha;
end




%% Golden section search
function step = goldensection_search( x_c , dir_vec , g, func_eval )
    global feval;

    % Golden Section Search Optimization    
    golden_ratio = 0.618;
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
%     feval = feval + 1;
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
%         feval = feval + 1;
        step = step*eeta;
    end
    a = 0;
    b = step;

    % Initialize
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    x_right = x_low + golden_ratio*(x_high-x_low);
    x_left = x_low + (1-golden_ratio)*(x_high-x_low);

    f_right = func_eval(x_right);
    feval = feval + 1;
    f_left = func_eval(x_left);
    feval = feval + 1;

    count = 0;

    tic
    while (norm(x_high-x_low) > 1e-4 && count<1e5)
        count = count + 1;

        if (f_right > f_left)
            x_high = x_right;
            x_right = x_left;
            f_right = f_left; 
            x_left = x_high - golden_ratio*(x_high-x_low);
            f_left = func_eval(x_left);
            feval = feval + 1;
        else     
            x_low = x_left;
            x_left = x_right;
            f_left = f_right;
            x_right = x_low + golden_ratio*(x_high-x_low);
            f_right = func_eval(x_right);
            feval = feval + 1;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end



% Bisection Search
function step = bisection( x_c , dir_vec , func_eval )
    global feval;
    
    step = 1e-8;
    eeta = 2;
    f_xc = func_eval(x_c);
%     feval = feval + 1;
    while( (func_eval( x_c + step * dir_vec ) - f_xc) < 0 )
%         feval = feval + 1;
        step = step*eeta;
    end
    a = 0;
    b = step;
    iter = 1e5;
    count=0;
    
    x_low = x_c + a * dir_vec;
    x_high = x_c + b * dir_vec;
    e = 1e-4/2; % Distinguishability constant 2e = 0.01
    
    while (norm(x_high - x_low) > 1e-2 && count<iter)
        count = count + 1;
        x = (x_high + x_low)/2;
        x_right = x + e*dir_vec;
        x_left = x - e*dir_vec;
        f_right = func_eval(x_right);
        f_left = func_eval(x_left);
        feval = feval + 1;
        feval = feval + 1;
        if (f_right > f_left)
            x_high = x_right;
        else
            x_low = x_left;
        end
    end
    xopt = (x_high + x_low)/2;
    step = norm(xopt - x_c)/norm(dir_vec);
end