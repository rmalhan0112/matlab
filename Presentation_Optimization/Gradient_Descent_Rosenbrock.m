%% RosenBrock Steepest Descent

clear all
clc
close all

% n = input('Enter the Dimension Number n');
% a = input('Enter the Parameter Alpha');
n=2;
global a;
a = 1;

%Plotting the Function
xopt = ones(n,1);
[x,y] = meshgrid(-2:0.1:2); %Rosenbrock
z = a*(y-x.^2).^2+(1-x).^2;
figure('units','normalized','outerposition',[0 0 1 1])
hold on;
surf(x,y,z)
xlabel('x1 value');
ylabel('x2 value');
zlabel('Function Value');
scatter3( xopt(1,1),xopt(2,1),func(xopt),500,'k','*','LineWidth',5 );
view(-140,30);

%% Initialization        
%Initial Value of Point from where to start search
x_int = [-1.2;1];
x_c = x_int;
g = gradient(x_c);    %Gradient Evaluation
counter = 0;
iter = 1000;
e = 1e-8;
scatter3( x_c(1,1),x_c(2,1),func(x_c),300,'k','x','LineWidth',3 );
flag=0;


%% Algorithm:
%Direction Vector for Search is Negation of Gradient
while ( norm(g) > e && counter < iter )
    counter = counter + 1;
    fprintf('Iteration: %d\n',counter)
    dir_vec = -g;
    dir_vec = dir_vec / norm(dir_vec);
    s = armijo_search( x_c , dir_vec , @func, g);
    x_c = x_c + s*dir_vec;
    g = gradient(x_c);
    scatter3( x_c(1,1),x_c(2,1),func(x_c),300,'r','x','LineWidth',3 );
    pause(0.0000001)
end
fprintf('Optimum Value: %f\n',x_c);
fprintf('Function Value: %f\n',func(x_c));
fprintf('Gradient: %f\n',norm(g));







%% Function Definitions

% Function Handle
function y = func(X)
    global a;
    n = size(X,1);
    n = n/2;
    y = 0;
    for i=1:1:n
        y = y + ( a*( X(2*i,1)-X(2*i-1,1)^2 )^2 + ( 1-X(2*i-1,1) )^2 );
    end
end

% Gradient
function g = gradient(X)
    global a;
    n = size(X,1);
%     n = n/2;
    for i=1:2:n
        g(i,1) = ( -2*a*( X(i+1,1)-X(i,1)^2 )*( 2*X(i,1) ) - 2*( 1-X(i,1) ) );
        g(i+1,1) = 2*a*( X(i+1,1)-X(i,1)^2 );
    end
end

% Hessian
function h = hessian(X)
    global a;
    n = size(X,1);
    n = n/2;
    for i=1:1:2*n
        for j=1:1:2*n
            % If row is odd
            if (mod(i,2)~=0)
                if (i==j) h(i,j)= -4*a*(X(i+1,1)-X(i,1)^2)+8*a*X(i,1)+2; end
                if (j==i+1) h(i,j)= 4*a*X(i,1)^3; end
                if (i~=j && j~=i+1) h(i,j)= 0; end
            end
            
            % If row is even
            if (mod(i,2)==0) 
                if (i==j) h(i,j)= -2*a*X(i-1,1)^2; end
                if (j==i-1) h(i,j)= -4*a*X(i-1,1); end
                if (i~=j && j~=i-1) h(i,j)= 0; end
            end
        end
    end
end


% Armijo's Search
% This function takes, current point x_k and direction as input and gives 
%the best step length to be taken s as output.
function s = armijo_search( x_c , dir_vec , func_eval, g )

    alpha = 1;
    eeta = 2;
    theta = 0.5;
    iter = 1e4;
    counter = 0;
    
%Check for Armijo Condition:
    if ( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while( func_eval(x_c + alpha * dir_vec) <= func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = eeta * alpha;
        end
        alpha = alpha / eeta;
    elseif ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + theta*alpha*(g'*dir_vec) )
        while ( func_eval(x_c + alpha * dir_vec) > func_eval(x_c) + ...
                theta*alpha*(g'*dir_vec) ) && counter < iter
            counter = counter + 1;
            alpha = alpha / eeta;
        end
        alpha = alpha / eeta;
    end
    s = alpha;
end