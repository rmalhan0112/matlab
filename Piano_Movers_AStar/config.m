disp('Loading Config Files...')
set(0,'DefaultFigureRenderer','opengl');

%% CONSTANTS
CONFIG.DEG2RAD = pi/180;
CONFIG.RAD2DEG = 180/pi;
CONFIG.DEBUG_MODE = false;

%% ROBOT PARAMETERS
CONFIG.DOF = 3;

CONFIG.MOBILE_BASE_MAX_LINEAR_SPEED = 1.0; % Max speed in m/s
CONFIG.MOBILE_BASE_MAX_ANGULAR_SPEED = 1.5; % Max angular speed in rad/sec

% max rotation speed in rad/sec
CONFIG.MANIPULATOR_MAX_JOINT_SPEED = 50*CONFIG.DEG2RAD;

CONFIG.MOBILE_BASE_DIM = [0.5, 0.5]; % Dimension of the mobile robot
CONFIG.MOBILE_BASE_DIM_RAD = sqrt(CONFIG.MOBILE_BASE_DIM(1)^2 + CONFIG.MOBILE_BASE_DIM(2)^2)*0.5;

%% MAP PARAMETERS
CONFIG.MAP_BOUNDS = [0, 200; % max x value in meters
                     0, 200]; % max y value in meters
                     

CONFIG.MAP_DELTAS = [2; % step size in x in meters
                     2]; % step size in y in meters
                     

CONFIG.ORI_DELTA = 30*CONFIG.DEG2RAD;
                 
CONFIG.DISCRETE_MAP_BOUNDS = [(CONFIG.MAP_BOUNDS(1,2) - CONFIG.MAP_BOUNDS(1,1))/CONFIG.MAP_DELTAS(1);
                              (CONFIG.MAP_BOUNDS(2,2) - CONFIG.MAP_BOUNDS(2,1))/CONFIG.MAP_DELTAS(2)];
                          
CONFIG.DISCRETE_ORI_BOUND = floor((pi)/CONFIG.ORI_DELTA)+1;

% the logic below get the number of digits in the max discrete value of the
% joints
CONFIG.STR_LEN = -1;
max_discrete_val = max([CONFIG.DISCRETE_MAP_BOUNDS(:)', CONFIG.DISCRETE_ORI_BOUND]);
if max_discrete_val < 10
   CONFIG.STR_LEN = 1;
else
    ten_pow = 2;
    while(1)
        divident = 10.0^ten_pow;
        quo = floor(max_discrete_val/divident);
        if quo == 0
            CONFIG.STR_LEN = ten_pow;
            break;
        end
        ten_pow = ten_pow + 1;
    end
end

%% PLANNING PARAMETERS
CONFIG.PRIMITIVES_SIM_TIME = 1; % time in sec
CONFIG.PRIMITIVE_ANGLES = [-CONFIG.ORI_DELTA, 0, CONFIG.ORI_DELTA];
CONFIG.PRIMITIVE_LEN = CONFIG.MAP_DELTAS(1,1)*3;
CONFIG.NEIGHS = [[0, 1, 0];
                 [1, 0, 0];
                 [0, -1, 0];
                 [-1, 0, 0];
                 [0, 0, -CONFIG.ORI_DELTA];
                 [0, 0, CONFIG.ORI_DELTA];
                 ];
CONFIG.GOAL_TOLERANCE = 5;

% normalized RGB values of colors
CONFIG.blue=[135 206 250]/255;
CONFIG.purple=[51 0 102]/255;
CONFIG.orange=[255 128 0]/255;
CONFIG.yellow=[255,255,0]/255;


%%
% %% number of rows in joint bounds should match MANIPULATOR DOF
% CONFIG.JOINT_BOUNDS = [-pi, pi; % base
%                        -pi, pi; % shoulder
%                        -pi, pi; % elbow
%                        -pi, pi; % wrist 1
%                        -pi, pi; % wrist 2
%                        -pi, pi]; % wrist 3
%                    
% % number of links in robot_links should match MANIPULATOR DOF
% robot_links = {};
% robot_links{end+1} = [0, -89.5, 0, 1000; 0, 0, 0, 1000; 0, 0, 38.5, 1000]'./1000; % Link1
% robot_links{end+1} = [425, 0, 0, 1000; 425, 0, 70.5, 1000; 0, 0, 70.5, 1000; 0, 0, 0, 1000]'./1000;
% robot_links{end+1} = [392.43, 0, 22.5, 1000; 392.43, 0, 0, 1000; 0, 0, 0, 1000]'./1000;
% robot_links{end+1} = [0, -109.15, 0, 1000; 0, 0, 0, 1000]'./1000;
% robot_links{end+1} = [0, 94.65, 0, 1000; 0, 0, 0, 1000]'./1000;
% robot_links{end+1} = [0, 0, -82.30, 1000; 0, 0, 0, 1000]'./1000;
% 
% robot_links_length = {};
% for jid = 1:CONFIG.MANIPULATOR_DOF
%     robot_pts = robot_links{jid};
%     curr_dists = [];
%     if size(robot_pts,2) > 1
%         for rpid = 1:size(robot_pts,2)-1
%             dist = compute_distance(robot_pts(1:3,rpid)', robot_pts(1:3, rpid+1)');
%             curr_dists = [curr_dists; dist];
%         end
%         robot_links_length{end+1} = curr_dists;
%     else
%         disp('ERROR')
%     end
% end
% 
% CONFIG.ROBOT_LINKs = robot_links;
% CONFIG.ROBOT_LINK_DISTs = robot_links_length;
% 
% CONFIG.MOBILE_BASE_OBJ = Mobile_Robot(0, 0, 0);
% T_mobilerobot_world = CONFIG.MOBILE_BASE_OBJ.homogeneous_pose();
% CONFIG.MANIPULATOR_OBJ = UR5(T_mobilerobot_world);
% 
% x_ur5_base = 0.21;
% y_ur5_base = 0;
% z_ur5_base = 0.68;
% phi_ur5_base = pi/2;
% T_ur5_mobilebase = ROTZ(phi_ur5_base);
% T_ur5_mobilebase(4,:) = [0,0,0];
% T_ur5_mobilebase(:,4) = [x_ur5_base; y_ur5_base; z_ur5_base; 1];
% CONFIG.T_ur5_mobilebase = T_ur5_mobilebase;