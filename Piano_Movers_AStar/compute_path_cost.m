function [lin_dist, rot_dist] = compute_path_cost( in_path )
    lin_dist = 0;
    rot_dist = 0;
    for idx = 1:size(in_path,1)-1
        c_wp = in_path(idx,:);
        n_wp = in_path(idx+1,:);
        dist = compute_distance_2D(c_wp, n_wp);
        if compute_distance_2D(c_wp, n_wp) > 0
            lin_dist = lin_dist + dist;
        end
        
        if abs(c_wp(3)-n_wp(3)) > 0
            rot = abs(c_wp(3)-n_wp(3));
            rot_dist = rot_dist + rot;
        end
%         total_cost = total_cost + compute_distance_2D(c_wp, n_wp);
    end
end

