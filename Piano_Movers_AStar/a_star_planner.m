% function [computed_path, cost_to_goal] = a_star_planner(s_I, s_G, in_CONFIG)
    disp('Running A* Planner...')    
    computed_path = [];
    cost_to_goal = 0;
    NODES_MAP = containers.Map;
    OPEN_SET = PriorityQueue();
    
    s_I_d = get_discrete_state(s_I, in_CONFIG)
    s_G_d = get_discrete_state(s_G, in_CONFIG)
    
    max_dist_cost = 2*compute_distance_2D(s_I, s_G);
    NODES = {};
    total_nodes_id = 0;
    n_I = Node(s_I);
    n_I.is_open = true;
    n_I.is_closed = false;
    n_I.g_cost = 0;
    new_h_cost = (compute_distance_2D(s_I, s_G)/max_dist_cost) + abs(s_I(3)-s_G(3))/pi
    n_I.h_cost = new_h_cost;
    n_I.t_cost =  n_I.g_cost +  n_I.h_cost;
    n_I.state_d = get_discrete_state(s_I, in_CONFIG);
%     v1 = n_I.state_d
    total_nodes_id = total_nodes_id + 1;
    n_I.node_id = total_nodes_id;
    n_I.map_key = get_key_for_state(n_I.state_d, in_CONFIG.STR_LEN);
    NODES{end+1} = n_I;
    NODES_MAP(n_I.map_key) = n_I.node_id;
    OPEN_SET.push(n_I.node_id, n_I.g_cost, n_I.h_cost);
    
    n_G = Node(s_G);
    n_G.state_d = get_discrete_state(s_G, in_CONFIG);
%     v2 = n_G.state_d
    n_G.map_key = get_key_for_state(n_G.state_d, in_CONFIG.STR_LEN);
    
    counter = 0;

    while(OPEN_SET.size() > 0)
        curr_id = OPEN_SET.top();
        OPEN_SET.pop();
        n_curr = NODES{curr_id};
        n_curr.is_open = false;
        n_curr.is_closed = true;
             
%         n_state_deg = n_curr.state;
%         n_state_deg(3) = n_state_deg(3)*CONFIG.RAD2DEG;
%         plot_tree(n_state_deg, in_CONFIG.yellow);
    
        hold on; 
        plot(n_curr.state(1), n_curr.state(2), 'yo')
        plot(n_curr.state(1), n_curr.state(2), 'k.')
        hold off;
        
%         n_d = [n_curr.node_id, n_curr.state, n_curr.g_cost, n_curr.h_cost]
        
        test_node = s_I;
        dist_2_test = compute_distance_2D(n_curr.state, test_node);
        if dist_2_test < 1
            disp('Test Case')
        end
        
        norm_2_goal = compute_distance_2D(n_curr.state, s_G);
        if ((norm_2_goal < in_CONFIG.GOAL_TOLERANCE) && (abs(n_curr.state(3) - s_G(3)) < 15*in_CONFIG.DEG2RAD))
            disp('Reconstructing Path...')
            run reconstruct_traj.m;
            break;
        end
        
        for pid = 1:size(in_CONFIG.NEIGHS,1)
%             for oid = 1:size(in_CONFIG.PRIMITIVE_ANGLES,2)
                c_pose = n_curr.state(1:2) + in_CONFIG.NEIGHS(pid,1:2).*in_CONFIG.PRIMITIVE_LEN;
                if (c_pose(1) < in_CONFIG.MAP_BOUNDS(1,1) || c_pose(1) > in_CONFIG.MAP_BOUNDS(1,2) || ...
                        c_pose(2) < in_CONFIG.MAP_BOUNDS(2,1) || c_pose(2) > in_CONFIG.MAP_BOUNDS(2,2))
                    continue;
                end
%                 c_pose = c_pose + n_curr.state(1:2)
                c_ori = n_curr.state(3)+in_CONFIG.NEIGHS(pid,3);
                if (c_ori < 0) || (c_ori > pi)
                    continue
                end
                c_state = [c_pose, c_ori]; % child state
                c_state_d = get_discrete_state(c_state, in_CONFIG);
                c_state_key = get_key_for_state(c_state_d, in_CONFIG.STR_LEN);
                is_valid_key = isKey(NODES_MAP, c_state_key);
                
                if is_valid_key
                    c_id = NODES_MAP(c_state_key);
                    c_curr = NODES{c_id};
%                     if c_curr.is_closed
%                         continue
%                     end
                end
                
                c_modify = c_state;
                c_modify(3) = c_modify(3)*in_CONFIG.RAD2DEG;
                coll_cost = world_col(c_modify);
                if coll_cost > 0
                    continue
                end
                
                hold on;
                plot(c_state(1), c_state(2), 'go')
                hold off;
                
                new_c_g_cost = n_curr.g_cost + in_CONFIG.PRIMITIVE_LEN/max_dist_cost + abs(c_state(3)-n_curr.state(3))/pi;

                if ~is_valid_key % NEW NODE
                    c_curr = Node(c_state);
                    c_curr.is_open = true;
                    c_curr.is_closed = false;
                    c_curr.g_cost = new_c_g_cost;
                    c_curr.h_cost = (compute_distance_2D(c_state, s_G)/max_dist_cost) + abs(c_state(3)-s_G(3))/pi;
                    c_curr.t_cost =  c_curr.g_cost +  c_curr.h_cost;
                    c_curr.state_d = c_state_d;
                    c_curr.parent_id = n_curr.node_id;
                    total_nodes_id = total_nodes_id + 1;
                    c_curr.node_id = total_nodes_id;
                    c_curr.map_key = c_state_key;
                    NODES{end+1} = c_curr;
                    NODES_MAP(c_curr.map_key) = c_curr.node_id;
                    OPEN_SET.push(c_curr.node_id, c_curr.g_cost, c_curr.h_cost);
%                     debug_c = [c_curr.node_id, c_curr.g_cost, c_curr.h_cost]
%                     c_d = c_curr.state_d
%                     c_d = [c_curr.node_id, c_curr.state, c_curr.g_cost, c_curr.h_cost]
                else
                    old_g_cost = c_curr.g_cost;
                    if (new_c_g_cost < old_g_cost)%(c_curr.g_cost - 0.001))
                        c_curr.g_cost = new_c_g_cost;
                        c_curr.t_cost =  c_curr.g_cost +  c_curr.h_cost;
                        c_curr.parent_id = n_curr.node_id;
                        if (c_curr.is_closed) && (~c_curr.is_open)
                            c_curr.is_closed = false;
                            c_curr.is_open = true;
                            OPEN_SET.push(c_curr.node_id, c_curr.g_cost, c_curr.h_cost);
                        end
%                         ch_d = [c_curr.node_id, c_curr.state, c_curr.g_cost, c_curr.h_cost]
                    end
                    
                end
                
%             end
        end
%         counter = counter + 1;
%         if counter > 5
%             gif
%             counter = 0;
%         end
     end
    
% end