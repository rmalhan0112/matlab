function col=plot_tree(Q,color)
%     axis([0 200 0 200])
%     axis('equal')
   
    hold on;
    %%OBJECT
    %Q=[30,160,90]; %% origin: x,y angle: theta % Objects x,y,theta is
                                                % the input to this
                                                % function
    Qw=30;Qh=10; % object height and width 
    %vertices before rotation
    Qv=[Q(1)-Qw/2 Q(2)-Qh/2;Q(1)+Qw/2 Q(2)-Qh/2;Q(1)+Qw/2 Q(2)+Qh/2;Q(1)-Qw/2 Q(2)+Qh/2];
    Qt=[Q(1);Q(2);0];   % object at zero rotation with center at Q(1) and Q(2)
    theta = Q(3)*pi/180;
    R=[cos(theta) -sin(theta) Q(1);sin(theta) cos(theta) Q(2);0 0 1];
    OA1=[Qv(1,1);Qv(1,2);1];OA2=[Qv(2,1);Qv(2,2);1];OA3=[Qv(3,1);Qv(3,2);1];OA4=[Qv(4,1);Qv(4,2);1];
    %vertices after rotation
    A1n=R*(OA1-Qt);A2n=R*(OA2-Qt);A3n=R*(OA3-Qt);A4n=R*(OA4-Qt);
    %plot object
    rect.Vertices=[A1n(1) A1n(2);A2n(1) A2n(2);A3n(1) A3n(2);A4n(1) A4n(2)];
    oc=color;
    rect.faces=[1 2 3 4];
    patch(rect,'Vertices',rect.Vertices,'FaceColor',oc); %plot object
    
    if color(1) == 1 && color(2) == 1
        plot(Q(1),Q(2),'kx')
    elseif color(1) == 0 && color(2) == 1 && color(3) == 0
        plot(Q(1),Q(2),'kx')    
    else
        plot(Q(1),Q(2),'wx')
    end
    
    hold off;
        
end

