classdef Node < handle
    properties
      node_id;
      state;
      state_d;
      parent_id;
      map_key;
      is_open;
      is_closed;
      h_cost;
      g_cost;
      t_cost;
    end
    methods
    function td = Node(in_state)
        if nargin > 0
            td.state = in_state;
            td.node_id = -1;
            td.state_d = [];
            td.parent_id = -1;
            td.map_key = '';
            td.is_open = false;
            td.is_closed = false;
            td.h_cost = 0;
            td.g_cost = 0;
            td.t_cost = 0;
        end
    end
    
%     function td = Node(in_state, in_color)
%         if nargin > 0
%             td.state = in_state;
%             td.d_state;
%             td.color = in_color;
%             td.parent_id = -1;
%             td.is_open = false;
%             td.is_closed = false;
%             td.h_cost = 0;
%             td.g_cost = 0;
%         else
%             error('ERROR init the Node Object');
%         end
%     end 
    end
end