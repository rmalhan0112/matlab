function discrete_state = get_discrete_state(in_state, in_CONFIG)
% This function discretizes a 3D state
    if size(in_state,1) > size(in_state,2)
        discrete_state = [];%in_state;
%         t1 = [in_state(1,1), in_CONFIG.MAP_BOUNDS(1,1)]
%         t2 = [in_state(2,1), in_CONFIG.MAP_BOUNDS(2,1)]
        discrete_state = [discrete_state; floor((in_state(1,1)-in_CONFIG.MAP_BOUNDS(1,1))/in_CONFIG.MAP_DELTAS(1))+1];
        discrete_state = [discrete_state; floor((in_state(2,1)-in_CONFIG.MAP_BOUNDS(2,1))/in_CONFIG.MAP_DELTAS(2))+1];
        if in_state(3,1) > -0.0001 && in_state(3,1) < 0
            in_state(3,1) = 0;
        end
        if in_state(3,1) < 0
            in_state(3,1) = in_state(3,1) + 2*pi;
        end
%         phi_data = [in_state(3,1)/in_CONFIG.ORI_DELTA, in_CONFIG.ORI_DELTA]
        discrete_state = [discrete_state; floor(in_state(3,1)/in_CONFIG.ORI_DELTA)+1];
    else
        discrete_state = [];%in_state;
%         t1 = [in_state(1,1), in_CONFIG.MAP_BOUNDS(1,1)]
%         t2 = [in_state(1,2), in_CONFIG.MAP_BOUNDS(2,1)]
        discrete_state = [discrete_state; floor((in_state(1,1)-in_CONFIG.MAP_BOUNDS(1,1))/in_CONFIG.MAP_DELTAS(1))+1];
        discrete_state = [discrete_state; floor((in_state(1,2)-in_CONFIG.MAP_BOUNDS(2,1))/in_CONFIG.MAP_DELTAS(2))+1];
        if in_state(1,3) > -0.0001 && in_state(1,3) < 0
            in_state(1,3) = 0;
        end
        if in_state(1,3) < 0
            in_state(1,3) = in_state(1,3) + 2*pi;
        end
%         phi_data = [in_state(1,3)/in_CONFIG.ORI_DELTA, in_CONFIG.ORI_DELTA]
        discrete_state = [discrete_state; floor(in_state(1,3)/in_CONFIG.ORI_DELTA)+1];
    end
end

