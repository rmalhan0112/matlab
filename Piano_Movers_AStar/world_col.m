function col=world_col(Q)
    col=1;
    if((Q(1)<200 & Q(1)>0)&(Q(2)<200 & Q(2)>0))
        %%WORLD
        %rectangle definitions [x,y,w,h]  x,y=origin w,h=width,height
        A=[0 0 10 200];B=[10 130 60 1];C=[10 190 60 10];D=[70 170 60 30];
        E=[130 190 70 10];F=[70 130 60 20];G=[190 130 10 60];H=[170 70 30 60];
        I=[130 70 20 60];J=[190 0 10 70];K=[10 0 180 10];L=[10 10 120 120];

        %vertices  1:bottom left, 2:bottom right, 3: top right, 4: top left
        A1=[A(1) A(2)];A2=[A(1)+A(3) A(2)];A3=[A(1)+A(3) A(2)+A(4)];A4=[A(1) A(2)+A(4)];
        B1=[B(1) B(2)];B2=[B(1)+B(3) B(2)];B3=[B(1)+B(3) B(2)+B(4)];B4=[B(1) B(2)+B(4)];
        C1=[C(1) C(2)];C2=[C(1)+C(3) C(2)];C3=[C(1)+C(3) C(2)+C(4)];C4=[C(1) C(2)+C(4)];
        D1=[D(1) D(2)];D2=[D(1)+D(3) D(2)];D3=[D(1)+D(3) D(2)+D(4)];D4=[D(1) D(2)+D(4)];
        E1=[E(1) E(2)];E2=[E(1)+E(3) E(2)];E3=[E(1)+E(3) E(2)+E(4)];E4=[E(1) E(2)+E(4)];
        F1=[F(1) F(2)];F2=[F(1)+F(3) F(2)];F3=[F(1)+F(3) F(2)+F(4)];F4=[F(1) F(2)+F(4)];
        G1=[G(1) G(2)];G2=[G(1)+G(3) G(2)];G3=[G(1)+G(3) G(2)+G(4)];G4=[G(1) G(2)+G(4)];
        H1=[H(1) H(2)];H2=[H(1)+H(3) H(2)];H3=[H(1)+H(3) H(2)+H(4)];H4=[H(1) H(2)+H(4)];
        I1=[I(1) I(2)];I2=[I(1)+I(3) I(2)];I3=[I(1)+I(3) I(2)+I(4)];I4=[I(1) I(2)+I(4)];
        J1=[J(1) J(2)];J2=[J(1)+J(3) J(2)];J3=[J(1)+J(3) J(2)+J(4)];J4=[J(1) J(2)+J(4)];
        K1=[K(1) K(2)];K2=[K(1)+K(3) K(2)];K3=[K(1)+K(3) K(2)+K(4)];K4=[K(1) K(2)+K(4)];
        L1=[L(1) L(2)];L2=[L(1)+L(3) L(2)];L3=[L(1)+L(3) L(2)+L(4)];L4=[L(1) L(2)+L(4)];
        %%OBJECT
        %Q=[30,160,90]; %% origin: x,y angle: theta % Objects x,y,theta is
                                                    % the input to this
                                                    % function
        Qw=30;Qh=10; % object height and width 

        %vertices before rotation
        Qv=[Q(1)-Qw/2 Q(2)-Qh/2;Q(1)+Qw/2 Q(2)-Qh/2;Q(1)+Qw/2 Q(2)+Qh/2;Q(1)-Qw/2 Q(2)+Qh/2];
        Qt=[Q(1);Q(2);0]; % object at zero rotation with center at Q(1) and Q(2)
        theta = Q(3)*pi/180;
        R=[cos(theta) -sin(theta) Q(1);sin(theta) cos(theta) Q(2);0 0 1];
        OA1=[Qv(1,1);Qv(1,2);1];OA2=[Qv(2,1);Qv(2,2);1];OA3=[Qv(3,1);Qv(3,2);1];OA4=[Qv(4,1);Qv(4,2);1];
        %vertices after rotation
        A1n=R*(OA1-Qt);A2n=R*(OA2-Qt);A3n=R*(OA3-Qt);A4n=R*(OA4-Qt);
        
%         hold on;
%         plot(A1n(1), A1n(2), 'ro')
%         plot(A2n(1), A2n(2), 'ro')
%         plot(A3n(1), A3n(2), 'ro')
%         plot(A4n(1), A4n(2), 'ro')
%         hold off;
        
        collide=0;
        %%COLLISION DETECTION by separating axis theorem
        % check for collision by drawing all 8 edges of 2 rectangles

        if(collision(A1n,A2n,A3n,A4n,A1,A2,A3,A4) && collision(A1,A2,A3,A4,A1n,A2n,A3n,A4n))
            'collides with A';
            collide=1;
        end
%         v1 = collision(A1n,A2n,A3n,A4n,B1,B2,B3,B4)
%         v2 = collision(B1,B2,B3,B4,A1n,A2n,A3n,A4n)
        if(collision(A1n,A2n,A3n,A4n,B1,B2,B3,B4) && collision(B1,B2,B3,B4,A1n,A2n,A3n,A4n))
            'collides with B';
            collide=1;
        end

        if(collision(A1n,A2n,A3n,A4n,C1,C2,C3,C4) && collision(C1,C2,C3,C4,A1n,A2n,A3n,A4n))
            'collides with C';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,D1,D2,D3,D4) && collision(D1,D2,D3,D4,A1n,A2n,A3n,A4n))
            'collides with D';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,E1,E2,E3,E4) && collision(E1,E2,E3,E4,A1n,A2n,A3n,A4n))
            'collides with E';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,F1,F2,F3,F4) && collision(F1,F2,F3,F4,A1n,A2n,A3n,A4n))
            'collides with F';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,G1,G2,G3,G4) && collision(G1,G2,G3,G4,A1n,A2n,A3n,A4n))
            'collides with G';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,H1,H2,H3,H4) && collision(H1,H2,H3,H4,A1n,A2n,A3n,A4n))
            'collides with H';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,I1,I2,I3,I4) && collision(I1,I2,I3,I4,A1n,A2n,A3n,A4n))
            'collides with I';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,J1,J2,J3,J4) && collision(J1,J2,J3,J4,A1n,A2n,A3n,A4n))
            'collides with J';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,K1,K2,K3,K4) && collision(K1,K2,K3,K4,A1n,A2n,A3n,A4n))
            'collides with K';
            collide=1;
        end
        if(collision(A1n,A2n,A3n,A4n,L1,L2,L3,L4) && collision(L1,L2,L3,L4,A1n,A2n,A3n,A4n))
            'collides with L';
            collide=1;
        end
        hold off
        if(collide==0)
            col=0;
            oc='y';
        else
            col=1;
            oc='r';
        end
    else
        col=1;
    end
end


