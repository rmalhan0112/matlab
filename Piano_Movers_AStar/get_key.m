function computed_key = get_key(in_value, str_len)
%This is a fucntion that computes a unique key for each value. It is used
%for storing in the MAP container. In this case it is just computing
%strings by appending discrete values of the state.
    computed_key = '';
    for idx = str_len-1:-1:0
        quo = floor(in_value/10^idx);
        if quo == 0
            computed_key = strcat(computed_key,'0');
        else
            computed_key = strcat(computed_key,num2str(in_value));
            break;
        end
    end
end

