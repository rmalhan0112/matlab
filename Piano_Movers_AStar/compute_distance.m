function [ distance_between_pnts ] = compute_distance(in_point_a, in_point_b)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    if size(in_point_a,1) > size(in_point_a,2)
        in_point_a = in_point_a';
    end
    if size(in_point_b,1) > size(in_point_b,2)
        in_point_b = in_point_b';
    end
    if size(in_point_a,2) ~= size(in_point_b,2)
        error('Provide points with same dimension')
    end
    distance_between_pnts = 0;
    for idx = 1:size(in_point_a,2)
        distance_between_pnts = distance_between_pnts + (in_point_b(1,idx) - in_point_a(1,idx))^2;
    end
    
    distance_between_pnts = sqrt(distance_between_pnts);
end

