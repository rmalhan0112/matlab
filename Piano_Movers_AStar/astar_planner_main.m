clear all; close all;

%  Qi=[30 160 90];     % Initial node [x,y,theta]
%     Qg=[160 30 0];      % Final node [x,y,theta]

run config;
axis tight manual % this ensures that getframe() returns a consistent size
% gif('AStar_PM_30_deg.gif','DelayTime',0.1,'LoopCount',5,'frame',gcf)
hold on;
axis([0, 200, 0, 200])
set(gcf,'color','w');
title('A* Algorithm');
daspect([1 1 1])

s_I = [30 160 90*CONFIG.DEG2RAD];
s_G = [160 30 0];
% s_G = [160 160 90*CONFIG.DEG2RAD];

run load_obstacles.m;
run plot_world;

s_I_mod = s_I;
s_I_mod(3) = s_I_mod(3)*CONFIG.RAD2DEG;
plot_tree(s_G, CONFIG.purple);
plot_tree(s_I_mod, [0 1 0]);

computed_path = [];
cost_to_goal = 0;

in_CONFIG = CONFIG;
tic
run a_star_planner;
toc

counter = 0;
computed_path_d = computed_path;
computed_path_d(:,3) = computed_path_d(:,3).*CONFIG.RAD2DEG;
for pid = 1:size(computed_path_d,1)
    curr_state = computed_path_d(pid,:);
    plot_tree(curr_state, CONFIG.blue);
    if pid > 1
        prev_state = computed_path_d(pid-1,:);
        plot_tree(prev_state, CONFIG.orange);
    end
    counter = counter + 1;
    if counter > 1
        counter = 0;
%         gif
    end
%     pause(1)
end

[lin_dist, rot_dist] = compute_path_cost(computed_path);
disp(strcat('TOTAL COST: ', num2str(lin_dist), ';',num2str(rot_dist)))