function computed_key = get_key_for_state(in_state_d, str_len)
%This is a fucntion that computes a unique key for the whole state. Each
%state consists of several values. It is used for storing in the MAP 
%container. In this case, it is just computing strings for each discrete 
%value and appending it.
    computed_key = '';
    if size(in_state_d,2) < size(in_state_d,1)
        in_state_d = in_state_d';
    end
    for idx = 1:size(in_state_d,2)-1
        curr_key_str = get_key(in_state_d(1,idx), str_len);
        computed_key = strcat(computed_key, curr_key_str);
    end
    
    state_ori = in_state_d(1,3);
    curr_key_str = get_key(state_ori, str_len);
    computed_key = strcat(computed_key, curr_key_str);
end

