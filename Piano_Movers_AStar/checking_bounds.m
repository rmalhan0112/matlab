function [ in_val ] = checking_bounds(in_val, BOUNDS)
% This functions keeps the randomly generated samples within bounds
    if size(in_val,1) > size(in_val,2)
        for sid = 1:size(in_val,1)
    %         test_val = [in_val(sid), BOUNDS(sid,1)]
            if in_val(sid) > BOUNDS(sid,2)
                in_val(sid) = BOUNDS(sid,2);
            elseif in_val(sid) < BOUNDS(sid,1)
                in_val(sid) = BOUNDS(sid,1);
            end
        end
    else
        for sid = 1:size(in_val,2)
    %         test_val = [in_val(sid), BOUNDS(sid,1)]
            if in_val(sid) > BOUNDS(sid,2)
                in_val(sid) = BOUNDS(sid,2);
            elseif in_val(sid) < BOUNDS(sid,1)
                in_val(sid) = BOUNDS(sid,1);
            end
        end
    end

%     X_DIM = 200;
%     Y_DIM = 200;
%     if in_val(1) >= X_DIM
%         in_val(1) = X_DIM-1;
%     elseif in_val(1) <= 0
%         in_val(1) = 1;
%     end
%     if in_val(2) >= Y_DIM
%         in_val(2) = Y_DIM-1;
%     elseif in_val(2) <= 0
%         in_val(2) = 1;
%     end

end

