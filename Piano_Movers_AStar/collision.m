function flag=collision(A1,A2,A3,A4,B1,B2,B3,B4)
    % for line connecting A1 and A2
    if (A2(1)==A1(1))                        % x=c

        m1=(A1(1)-A2(1))/(A1(2)-A2(2));      % delta x / delta y
        c1=A1(1)-m1*A1(2);                   % x=my+c => c= x-my
        sign1=A3(1)-m1*A3(2)-c1;             % sign of A3 with respect to line connecting A1 and A2
        sB11=B1(1)-m1*B1(2)-c1;              % sign of B1 with respect to line connecting A1 and A2
        sB21=B2(1)-m1*B2(2)-c1;              % sign of B2 with respect to line connecting A1 and A2
        sB31=B3(1)-m1*B3(2)-c1;              % sign of B3 with respect to line connecting A1 and A2
        sB41=B4(1)-m1*B4(2)-c1;              % sign of B4 with respect to line connecting A1 and A2         
    else
        m1=(A1(2)-A2(2))/(A1(1)-A2(1));      % delta y / delta x
        c1=A1(2)-m1*A1(1);                   % y=mx+c => c= y-mx
        sign1=A3(2)-m1*A3(1)-c1;             % sign of A3 with respect to line connecting A1 and A2
        sB11=B1(2)-m1*B1(1)-c1;              % sign of B1 with respect to line connecting A1 and A2
        sB21=B2(2)-m1*B2(1)-c1;              % sign of B2 with respect to line connecting A1 and A2
        sB31=B3(2)-m1*B3(1)-c1;              % sign of B3 with respect to line connecting A1 and A2
        sB41=B4(2)-m1*B4(1)-c1;              % sign of B4 with respect to line connecting A1 and A2 
    end
    
    % for line connecting A2 and A3
    if (A2(1)==A3(1))                        %x=c
        m2=(A2(1)-A3(1))/(A2(2)-A3(2));      % delta x / delta y
        c2=A2(1)-m2*A2(2);                   % x=my+c => c= x-my
        sign2=A4(1)-m2*A4(2)-c2;             % sign of A4 with respect to line connecting A3 and A2
        sB12=B1(1)-m2*B1(2)-c2;              % sign of B1 with respect to line connecting A3 and A2
        sB22=B2(1)-m2*B2(2)-c2;              % sign of B2 with respect to line connecting A3 and A2
        sB32=B3(1)-m2*B3(2)-c2;              % sign of B3 with respect to line connecting A3 and A2
        sB42=B4(1)-m2*B4(2)-c2;              % sign of B4 with respect to line connecting A3 and A2 
    else
        m2=(A2(2)-A3(2))/(A2(1)-A3(1));      % delta y / delta x
        c2=A2(2)-m2*A2(1);                   % y=mx+c => c= y-mx
        sign2=A4(2)-m2*A4(1)-c2;             % sign of A4 with respect to line connecting A3 and A2
        sB12=B1(2)-m2*B1(1)-c2;              % sign of B1 with respect to line connecting A3 and A2
        sB22=B2(2)-m2*B2(1)-c2;              % sign of B2 with respect to line connecting A3 and A2
        sB32=B3(2)-m2*B3(1)-c2;              % sign of B3 with respect to line connecting A3 and A2
        sB42=B4(2)-m2*B4(1)-c2;              % sign of B4 with respect to line connecting A3 and A2 
    end
    % for line connecting A3 and A4 
    if (A3(1)==A4(1))                        %x=c
        m3=(A3(1)-A4(1))/(A3(2)-A4(2));      % delta x / delta y
        c3=A3(2)-m3*A3(1);                   % x=my+c => c= x-my
        sign3=A1(1)-m3*A1(2)-c3;             % sign of A1 with respect to line connecting A3 and A4
        sB13=B1(1)-m3*B1(2)-c3;              % sign of B1 with respect to line connecting A3 and A4
        sB23=B2(1)-m3*B2(2)-c3;              % sign of B2 with respect to line connecting A3 and A4
        sB33=B3(1)-m3*B3(2)-c3;              % sign of B3 with respect to line connecting A3 and A4
        sB43=B4(1)-m3*B4(2)-c3;              % sign of B4 with respect to line connecting A3 and A4 
    else
        m3=(A3(2)-A4(2))/(A3(1)-A4(1));      % delta y / delta x
        c3=A3(2)-m3*A3(1);                   % y=mx+c => c= y-mx
        sign3=A1(2)-m3*A1(1)-c3;             % sign of A1 with respect to line connecting A3 and A4
        sB13=B1(2)-m3*B1(1)-c3;              % sign of B1 with respect to line connecting A3 and A4
        sB23=B2(2)-m3*B2(1)-c3;              % sign of B2 with respect to line connecting A3 and A4
        sB33=B3(2)-m3*B3(1)-c3;              % sign of B3 with respect to line connecting A3 and A4
        sB43=B4(2)-m3*B4(1)-c3;              % sign of B4 with respect to line connecting A3 and A4 
    end
   
    % for line connecting A4 and A1
    if (A4(1)==A1(1))                        %x=c
        m4=(A4(1)-A1(1))/(A4(2)-A1(2));      % delta x / delta y
        c4=A1(1)-m4*A1(2);                   % x=my+c => c= x-my
        sign4=A2(1)-m4*A2(2)-c4;             % sign of A2 with respect to line connecting A4 and A1
        sB14=B1(1)-m4*B1(2)-c4;              % sign of B1 with respect to line connecting A4 and A1
        sB24=B2(1)-m4*B2(2)-c4;              % sign of B2 with respect to line connecting A4 and A1
        sB34=B3(1)-m4*B3(2)-c4;              % sign of B3 with respect to line connecting A4 and A1
        sB44=B4(1)-m4*B4(2)-c4;              % sign of B4 with respect to line connecting A4 and A1 
    else
        m4=(A4(2)-A1(2))/(A4(1)-A1(1));      % delta y / delta x
        c4=A1(2)-m4*A1(1);                   % y=mx+c => c= y-mx
        sign4=A2(2)-m4*A2(1)-c4;             % sign of A2 with respect to line connecting A4 and A1
        sB14=B1(2)-m4*B1(1)-c4;              % sign of B1 with respect to line connecting A4 and A1
        sB24=B2(2)-m4*B2(1)-c4;              % sign of B2 with respect to line connecting A4 and A1
        sB34=B3(2)-m4*B3(1)-c4;              % sign of B3 with respect to line connecting A4 and A1
        sB44=B4(2)-m4*B4(1)-c4;              % sign of B4 with respect to line connecting A4 and A1 
    end
       
    
    flag=true;
    
    if (sB11*sign1<0 && sB21*sign1<0 && sB31*sign1<0 && sB41*sign1<0)
        flag=false;
    end
    
    if (sB12*sign2<0 && sB22*sign2<0 && sB32*sign2<0 && sB42*sign2<0)
        flag=false;
    end
        
    if (sB13*sign3<0 && sB23*sign3<0 && sB33*sign3<0 && sB43*sign3<0)
        flag=false;
    end
        
    if (sB14*sign4<0 && sB24*sign4<0 && sB34*sign4<0 && sB44*sign4<0)
        flag=false;
    end
    
end