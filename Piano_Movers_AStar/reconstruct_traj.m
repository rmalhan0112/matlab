
% figure;
% visualize_map_3D(in_CONFIG);
% visualize_grasping_space(in_CONFIG);

computed_path = [];
path_nodes = [];
dist_2_start = n_curr.g_cost;

cost_to_goal = dist_2_start + norm_2_goal;

computed_path = [computed_path; n_G.state];
computed_path = [computed_path; n_curr.state];

path_nodes = [path_nodes; n_G];
path_nodes = [path_nodes; n_curr];

% plot([n_G.state(1), n_curr.state(1)], [n_G.state(2), ...
%         n_curr.state(2)], 'LineWidth', 2, 'color', [1, 0, 0]);
% plot(n_curr.state(1), n_curr.state(2), 'ko')

prev_node = n_curr;

while dist_2_start > 1
    curr_parent_id = prev_node.parent_id;
    curr_node = NODES{curr_parent_id};
    computed_path = [computed_path; curr_node.state];
    path_nodes = [path_nodes; curr_node];
    dist_2_start = compute_distance_2D(s_I, curr_node.state);
%     debug_data = [dist_2_start, curr_node.state]
%     plot([prev_node.state(1), curr_node.state(1)], [prev_node.state(2), ...
%         curr_node.state(2)], 'LineWidth', 2, 'color', [1, 0, 0]);
%     plot(curr_node.state(1), curr_node.state(2), 'ko')
    prev_node = curr_node;
end
% plot([prev_node.state(1), n_I.state(1)], [prev_node.state(2), ...
%         n_I.state(2)], 'LineWidth', 2, 'color', [1, 0, 0]);
computed_path = [computed_path; n_I.state];
path_nodes = [path_nodes; n_I];

computed_path = flipud(computed_path);
path_nodes = flipud(path_nodes);